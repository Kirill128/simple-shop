package by.lysyakov.shop.exelparser.core.etl.value;

@FunctionalInterface
public interface DefaultDataValue<T> extends DataValue<T> {
    default boolean hasError() {
        return false;
    }

    default String getErrorMessage() {
        return null;
    }
}
