package by.lysyakov.shop.exelparser.core.exceptions;

public class ColumnTransformException extends WorkbookTransformException {
    public ColumnTransformException(String message) {
        super(message);
    }

    public ColumnTransformException(String message, Throwable cause) {
        super(message, cause);
    }

    public ColumnTransformException(Throwable cause) {
        super(cause);
    }

    public ColumnTransformException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ColumnTransformException() {
    }
}
