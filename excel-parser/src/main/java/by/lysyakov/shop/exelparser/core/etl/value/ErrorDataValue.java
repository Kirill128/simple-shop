package by.lysyakov.shop.exelparser.core.etl.value;

@FunctionalInterface
public interface ErrorDataValue<T> extends DataValue<T> {
    default T getValue() {
        return null;
    }

    default boolean hasError() {
        return true;
    }
}
