package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Map;

public class ShortValueExtractor extends AbstractCellValueExtractor<Short> {
    static CellValueExtractor<Short> STRING_EXTRACTOR = cell -> {
        try {
            return new SuccessCellValue<>(Short.valueOf(cell.getStringCellValue().trim()));
        } catch (Exception e) {
            return new ErrorCellValue<>("Cannot convert STRING '%s' to SHORT.".formatted(cell.getStringCellValue()));
        }
    };

    public ShortValueExtractor() {
        super(Map.of(CellType.STRING, STRING_EXTRACTOR,
                     CellType.NUMERIC, cell -> new SuccessCellValue<>((short) cell.getNumericCellValue())),
              "Short");
    }
}
