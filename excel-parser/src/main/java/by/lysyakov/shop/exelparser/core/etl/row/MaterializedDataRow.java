package by.lysyakov.shop.exelparser.core.etl.row;

import java.util.List;
import java.util.Map;

public interface MaterializedDataRow {

    int getRowNum();

    List<Object> getData();

    Map<Integer, String> getErrors();

}
