package by.lysyakov.shop.exelparser.core.validation.impl;

import by.lysyakov.shop.exelparser.core.validation.ValidationRule;

import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.joining;

public class ValidationRuleFactory {


    public static <T> ValidationRule<T> emptyValidationRule() {
        return value -> null;
    }

    public static <T> ValidationRule<T> nullableRule(boolean nullable) {
        return value -> {
            if (value == null) {
                return nullable ? null : "Cell must be not null.";
            }
            return null;
        };
    }

    public static <T extends Comparable<T>> ValidationRule<T> valueFromRule(T from) {
        return value -> value.compareTo(from) >= 0 ? null : "Cell value '%s' must start from '%s'.".formatted(value, from);
    }

    public static <T extends Comparable<T>> ValidationRule<T> valueToRule(T to) {
        return value -> value.compareTo(to) <= 0 ? null : "Cell value '%s' must be less or equal '%s'.".formatted(value, to);
    }

    public static ValidationRule<String> regexRule(String regex) {
        return regexRule(Pattern.compile(regex));
    }

    public static ValidationRule<String> regexRule(Pattern pattern) {
        return value -> pattern.matcher(value).matches() ? null : "Cell value '%s' must correspond regex '%s'.".formatted(value, pattern.pattern());
    }

    public static <T> ValidationRule<T> inSetRule(Set<T> possibleValues) {
        return value -> possibleValues.contains(value) ? null : "Not possible value '%s'. Possible values [%s].".formatted(value,
                                                                                                                         setToString(possibleValues));
    }

    static <T> String setToString(Set<T> source) {
        return source.stream()
                     .map(Objects::toString)
                     .map("'%s'"::formatted)
                     .collect(joining(", "));
    }

}
