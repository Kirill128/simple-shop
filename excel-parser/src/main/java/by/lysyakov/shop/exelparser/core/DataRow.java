package by.lysyakov.shop.exelparser.core;

import java.util.List;

import static java.util.Collections.emptyList;

public record DataRow(List<Object> rowData, int groupLevel) {
    public static DataRow empty() {
        return new DataRow(emptyList(), 0);
    }
}
