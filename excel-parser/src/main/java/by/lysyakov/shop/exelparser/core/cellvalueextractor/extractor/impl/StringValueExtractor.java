package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Map;

public class StringValueExtractor extends AbstractCellValueExtractor<String> {

    public StringValueExtractor() {
        super(Map.of(CellType.STRING, cell -> new SuccessCellValue<>(cell.getStringCellValue().isBlank() ? null : cell.getStringCellValue())),
              "String");
    }

}
