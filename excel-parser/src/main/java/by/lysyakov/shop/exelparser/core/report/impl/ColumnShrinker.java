package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Sheet;

public class ColumnShrinker extends AbstractDimensionShrinker {
    private Integer to;

    public ColumnShrinker(Sheet sheet,
                          int firstColumn) {
        super(sheet,
              firstColumn);
    }

    @Override
    public void shrink() {
        to = indexes.first();

        super.shrink();
    }

    @Override
    void shift(int from, int number) {
         sheet.shiftColumns(from, to,
                            number);

         to -= number;
    }
}
