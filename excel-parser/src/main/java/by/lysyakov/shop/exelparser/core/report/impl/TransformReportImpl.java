package by.lysyakov.shop.exelparser.core.report.impl;

import by.lysyakov.shop.exelparser.core.report.TransformReport;
import by.lysyakov.shop.exelparser.core.exceptions.TransformReportException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class TransformReportImpl implements TransformReport {
    private final CellFactory cellFactory;

    private final HyperlinkFactory hyperlinkFactory;

    private final SheetShrinker sheetShrinker;

    private boolean hasErrors = false;

    TransformReportImpl(Workbook workbook,
                        String dataSheetName,
                        String reportSheetName,
                        int rowOffset, int columnOffset) {
        this(workbook,
             dataSheetName,
             createSheetIfNotExists(workbook,
                                    reportSheetName),
             rowOffset, columnOffset);
    }

    TransformReportImpl(Workbook workbook,
                        String dataSheetName,
                        Sheet reportSheet,
                        int rowOffset, int columnOffset) {
        this(new CellFactoryImpl(reportSheet,
                                 rowOffset, columnOffset),
             new HyperlinkFactoryImpl(workbook,
                                      dataSheetName),
             new SheetShrinkerImpl(reportSheet,
                                   rowOffset, columnOffset));
    }

    TransformReportImpl(CellFactory cellFactory,
                        HyperlinkFactory hyperlinkFactory,
                        SheetShrinker sheetShrinker) {
        this.cellFactory = cellFactory;
        this.hyperlinkFactory = hyperlinkFactory;
        this.sheetShrinker = sheetShrinker;
    }

    static Sheet createSheetIfNotExists(Workbook workbook,
                                        String sheetName) {
        try {
            return workbook.createSheet(sheetName);
        } catch (IllegalArgumentException exception) {
            throw new TransformReportException("Cannot create report sheet '%s'.".formatted(sheetName),
                                               exception);
        }
    }

    @Override
    public void error(int row, int column,
                      String message) {
        error(cellFactory.create(row, column),
              message,
              hyperlinkFactory.create(row, column));

        hasErrors = true;

        sheetShrinker.add(row, column);
    }

    void error(Cell cell,
               String message,
               Hyperlink hyperlink) {
        cell.setCellValue(message);
        cell.setHyperlink(hyperlink);
    }

    @Override
    public boolean hasErrors() {
        return hasErrors;
    }

    @Override
    public void close() throws Exception {
        sheetShrinker.shrink();
    }
}
