package by.lysyakov.shop.exelparser.core.exceptions;

public abstract class TransformException extends Exception {
    public TransformException(String message) {
        super(message);
    }

    public TransformException(String message,
                              Throwable cause) {
        super(message, cause);
    }
}
