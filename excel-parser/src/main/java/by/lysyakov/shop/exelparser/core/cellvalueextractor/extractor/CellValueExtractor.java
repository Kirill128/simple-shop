package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.CellValue;
import org.apache.poi.ss.usermodel.Cell;

@FunctionalInterface
public interface CellValueExtractor<T> {

    CellValue<T> getValue(Cell cell);

}
