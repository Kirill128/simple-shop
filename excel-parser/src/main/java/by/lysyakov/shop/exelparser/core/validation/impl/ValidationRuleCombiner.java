package by.lysyakov.shop.exelparser.core.validation.impl;

import by.lysyakov.shop.exelparser.core.validation.ValidationRule;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class ValidationRuleCombiner {

    // logic AND
    public static <T> ValidationRule<T> findAllErrors(ValidationRule<T>... rules) {
        return findAllErrors(Arrays.stream(rules));
    }

    public static <T> ValidationRule<T> findAllErrors(Stream<ValidationRule<T>> rules) {
        return value -> Optional.of(findAllErrorsMessage(value, rules))
                                .filter(message -> !message.isEmpty())
                                .orElse(null);
    }

    static <T> String findAllErrorsMessage(T value,
                                           Stream<ValidationRule<T>> rules) {
        return rules.map(rule -> rule.validate(value))
                    .filter(Objects::nonNull)
                    .map("'%s'"::formatted)
                    .collect(joining(", "));
    }

    //logic OR
    public static <T> ValidationRule<T> findFirstError(ValidationRule<T>... rules) {
        return findFirstError(Arrays.stream(rules));
    }

    public static <T> ValidationRule<T> findFirstError(Stream<ValidationRule<T>> rules) {
        return value -> rules.map(rule -> rule.validate(value))
                             .filter(Objects::nonNull)
                             .map("'%s'"::formatted)
                             .findFirst()
                             .orElse(null);
    }

}
