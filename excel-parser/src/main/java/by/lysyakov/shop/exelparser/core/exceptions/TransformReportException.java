package by.lysyakov.shop.exelparser.core.exceptions;

public class TransformReportException extends RuntimeException {
    public TransformReportException(String message) {
        super(message);
    }

    public TransformReportException(String message, Throwable cause) {
        super(message, cause);
    }
}
