package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.CellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import org.apache.poi.ss.usermodel.CellType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.Optional;

import static by.lysyakov.shop.exelparser.core.cellvalueextractor.MapUtils.mapOf;
import static java.util.Collections.emptyMap;
import static java.util.Map.entry;

public class LocalDateTimeValueExtractor extends AbstractCellValueExtractor<LocalDateTime> {

    public LocalDateTimeValueExtractor() {
        this(emptyMap());
    }

    public LocalDateTimeValueExtractor(String pattern) {
        this(Map.of(CellType.STRING, stringExtractor(pattern)));
    }

    protected LocalDateTimeValueExtractor(Map<CellType, CellValueExtractor<LocalDateTime>> extraExtractors) {
        super(mapOf(extraExtractors,
                    entry(CellType.NUMERIC, cell -> new SuccessCellValue<>(cell.getLocalDateTimeCellValue()))),
              "DateTime");
    }

    static CellValueExtractor<LocalDateTime> stringExtractor(String pattern) {
        return cell -> parseByPattern(cell.getStringCellValue().trim(),
                                      DateTimeFormatter.ofPattern(pattern))
                        .<CellValue<LocalDateTime>>map(SuccessCellValue::new)
                        .orElseGet(() -> new ErrorCellValue<>("Cannot cast str '%s' by pattern '%s' to type 'LocalDateTime'.".formatted(cell.getStringCellValue(),
                                                                                                                                        pattern)));
    }

    static Optional<LocalDateTime> parseByPattern(String date, DateTimeFormatter pattern) {
        try {
            return Optional.of(LocalDateTime.parse(date, pattern));
        } catch (DateTimeParseException e) {
            return Optional.empty();
        }
    }
}
