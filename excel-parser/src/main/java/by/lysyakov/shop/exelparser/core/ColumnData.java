package by.lysyakov.shop.exelparser.core;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import by.lysyakov.shop.exelparser.core.validation.ValidationRule;

//TODO: change name to more convenient
public record ColumnData<T>(CellValueExtractor<T> cellValueExtractor, ValidationRule<T> validator) {
}
