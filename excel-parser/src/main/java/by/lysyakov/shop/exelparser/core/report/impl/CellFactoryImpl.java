package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import static org.apache.poi.ss.usermodel.CellType.STRING;

public class CellFactoryImpl implements CellFactory {
    private final int columnOffset;

    private final RowFactory rowFactory;

    public CellFactoryImpl(Sheet sheet,
                           int rowOffset, int columnOffset) {
        this(new RowFactoryImpl(sheet, rowOffset),
             columnOffset);
    }

    CellFactoryImpl(RowFactory rowFactory,
                    int columnOffset) {
        this.rowFactory = rowFactory;
        this.columnOffset = columnOffset;
    }

    @Override
    public Cell create(int row, int column) {
        return create(rowFactory.getOrCreate(row), column);
    }

    Cell create(Row row, int column) {
        return row.createCell(offset(column), STRING);
    }

    int offset(int index) {
        return index + columnOffset;
    }
}
