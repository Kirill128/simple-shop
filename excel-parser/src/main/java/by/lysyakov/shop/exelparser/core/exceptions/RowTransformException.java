package by.lysyakov.shop.exelparser.core.exceptions;

public class RowTransformException extends WorkbookTransformException {
    public RowTransformException(String message) {
        super(message);
    }

    public RowTransformException(String message, Throwable cause) {
        super(message, cause);
    }

    public RowTransformException(Throwable cause) {
        super(cause);
    }

    public RowTransformException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RowTransformException() {
    }
}
