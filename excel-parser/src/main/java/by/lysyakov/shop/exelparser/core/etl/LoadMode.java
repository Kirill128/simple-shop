package by.lysyakov.shop.exelparser.core.etl;

public enum LoadMode {
    SKIP_FIELD,
    SKIP_ROW,
    ABORT
}
