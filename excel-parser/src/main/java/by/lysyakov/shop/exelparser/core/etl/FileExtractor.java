package by.lysyakov.shop.exelparser.core.etl;

import by.lysyakov.shop.exelparser.core.etl.exception.FileExtractException;

import java.io.InputStream;
import java.util.Map;

/** File extractor interface */
public interface FileExtractor extends AutoCloseable {

    /** File extractor abstract factory interface */
    interface FileExtractorFactory {
        /** Create file extractor instance */
        FileExtractor create(Map<String, String> configuration);
    }

    /**
     * Extract file data into {@link InputStream}
     *
     * @param filePath path to file to extract
     *
     * @return data
     *
     * @throws FileExtractException in case of extract error
     */
    InputStream extract(String filePath) throws FileExtractException;

}
