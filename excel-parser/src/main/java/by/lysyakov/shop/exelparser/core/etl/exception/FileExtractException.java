package by.lysyakov.shop.exelparser.core.etl.exception;

/** Exception thrown in case of file extract error */
public class FileExtractException extends RuntimeException {
    /** {@link RuntimeException} */
    public FileExtractException(String message, Throwable cause) {
        super(message, cause);
    }
}
