package by.lysyakov.shop.exelparser.core.etl;

import by.lysyakov.shop.exelparser.core.etl.value.DataValue;
import by.lysyakov.shop.exelparser.core.etl.value.DefaultDataValue;

@FunctionalInterface
public interface Transformation<I, O> {
    DataValue<O> transform(DefaultDataValue<I> excelCell);
}
