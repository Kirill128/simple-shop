package by.lysyakov.shop.exelparser.core.etl;

import by.lysyakov.shop.exelparser.core.etl.row.DataRow;

public interface Extractor extends Iterable<DataRow> {
}
