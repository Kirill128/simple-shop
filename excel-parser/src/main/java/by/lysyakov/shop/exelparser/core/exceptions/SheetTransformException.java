package by.lysyakov.shop.exelparser.core.exceptions;

public class SheetTransformException extends WorkbookTransformException {
    public SheetTransformException(String message) {
        super(message);
    }

    public SheetTransformException(String message, Throwable cause) {
        super(message, cause);
    }

    public SheetTransformException(Throwable cause) {
        super(cause);
    }

    public SheetTransformException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public SheetTransformException() {
    }
}
