package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Row;

@FunctionalInterface
interface RowFactory {
    Row getOrCreate(int index);
}
