package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.combiner;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;

public class ExtractorCombiner {
    public static <T> CellValueExtractor<T> or(CellValueExtractor<T> first, CellValueExtractor<T> second) {
        return (cell) -> {
            var firstExtractedValue = first.getValue(cell);
            if (firstExtractedValue.hasError()) return second.getValue(cell);
            else return firstExtractedValue;
        };
    }
}
