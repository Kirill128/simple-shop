package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Sheet;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public abstract class AbstractDimensionShrinker implements DimensionShrinker {
    protected final Sheet sheet;

    private final int firstIndex;

    protected final SortedSet<Integer> indexes = new TreeSet<>(Comparator.reverseOrder());

    public AbstractDimensionShrinker(Sheet sheet,
                                     int firstIndex) {
        this.sheet = sheet;

        this.firstIndex = firstIndex;

        indexes.add(firstIndex - 1);
    }

    @Override
    public void add(int index) {
        indexes.add(index + firstIndex);
    }

    @Override
    public void shrink() {
        indexes.stream()
               .sequential()
               .reduce(this::shrink)
               .isPresent(); //FIXME
    }

    int shrink(int current,
               int next) {
        var number = next - current + 1;
        if (number < 0) {
            shift(current,
                  number);
        }
        return next;
    }

    abstract void shift(int from,
                        int number);
}
