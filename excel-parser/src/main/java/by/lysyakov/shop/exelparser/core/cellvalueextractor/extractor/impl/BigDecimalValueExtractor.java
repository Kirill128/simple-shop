package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.FormatUtils;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import org.apache.poi.ss.usermodel.CellType;

import java.math.BigDecimal;
import java.util.Map;

public class BigDecimalValueExtractor extends AbstractCellValueExtractor<BigDecimal> {

    public BigDecimalValueExtractor() {
        this('.');
    }

    public BigDecimalValueExtractor(char separator) {
        super(Map.of(CellType.STRING, stringExtractor(separator),
                     CellType.NUMERIC, cell -> new SuccessCellValue<>(BigDecimal.valueOf(cell.getNumericCellValue()))),
              "BigDecimal");
    }

    static CellValueExtractor<BigDecimal> stringExtractor(char separator) {
        var format = FormatUtils.getBigDecimalFormat(separator);
        return cell -> {
            try {
                return new SuccessCellValue<>((BigDecimal) format.parse(cell.getStringCellValue().trim()));
            } catch (Exception e) {
                return new ErrorCellValue<>("Cannot convert STRING '%s' to BIGDECIMAL.".formatted(cell.getStringCellValue()));
            }
        };
    }
}
