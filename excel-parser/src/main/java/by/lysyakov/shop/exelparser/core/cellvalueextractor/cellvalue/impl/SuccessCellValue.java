package by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.CellValue;

public record SuccessCellValue<T>(T value) implements CellValue<T> {
    @Override
    public T value() {
        return value;
    }

    @Override
    public boolean hasError() {
        return false;
    }

    @Override
    public String errorMessage() {
        return null;
    }

}
