package by.lysyakov.shop.exelparser.core.report;

import org.apache.poi.ss.usermodel.Workbook;

public interface TransformReportFactory {
    TransformReport create(Workbook workbook,
                           String dataSheetName,
                           String reportSheetName,
                           int rowOffset, int columnOffset);
}
