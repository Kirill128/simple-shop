package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Sheet;

public class SheetShrinkerImpl implements SheetShrinker {
    private final DimensionShrinker rowShrinker, columnShrinker;

    SheetShrinkerImpl(Sheet sheet,
                      int firstRow, int firstColumn) {
        this(new RowShrinker(sheet, firstRow), new ColumnShrinker(sheet, firstColumn));
    }

    SheetShrinkerImpl(DimensionShrinker rowShrinker, DimensionShrinker columnShrinker) {
        this.rowShrinker = rowShrinker;
        this.columnShrinker = columnShrinker;
    }

    @Override
    public void add(int row, int column) {
        rowShrinker.add(row);
        columnShrinker.add(row);
    }

    @Override
    public void shrink() {
        rowShrinker.shrink();
        columnShrinker.shrink();
    }
}
