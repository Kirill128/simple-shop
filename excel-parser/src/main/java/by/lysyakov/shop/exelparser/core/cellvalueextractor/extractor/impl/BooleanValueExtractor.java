package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Map;

import static by.lysyakov.shop.exelparser.core.cellvalueextractor.MapUtils.mapOf;
import static java.util.Collections.emptyMap;
import static java.util.Map.entry;

public class BooleanValueExtractor extends AbstractCellValueExtractor<Boolean> {

    public BooleanValueExtractor() {
        this(emptyMap());
    }

    public BooleanValueExtractor(String falseStr, String trueStr) {
        this(Map.of(CellType.STRING, stringExtractor(falseStr, trueStr)));

    }

    public BooleanValueExtractor(int falseNum, int trueNum) {
        this(Map.of(CellType.NUMERIC, numericExtractor(falseNum, trueNum)));
    }

    public BooleanValueExtractor(String falseStr, String trueStr,
                                 int falseNum, int trueNum) {
        this(Map.of(CellType.STRING, stringExtractor(falseStr, trueStr),
                    CellType.NUMERIC, numericExtractor(falseNum, trueNum)));
    }

    protected BooleanValueExtractor(Map<CellType, CellValueExtractor<Boolean>> extraExtractors) {
        super(mapOf(extraExtractors,
                    entry(CellType.BOOLEAN, cell -> new SuccessCellValue<>(cell.getBooleanCellValue()))) ,
              "Boolean");
    }

    static CellValueExtractor<Boolean> numericExtractor(int falseNum, int trueNum) {
        validateArgs(falseNum, trueNum);
        return cell -> {
            int cellValue = (int) cell.getNumericCellValue();//FIXME  double ??? OR NOT ???
            if (cellValue == falseNum) {
                return new SuccessCellValue<>(false);
            } else if (cellValue == trueNum) {
                return new SuccessCellValue<>(true);
            } else {
                return new ErrorCellValue<>("Cannot get bool value from NUMERIC cell='%s' when false='%s', true='%s'".formatted(cell.getNumericCellValue(),
                                                                                                                              falseNum,
                                                                                                                              trueNum));
            }
        };
    }

    static CellValueExtractor<Boolean> stringExtractor(String falseStr, String trueStr) {
        validateArgs(falseStr, trueStr);
        return cell -> {
            var cellValue = cell.getStringCellValue().trim();
            if (cellValue.equals(falseStr)) {
                return new SuccessCellValue<>(false);
            } else if (cellValue.equals(trueStr)) {
                return new SuccessCellValue<>(true);
            } else {
                return new ErrorCellValue<>("Cannot get bool value from STRING cell='%s' when false='%s', true='%s'".formatted(cellValue,
                                                                                                                             falseStr,
                                                                                                                             trueStr));
            }
        };
    }

    static <T, V> void validateArgs(T falsePattern, V truePattern) {
        if (falsePattern.equals(truePattern)) {
            throw new IllegalArgumentException("False='%s', True='%s' must be different.".formatted(falsePattern, truePattern));
        }
    }
}
