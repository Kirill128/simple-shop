package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import org.apache.poi.ss.usermodel.CellType;

import java.math.BigInteger;
import java.util.Map;

public class BigIntegerValueExtractor extends AbstractCellValueExtractor<BigInteger> {
    static CellValueExtractor<BigInteger> STRING_EXTRACTOR = cell -> {
        try {
            return new SuccessCellValue<>(new BigInteger(cell.getStringCellValue().trim()));
        } catch (NumberFormatException e) {
            return new ErrorCellValue<>("Cannot convert STRING '%s' to BIGINTEGER.".formatted(cell.getStringCellValue()));
        }
    };

    public BigIntegerValueExtractor() {
        super(Map.of(CellType.STRING, STRING_EXTRACTOR,
                     CellType.NUMERIC, cell -> new SuccessCellValue<>(BigInteger.valueOf((long) cell.getNumericCellValue()))),
              "BigInteger");
    }
}
