package by.lysyakov.shop.exelparser.core.exceptions;

public class WorkbookTransformException extends RuntimeException {
    public WorkbookTransformException() {
    }

    public WorkbookTransformException(String message) {
        super(message);
    }

    public WorkbookTransformException(String message, Throwable cause) {
        super(message, cause);
    }

    public WorkbookTransformException(Throwable cause) {
        super(cause);
    }

    public WorkbookTransformException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
