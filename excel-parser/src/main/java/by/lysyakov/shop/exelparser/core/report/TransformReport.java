package by.lysyakov.shop.exelparser.core.report;

public interface TransformReport extends AutoCloseable {
    void error(int row, int column,
               String message);

    boolean hasErrors();
}
