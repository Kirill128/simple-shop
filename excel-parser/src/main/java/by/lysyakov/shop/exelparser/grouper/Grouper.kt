package by.lysyakov.shop.exelparser.grouper

import by.lysyakov.shop.exelparser.core.DataRow
import java.util.stream.Stream

class Grouper {

    fun group(rows: Stream<DataRow>, isItemCheck: (DataRow) -> Boolean): List<Group> = splitToGroups(rows.toList(), isItemCheck)

    companion object {

        private fun splitToGroups(source: List<DataRow>, isItemCheck: (DataRow) -> Boolean): List<Group> =
            source.splitByBorder { it.groupLevel == source.first().groupLevel }
                  .map {
                      val head = it.first()
                      val body = it.subList(1, it.size)
                      val wholeBodySameGroupingLevel = body.all { bodyEl -> body.first().groupLevel == bodyEl.groupLevel }

                      if (wholeBodySameGroupingLevel) {
                          val (items, groups) = body.partition { isItemCheck(it) }
                          Group(head, groups.map { Group(it, emptyList(), emptyList()) }, items)
                      }
                      else {
                          val (items, subGroups) = splitToGroups(body, isItemCheck).partition { it.subgroups.isEmpty() &&
                                                                                                it.items.isEmpty() &&
                                                                                                isItemCheck(it.headRow) }
                          Group(head, subGroups, items.map { it.headRow })
                      }
                  }

        private fun <T> List<T>.splitByBorder(borderPredicate: (T) -> Boolean): List<List<T>> {
            if (isEmpty()) return emptyList()
            val matched = listOf(this.first()) + this.subList(1, this.size).takeWhile { !borderPredicate(it) }
            return listOf(matched) + this.subList(matched.size, this.size).splitByBorder(borderPredicate)
        }

    }

}
