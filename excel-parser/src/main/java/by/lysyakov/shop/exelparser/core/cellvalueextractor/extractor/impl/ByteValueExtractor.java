package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Map;

public class ByteValueExtractor extends AbstractCellValueExtractor<Byte> {
    static CellValueExtractor<Byte> STRING_EXTRACTOR = cell -> {
        try {
            return new SuccessCellValue<>(Byte.valueOf(cell.getStringCellValue().trim()));
        } catch (Exception e) {
            return new ErrorCellValue<>("Cannot convert STRING '%s' to BYTE.".formatted(cell.getStringCellValue()));
        }
    };

    public ByteValueExtractor() {
        super(Map.of(CellType.STRING, STRING_EXTRACTOR,
                     CellType.NUMERIC, cell -> new SuccessCellValue<>((byte) cell.getNumericCellValue())),
              "Byte");
    }
}
