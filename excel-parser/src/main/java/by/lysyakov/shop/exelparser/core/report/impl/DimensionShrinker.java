package by.lysyakov.shop.exelparser.core.report.impl;

public interface DimensionShrinker {
    void add(int index);

    void shrink();
}
