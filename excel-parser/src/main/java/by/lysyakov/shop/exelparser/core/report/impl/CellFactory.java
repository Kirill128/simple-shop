package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Cell;

@FunctionalInterface
interface CellFactory {
    Cell create(int row, int column);
}
