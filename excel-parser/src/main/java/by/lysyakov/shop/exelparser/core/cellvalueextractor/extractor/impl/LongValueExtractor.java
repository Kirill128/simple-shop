package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Map;

public class LongValueExtractor extends AbstractCellValueExtractor<Long> {
    static CellValueExtractor<Long> STRING_EXTRACTOR = cell -> {
        try {
            return new SuccessCellValue<>(Long.valueOf(cell.getStringCellValue().trim()));
        } catch (Exception e) {
            return new ErrorCellValue<>("Cannot convert STRING '%s' to LONG.".formatted(cell.getStringCellValue()));
        }
    };

    public LongValueExtractor() {
        super(Map.of(CellType.STRING, STRING_EXTRACTOR,
                     CellType.NUMERIC, cell -> new SuccessCellValue<>((long) cell.getNumericCellValue())),
              "Long");
    }

}
