package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Sheet;

public class RowShrinker extends AbstractDimensionShrinker {
    public RowShrinker(Sheet sheet,
                       int firstRow) {
        super(sheet,
              firstRow);
    }

    @Override
    void shift(int from,
               int number) {
        sheet.shiftRows(from, sheet.getLastRowNum(),
                        number);
    }
}
