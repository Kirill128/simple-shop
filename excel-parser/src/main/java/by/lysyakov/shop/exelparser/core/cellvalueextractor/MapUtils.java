package by.lysyakov.shop.exelparser.core.cellvalueextractor;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapUtils {
    public static <K, V> Map<K, V> mapOf(Map<K, V> map, Entry<K, V>... entries) {
        return new HashMap<>(map) {{
            putAll(Map.ofEntries(entries));
        }};
    }
}
