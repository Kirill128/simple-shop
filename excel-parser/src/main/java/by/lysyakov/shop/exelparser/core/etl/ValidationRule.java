package by.lysyakov.shop.exelparser.core.etl;

import by.lysyakov.shop.exelparser.core.etl.value.DataValue;
import by.lysyakov.shop.exelparser.core.etl.value.DefaultDataValue;

@FunctionalInterface
public interface ValidationRule<I> {
    DataValue<I> validate(DefaultDataValue<I> dataCell);
}
