package by.lysyakov.shop.exelparser.core.report.impl;

import by.lysyakov.shop.exelparser.core.report.TransformReport;
import by.lysyakov.shop.exelparser.core.report.TransformReportFactory;
import org.apache.poi.ss.usermodel.Workbook;

public class TransformReportFactoryImpl implements TransformReportFactory {
    @Override
    public TransformReport create(Workbook workbook,
                                  String dataSheetName,
                                  String reportSheetName,
                                  int rowOffset, int columnOffset) {
        return new TransformReportImpl(workbook,
                                       dataSheetName,
                                       reportSheetName,
                                       rowOffset, columnOffset);
    }
}
