package by.lysyakov.shop.exelparser.core.exceptions;

public class MetadataTransformException extends TransformException {
    public MetadataTransformException(String message) {
        super(message);
    }

    public MetadataTransformException(String message,
                                      Throwable cause) {
        super(message, cause);
    }
}
