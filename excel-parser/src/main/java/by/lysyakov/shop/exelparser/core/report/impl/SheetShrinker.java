package by.lysyakov.shop.exelparser.core.report.impl;

public interface SheetShrinker {
    void add(int row, int column);

    void shrink();
}
