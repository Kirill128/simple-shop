package by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue;

public interface CellValue<T> {

    T value();

    boolean hasError();

    String errorMessage();

}
