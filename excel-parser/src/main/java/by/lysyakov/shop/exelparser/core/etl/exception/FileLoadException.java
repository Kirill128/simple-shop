package by.lysyakov.shop.exelparser.core.etl.exception;


/** Exception thrown in case of file load error */
public class FileLoadException extends RuntimeException {
    /** {@link RuntimeException#RuntimeException(String, Throwable)} */
    public FileLoadException(String message, Throwable cause) {
        super(message, cause);
    }
    /** {@link RuntimeException#RuntimeException(String)} */
    public FileLoadException(String message) {
        super(message);
    }
}
