package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Map;

public class IntegerValueExtractor extends AbstractCellValueExtractor<Integer> {
    static CellValueExtractor<Integer> STRING_EXTRACTOR = cell -> {
        try {
            return new SuccessCellValue<>(Integer.valueOf(cell.getStringCellValue().trim()));
        } catch (Exception e) {
            return new ErrorCellValue<>("Cannot convert STRING '%s' to INTEGER.".formatted(cell.getStringCellValue()));
        }
    };

    public IntegerValueExtractor() {
        super(Map.of(CellType.STRING, STRING_EXTRACTOR,
                     CellType.NUMERIC, cell -> new SuccessCellValue<>((int) cell.getNumericCellValue())),
              "Integer");
    }
}
