package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Optional;

public class RowFactoryImpl implements RowFactory {
    private final Sheet sheet;

    private final int offset;

    public RowFactoryImpl(Sheet sheet,
                          int offset) {
        this.sheet = sheet;
        this.offset = offset;
    }

    @Override
    public Row getOrCreate(int index) {
        return Optional.ofNullable(sheet.getRow(offset(index)))
                       .orElseGet(() -> sheet.createRow(offset(index)));
    }

    int offset(int index) {
        return index + offset;
    }
}
