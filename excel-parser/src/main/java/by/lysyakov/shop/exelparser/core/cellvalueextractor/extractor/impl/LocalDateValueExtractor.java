package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.CellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import org.apache.poi.ss.usermodel.CellType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.Optional;

import static by.lysyakov.shop.exelparser.core.cellvalueextractor.MapUtils.mapOf;
import static java.util.Collections.emptyMap;
import static java.util.Map.entry;

public class LocalDateValueExtractor extends AbstractCellValueExtractor<LocalDate> {

    public LocalDateValueExtractor() {
        this(emptyMap());
    }

    public LocalDateValueExtractor(String pattern) {
        this(Map.of(CellType.STRING, stringExtractor(pattern)));
    }

    protected LocalDateValueExtractor(Map<CellType, CellValueExtractor<LocalDate>> extraExtractors) {
        super(mapOf(extraExtractors,
                    entry(CellType.NUMERIC, cell -> new SuccessCellValue<>(cell.getLocalDateTimeCellValue().toLocalDate()))),
              "Date");
    }

    static CellValueExtractor<LocalDate> stringExtractor(String pattern) {
        return cell -> parseByPattern(cell.getStringCellValue().trim(),
                                      DateTimeFormatter.ofPattern(pattern))
                        .<CellValue<LocalDate>>map(SuccessCellValue::new)
                        .orElseGet(() -> new ErrorCellValue<>("Cannot cast str '%s' by pattern '%s' to type 'LocalDate'.".formatted(cell.getStringCellValue(),
                                                                                                                                    pattern)));
    }

    static Optional<LocalDate> parseByPattern(String date, DateTimeFormatter pattern) {
        try {
            return Optional.of(LocalDate.parse(date, pattern));
        } catch (DateTimeParseException e) {
            return Optional.empty();
        }
    }
}
