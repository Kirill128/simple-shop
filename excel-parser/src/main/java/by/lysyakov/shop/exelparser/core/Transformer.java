package by.lysyakov.shop.exelparser.core;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import by.lysyakov.shop.exelparser.core.exceptions.ColumnTransformException;
import by.lysyakov.shop.exelparser.core.exceptions.MetadataTransformException;
import by.lysyakov.shop.exelparser.core.exceptions.RowTransformException;
import by.lysyakov.shop.exelparser.core.exceptions.SheetTransformException;
import by.lysyakov.shop.exelparser.core.exceptions.TransformException;
import by.lysyakov.shop.exelparser.core.exceptions.WorkbookTransformException;
import by.lysyakov.shop.exelparser.core.report.TransformReport;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Comparator.comparingInt;
import static java.util.Map.entry;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;
import static org.apache.poi.ss.util.CellReference.convertColStringToIndex;
import static org.apache.poi.ss.util.CellReference.convertNumToColString;

public record Transformer(TransformReport transformReport) implements by.lysyakov.shop.exelparser.core.etl.Transformer {

    public enum TransformerFactory implements by.lysyakov.shop.exelparser.core.etl.Transformer.TransformerFactory {
        INSTANCE;

        public by.lysyakov.shop.exelparser.core.etl.Transformer create(TransformReport transformReport) {
            return new Transformer(transformReport);
        }
    }

    record RowRange(int from, int to) {
        boolean isWithin(int value) {
            return value >= from && value <= to;
        }
    }

    //TODO: REGULAR IS INVALID, FOR EXAMPLE: 'AAE'
    private static final Pattern COLUMN_NAME_PATTERN = Pattern.compile("[A-Z]{1,2}|" +
                                                                       "[A-W][A-Z]{2}|" +
                                                                       "X[A-E][A-Z]|" +
                                                                       "XF[A-D]");

    @Override
    public <T> Stream<DataRow> transform(InputStream file, Metadata<T> metadata) throws TransformException {
        return transform(openWorkbook(file),
                         metadata);
    }

    public <T> Stream<DataRow> transform(Workbook workbook, Metadata<T> metadata) throws TransformException {
        try {
            var sheet = getSheetIfExistsAndNotEmpty(workbook, metadata.sheet());

            checkColumnNames(metadata.columnExtractors().keySet());
            checkColumnExtractors(metadata.columnExtractors());

            var columnIndexes = getColumnIndexes(metadata.columnExtractors().keySet());

            checkColsExists(sheet, columnIndexes);

            var columMappers = getColumnExtractors(metadata.columnExtractors(), columnIndexes);

            return getRowsIndexesIfExists(sheet, metadata.firstRow(), metadata.lastRow())
                    .mapToObj(rowNum -> Optional.ofNullable(sheet.getRow(rowNum))
                                                .map(row -> new DataRow(getRowValues(row, columMappers, transformReport), row.getOutlineLevel()))
                                                .orElseGet(() -> {
                                                    columMappers.forEach((colNum, extractorValidator) -> {
                                                        String message = extractorValidator.validator()
                                                                                           .validate(null);
                                                        if (message != null) {
                                                            transformReport.error(rowNum, colNum, message);
                                                        }
                                                    });
                                                    return DataRow.empty();
                                                }))
                    .onClose(() -> closeWorkbook(workbook));
        } catch (Exception exception) {
            closeWorkbook(workbook,
                          exception);

            throw exception;
        }
    }

    static void checkColumnNames(Set<String> names) throws MetadataTransformException {
        if (names == null || names.isEmpty()) {
            throw new MetadataTransformException("No column names.");
        }

        var invalidNames = getInvalidColumnNames(names);
        if (!invalidNames.isEmpty()) {
            throw new MetadataTransformException("Column names (%s) must consist of Latin letters from 'A' to 'XFD'.".formatted(invalidNames));
        }
    }

    static <T> void checkColumnExtractors(Map<String, ColumnData<T>> columnExtractors) throws MetadataTransformException {
        var invalidTypes = getInvalidExtractors(columnExtractors);
        if (!invalidTypes.isEmpty()) {
            throw new MetadataTransformException("Wrong columns extractors: (%s).".formatted(invalidTypes));
        }
    }

    static <T> String getInvalidExtractors(Map<String, ColumnData<T>> columnTypes) {
        return columnTypes.entrySet()
                          .stream()
                          .filter(entry -> !isValidColumnExtractor(entry.getValue().cellValueExtractor()))
                          .map(entry -> "%s ==> %s".formatted(entry.getKey(), entry.getValue().cellValueExtractor()))
                          .collect(joining(", "));

    }

    static <T> boolean isValidColumnExtractor(CellValueExtractor<T> type) {
        return type != null;
    }

    static String getInvalidColumnNames(Set<String> names) {
        return names.stream()
                    .filter(not(Transformer::isValidColumnName))
                    .map("'%s'"::formatted)
                    .sorted()
                    .collect(joining(", "));
    }

    static boolean isValidColumnName(String name) {
        return name != null && COLUMN_NAME_PATTERN.matcher(name)
                                                  .matches();
    }

    static Sheet getSheetIfExistsAndNotEmpty(Workbook workbook, String sheetName) {
        Sheet sheet = Optional.ofNullable(workbook.getSheet(sheetName))
                              .orElseThrow(() -> new SheetTransformException("No such sheet '%s' in workbook (%s).".formatted(sheetName,
                                                                                                                              getWorkbookSheetNames(workbook))));
        if (isSheetEmpty(sheet)) {
            throw new SheetTransformException("Sheet '%s' shouldn't be empty.".formatted(sheetName));
        }
        return sheet;
    }

    static boolean isSheetEmpty(Sheet sheet) {
        return sheet.getFirstRowNum() == -1;
    }

    static String getWorkbookSheetNames(Workbook workbook) {
        return StreamSupport.stream(workbook.spliterator(),
                                    false)
                            .map(Sheet::getSheetName)
                            .map("'%s'"::formatted)
                            .collect(joining(", "));
    }

    static IntStream getRowsIndexesIfExists(Sheet sheet, int firstRow, Integer lastRow) {
        firstRow--;
        lastRow = Optional.ofNullable(lastRow)
                          .map(row -> row - 1)
                          .orElseGet(sheet::getLastRowNum);

        checkRowsExists(sheet,
                        new RowRange(firstRow,
                                     lastRow));

        return IntStream.rangeClosed(firstRow,
                                     lastRow);
    }

    static Map<String, Integer> getColumnIndexes(Set<String> columnNames) {
        return columnNames.stream()
                          .map(name -> entry(name,
                                             convertColStringToIndex(name)))
                          .collect(toMap(Entry::getKey,
                                         Entry::getValue,
                                         (left, right) -> right,
                                         LinkedHashMap::new));
    }

    static <T> Map<Integer, ColumnData<T>> getColumnExtractors(Map<String, ColumnData<T>> columnTypes,
                                                               Map<String, Integer> columnIndexes) {
        return columnIndexes.entrySet()
                            .stream()
                            .map(nameInd -> entry(nameInd.getValue(),
                                                  columnTypes.get(nameInd.getKey())))
//                            .sorted(comparingInt(Entry::getKey))
                            .collect(toMap(Entry::getKey,
                                           Entry::getValue,
                                           (left, right) -> right,
                                           LinkedHashMap::new));
    }

    static void checkColsExists(Sheet sheet, Map<String, Integer> columnIndexes) {
        checkColsExists(getColumnsRange(sheet),
                        columnIndexes);
    }

    static void checkColsExists(RowRange rowRange, Map<String, Integer> colNameToInd) {
        String notExistingColNames = colNameToInd.entrySet()
                                                 .stream()
                                                 .filter(nameAndInd -> !rowRange.isWithin(nameAndInd.getValue()))
                                                 .map(Entry::getKey)
                                                 .collect(joining(", "));
        if (!notExistingColNames.isEmpty()) {
            throw new ColumnTransformException("Not existing columns '%s'. Possible columns from '%s' to '%s'.".formatted(notExistingColNames,
                                                                                                                          convertNumToColString(rowRange.from()),
                                                                                                                          convertNumToColString(rowRange.to())));
        }
    }

    static <T> List<Object> getRowValues(Row row,
                                         Map<Integer, ColumnData<T>> colMappers,
                                         TransformReport transformReport) {
        return colMappers.entrySet()
                         .stream()
                         .map(indAndMapper -> getCellValue(row.getCell(indAndMapper.getKey()),
                                                           indAndMapper.getValue(),
                                                           transformReport))
                         .toList();
    }

    static <T> Object getCellValue(Cell nullableCell, ColumnData<T> columnData, TransformReport transformReport) {
        return Optional.ofNullable(nullableCell)
                       .map(cell -> {
                           var cellValue = columnData.cellValueExtractor()
                                                     .getValue(cell);
                           if (cellValue.hasError()) {
                               transformReport.error(cell.getRowIndex(),
                                                     cell.getColumnIndex(),
                                                     cellValue.errorMessage());
                               return cellValue.value();
                           }
                           String message = columnData.validator()
                                                      .validate(cellValue.value());
                           if (message != null) {
                               transformReport.error(cell.getRowIndex(),
                                                     cell.getColumnIndex(),
                                                     message);
                           }
                           return cellValue.value();
                       })
                       .orElse(null);
    }

    static void checkRowsExists(Sheet sheet, RowRange rowRange) {
        StringBuilder exceptionMessage = new StringBuilder();
        if (rowRange.from() < sheet.getFirstRowNum()) {
            exceptionMessage.append("First row value (%d) must not be less than %d.\n".formatted(rowRange.from() + 1,
                                                                                                 sheet.getFirstRowNum() + 1));
        }
        if (rowRange.to() > sheet.getLastRowNum()) {
            exceptionMessage.append("Last row value (%d) must not be greater than '%d'.".formatted(rowRange.to() + 1,
                                                                                                   sheet.getLastRowNum() + 1));
        }
        if (!exceptionMessage.isEmpty()) {
            throw new RowTransformException(exceptionMessage.toString());
        }
    }

    static RowRange getColumnsRange(Sheet sheet) {
        return StreamSupport.stream(sheet.spliterator(),
                                    false)
                            .map(row -> new RowRange(row.getFirstCellNum(),
                                                     row.getLastCellNum()))
                            .reduce((firstR, secondR) -> new RowRange(min(firstR.from(), secondR.from()),
                                                                      max(firstR.to(), secondR.to())))
                            .get();
    }

    static Workbook openWorkbook(InputStream file) {
        try {
            return WorkbookFactory.create(file);
        } catch (Exception exception) {
            throw new WorkbookTransformException("Failed to open workbook.",
                                                 exception);
        }
    }

    static void closeWorkbook(Workbook workbook) {
        try {
            workbook.close();
        } catch (Exception exception) {
            throw new WorkbookTransformException("Failed to close workbook.",
                                                 exception);
        }
    }

    static void closeWorkbook(Workbook workbook, Exception exception) {
        try {
            closeWorkbook(workbook);
        } catch (Exception suppressedException) {
            exception.addSuppressed(suppressedException);
        }
    }
}
