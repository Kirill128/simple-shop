package by.lysyakov.shop.exelparser.core;

import by.lysyakov.shop.exelparser.core.exceptions.MetadataInitializationException;

import java.util.Map;

public record Metadata<T>(String sheet,
                          int firstRow,
                          Integer lastRow,
                          Map<String, ColumnData<T>> columnExtractors) {

    public Metadata {
        if (sheet == null || sheet.isBlank()) {
            throw new MetadataInitializationException("Sheet name can't be null or blank");
        }

        if (lastRow != null && lastRow < firstRow) {
            throw new MetadataInitializationException("Last row can't be less than first row");
        }

        if (columnExtractors == null) {
            throw new MetadataInitializationException("Column types map can't be null");
        }

    }

}
