package by.lysyakov.shop.exelparser.grouper

import by.lysyakov.shop.exelparser.core.DataRow

/**
 * elements - not including subgroups elements
 */
data class Group(val headRow: DataRow, val subgroups: List<Group>, val items: List<DataRow>)
