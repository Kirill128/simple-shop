package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.Hyperlink;

@FunctionalInterface
interface HyperlinkFactory {
    Hyperlink create(int row, int column);
}
