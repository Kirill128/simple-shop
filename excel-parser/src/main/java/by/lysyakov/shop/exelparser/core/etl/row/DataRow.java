package by.lysyakov.shop.exelparser.core.etl.row;

import by.lysyakov.shop.exelparser.core.etl.value.DataValue;

public interface DataRow {
    int getRowNum();

    DataValue<?> getDataValue(int index);
}
