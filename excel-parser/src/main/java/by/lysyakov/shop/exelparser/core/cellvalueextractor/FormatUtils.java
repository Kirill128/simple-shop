package by.lysyakov.shop.exelparser.core.cellvalueextractor;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class FormatUtils {
    public static DecimalFormat getBigDecimalFormat(char separator) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(separator);

        DecimalFormat format = new DecimalFormat();
        format.setDecimalFormatSymbols(symbols);
        format.setParseBigDecimal(true);

        return format;
    }
}
