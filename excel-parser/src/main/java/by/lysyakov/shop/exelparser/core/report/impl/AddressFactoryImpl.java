package by.lysyakov.shop.exelparser.core.report.impl;

import static org.apache.poi.ss.util.CellReference.convertNumToColString;

class AddressFactoryImpl implements AddressFactory {
    private final String SHEET_ADDRESS_FORMAT = "'%s'!";

    private final String sheetAddress;

    public AddressFactoryImpl(String sheetName) {
        this.sheetAddress = SHEET_ADDRESS_FORMAT.formatted(sheetName);
    }

    @Override
    public String create(int row, int column) {
        return create(rowIndexToNumber(row), columnIndexToName(column));
    }

    String create(int row, String column) {
        return sheetAddress + column + row;
    }

    static int rowIndexToNumber(int index) {
        return index + 1;
    }

    static String columnIndexToName(int index) {
        return convertNumToColString(index);
    }
}
