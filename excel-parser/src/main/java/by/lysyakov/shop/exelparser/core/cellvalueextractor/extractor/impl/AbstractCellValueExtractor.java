package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.CellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Map;

import static by.lysyakov.shop.exelparser.core.cellvalueextractor.MapUtils.mapOf;
import static java.util.Map.entry;

public abstract class AbstractCellValueExtractor<T> implements CellValueExtractor<T> {
    protected CellValueExtractor<T> errorExtractor;
    protected Map<CellType, CellValueExtractor<T>> extractors;

    public AbstractCellValueExtractor(Map<CellType, CellValueExtractor<T>> extractors,
                                      String finalType) {
        this.extractors = mapOf(extractors,
                                entry(CellType.BLANK, cell -> new SuccessCellValue<>(null)));
        this.errorExtractor = cell -> new ErrorCellValue<>("Failed to get '%s' value from cell with type '%s'; supported types %s.".formatted(finalType,
                                                                                                                                              cell.getCellType(),
                                                                                                                                              this.extractors.keySet()));
    }

    @Override
    public CellValue<T> getValue(Cell cell) {
        return extractors.getOrDefault(cell.getCellType(), errorExtractor)
                         .getValue(cell);
    }

}
