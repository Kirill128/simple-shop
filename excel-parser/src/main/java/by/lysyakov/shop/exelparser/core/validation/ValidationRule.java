package by.lysyakov.shop.exelparser.core.validation;

@FunctionalInterface
public interface ValidationRule<T> {
    String validate(T value);
}
