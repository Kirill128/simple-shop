package by.lysyakov.shop.exelparser.core.exceptions;

public class MetadataInitializationException extends RuntimeException  {
    public MetadataInitializationException(String message) {
        super(message);
    }

    public MetadataInitializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
