package by.lysyakov.shop.exelparser.core.etl;

import by.lysyakov.shop.exelparser.core.etl.row.MaterializedDataRow;

import java.util.function.Predicate;

@FunctionalInterface
public interface Loader {

    void load(MaterializedDataRow dataRow);

    default Loader filter(Predicate<MaterializedDataRow> filter) {
        return (dataRow) -> {
            if (filter.test(dataRow)) {
                this.load(dataRow);
            }
        };
    }

    default Loader alsoTo(Loader other) {
        return (dataRow) -> {
            this.load(dataRow);
            other.load(dataRow);
        };
    }
}
