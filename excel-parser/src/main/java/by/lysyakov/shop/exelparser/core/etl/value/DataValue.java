package by.lysyakov.shop.exelparser.core.etl.value;

public interface DataValue<T> {
    T getValue();

    boolean hasError();

    String getErrorMessage();
}
