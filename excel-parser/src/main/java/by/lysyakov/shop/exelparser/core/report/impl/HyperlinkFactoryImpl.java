package by.lysyakov.shop.exelparser.core.report.impl;

import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Workbook;

import static org.apache.poi.common.usermodel.HyperlinkType.DOCUMENT;

class HyperlinkFactoryImpl implements HyperlinkFactory {
    private final CreationHelper creationHelper;

    private final AddressFactory addressFactory;

    public HyperlinkFactoryImpl(Workbook workbook,
                                String sheetName) {
        this(workbook.getCreationHelper(),
             new AddressFactoryImpl(sheetName));
    }

    public HyperlinkFactoryImpl(CreationHelper creationHelper,
                                AddressFactory addressFactory) {
        this.creationHelper = creationHelper;
        this.addressFactory = addressFactory;
    }

    @Override
    public Hyperlink create(int row, int column) {
        return create(addressFactory.create(row, column));
    }

    Hyperlink create(String address) {
        var hyperlink = creationHelper.createHyperlink(DOCUMENT);
        hyperlink.setAddress(address);
        return hyperlink;
    }
}
