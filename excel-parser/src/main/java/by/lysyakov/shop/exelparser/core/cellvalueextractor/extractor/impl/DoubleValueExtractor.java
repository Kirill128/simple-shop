package by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.FormatUtils;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.ErrorCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl.SuccessCellValue;
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Map;

public class DoubleValueExtractor extends AbstractCellValueExtractor<Double> {

    public DoubleValueExtractor() {
        this('.');
    }

    public DoubleValueExtractor(char separator) {
        super(Map.of(CellType.NUMERIC, cell -> new SuccessCellValue<>(cell.getNumericCellValue()),
                     CellType.STRING, stringExtractor(separator)),
              "Double");
    }

    static CellValueExtractor<Double> stringExtractor(char separator) {
        var format = FormatUtils.getBigDecimalFormat(separator);
        return cell -> {
            try {
                return new SuccessCellValue<>(format.parse(cell.getStringCellValue().trim())
                                                    .doubleValue());
            } catch (Exception e) {
                return new ErrorCellValue<>("Cannot convert STRING '%s' to DOUBLE.".formatted(cell.getStringCellValue()));
            }
        };
    }
}
