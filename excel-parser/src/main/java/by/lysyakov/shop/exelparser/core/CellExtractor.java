package by.lysyakov.shop.exelparser.core;

import org.apache.poi.ss.usermodel.Cell;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

@FunctionalInterface
public interface CellExtractor<T> {
    T getValue(Cell cell);

    default <R> CellExtractor<R> andThen(Function<T, R> function) {
        return cell -> function.apply(getValue(cell));
    }

    static CellExtractor<Boolean> ofBoolean() {
        return Cell::getBooleanCellValue;
    }

    static CellExtractor<Boolean> ofBooleanFromNumber(Number falseValue, Number trueValue) {
        var falseDoubleValue = falseValue.doubleValue();
        var trueDoubleValue = trueValue.doubleValue();
        return cell -> toBoolean(falseDoubleValue, trueDoubleValue,
                                 cell.getNumericCellValue());
    }

    static CellExtractor<Boolean> ofBooleanFromString(String falseValue, String trueValue) {
        return cell -> toBoolean(falseValue, trueValue,
                                 cell.getStringCellValue());
    }

    static <T> boolean toBoolean(T falseValue, T trueValue,
                                 T value) {
        if (value.equals(falseValue)) {
            return false;
        }
        if (value.equals(trueValue)) {
            return true;
        }
        throw new IllegalStateException();
    }

    static CellExtractor<Byte> ofByte() {
        return ofNumber(Double::byteValue);
    }

    static CellExtractor<Byte> ofByteFromString() {
        return ofNumberFromString(Byte::parseByte);
    }
    
    static CellExtractor<Short> ofShort() {
        return ofNumber(Double::shortValue);
    }

    static CellExtractor<Short> ofShortFromString() {
        return ofNumberFromString(Short::parseShort);
    }

    static CellExtractor<Integer> ofInteger() {
        return ofNumber(Double::intValue);
    }

    static CellExtractor<Integer> ofIntegerFromString() {
        return ofNumberFromString(Integer::parseInt);
    }

    static CellExtractor<Long> ofLong() {
        return ofNumber(Double::longValue);
    }

    static CellExtractor<Long> ofLongFromString() {
        return ofNumberFromString(Long::parseLong);
    }

    static CellExtractor<Double> ofDouble() {
        return Cell::getNumericCellValue;
    }

    static CellExtractor<BigInteger> ofBigInteger() {
        return toBigInteger(ofBigDecimal());
    }

    static CellExtractor<BigInteger> ofBigIntegerFromString() {
        return toBigInteger(ofBigDecimalFromString());
    }

    static CellExtractor<BigInteger> toBigInteger(CellExtractor<BigDecimal> bigDecimalExtractor) {
        return bigDecimalExtractor.andThen(BigDecimal::toBigInteger);
    }

    static CellExtractor<BigDecimal> ofBigDecimal() {
        return ofNumber(val -> {
            return BigDecimal.valueOf(val);
        });
    }

    static CellExtractor<BigDecimal> ofBigDecimalFromString() {
        return ofNumber(BigDecimal::valueOf);
    }

    static <T extends Number> CellExtractor<T> ofNumber(Function<Double, T> converter) {
        return cell -> converter.apply(cell.getNumericCellValue());
    }

    static <T extends Number> CellExtractor<T> ofNumberFromString(Function<String, T> converter) {
        return cell -> converter.apply(cell.getStringCellValue());
    }

    static CellExtractor<LocalDate> ofLocalDate() {
        return ofLocalDateTime().andThen(LocalDateTime::toLocalDate);
    }

    static CellExtractor<LocalDate> ofLocalDateFromString() {
        return ofLocalDateFromString(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    static CellExtractor<LocalDate> ofLocalDateFromString(String format) {
        return ofLocalDateFromString(DateTimeFormatter.ofPattern(format));
    }

    static CellExtractor<LocalDate> ofLocalDateFromString(DateTimeFormatter formatter) {
        return cell -> LocalDate.parse(cell.getStringCellValue(), formatter);
    }

    static CellExtractor<LocalDateTime> ofLocalDateTime() {
        return Cell::getLocalDateTimeCellValue;
    }

    static CellExtractor<LocalDateTime> ofLocalDateTimeFromString() {
        return ofLocalDateTimeFromString(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    static CellExtractor<LocalDateTime> ofLocalDateTimeFromString(String format) {
        return ofLocalDateTimeFromString(DateTimeFormatter.ofPattern(format));
    }

    static CellExtractor<LocalDateTime> ofLocalDateTimeFromString(DateTimeFormatter formatter) {
        return cell -> LocalDateTime.parse(cell.getStringCellValue(), formatter);
    }

    static CellExtractor<String> ofString() {
        return Cell::getStringCellValue;
    }

    static CellExtractor<String> ofStringOnly() {
        return cell -> {
            switch (cell.getCellType()) {
                case STRING -> {return cell.getStringCellValue();}
                case NUMERIC -> {return String.valueOf(cell.getNumericCellValue());}
                case BOOLEAN -> {return String.valueOf(cell.getBooleanCellValue());}
                default -> throw new IllegalArgumentException("Unsupported cell type %s for string cell %s".formatted(cell.getCellType(), cell)); //todo fixme
            }
        };
    }
}
