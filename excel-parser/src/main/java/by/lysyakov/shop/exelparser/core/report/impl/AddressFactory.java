package by.lysyakov.shop.exelparser.core.report.impl;

@FunctionalInterface
interface AddressFactory {
    String create(int row, int column);
}
