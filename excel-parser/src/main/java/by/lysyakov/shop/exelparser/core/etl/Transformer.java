package by.lysyakov.shop.exelparser.core.etl;

import by.lysyakov.shop.exelparser.core.DataRow;
import by.lysyakov.shop.exelparser.core.exceptions.TransformException;
import by.lysyakov.shop.exelparser.core.report.TransformReport;
import by.lysyakov.shop.exelparser.core.Metadata;

import java.io.InputStream;
import java.util.stream.Stream;

public interface Transformer {

    interface TransformerFactory {
        /** Create file extractor instance */
        Transformer create(TransformReport transformReport);
    }

    <T> Stream<DataRow> transform(InputStream file,
                                  Metadata<T> metadata) throws TransformException;

}
