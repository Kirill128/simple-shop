package by.lysyakov.shop.exelparser.core.etl;

import by.lysyakov.shop.exelparser.core.etl.exception.FileLoadException;

import java.io.OutputStream;
import java.util.Map;

public interface FileLoader extends AutoCloseable {
    /** File loader abstract factory interface */
    interface FileLoaderFactory {
        /** Create file loader instance */
        FileLoader create(Map<String, String> configuration);
    }

    /**
     * Create file an open output stream to the file to write byte data into it
     * <p>
     * Recursively creates file for given path, if file already exists throws exception.
     *
     * @param filePath path to file to create
     *
     * @return output stream to load byte data into file
     *
     * @throws FileLoadException if file exists or file create I/O error
     */
    OutputStream create(String filePath) throws FileLoadException;
}
