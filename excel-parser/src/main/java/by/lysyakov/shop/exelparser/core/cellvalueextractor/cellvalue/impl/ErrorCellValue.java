package by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.impl;

import by.lysyakov.shop.exelparser.core.cellvalueextractor.cellvalue.CellValue;

public record ErrorCellValue<T>(String message) implements CellValue<T> {
    @Override
    public T value() {
        return null;
    }

    @Override
    public boolean hasError() {
        return true;
    }

    @Override
    public String errorMessage() {
        return message;
    }

}
