FROM nginx:1.22-alpine

COPY ./nginx.conf /etc/nginx/nginx.conf

COPY ./front/. /usr/share/nginx/html

RUN ["apk", "update"]
RUN ["apk", "add", "vim"]
RUN ["apk", "add", "openrc"]
#RUN ["apk", "add", "nodejs", "npm"]
#RUN ["apk", "add", "openjdk17-jre"]
RUN ["apk", "add", "postgresql"]

# postgrsesql init

#RUN mkdir /run/postgresql
#RUN chown postgres:postgres /run/postgresql/
#RUN su -c 'mkdir /var/lib/postgresql/data' postgres
#RUN su -c 'chmod 700 /var/lib/postgresql/data' postgres
#RUN su -c 'initdb -D /var/lib/postgresql/data' postgres
#RUN su -c 'pg_ctl start -D /var/lib/postgresql/data' postgres
#RUN su -c 'createdb shop' postgres

COPY ./docker-pgsql-init /etc/init.d/
RUN ["chmod", "+x", "/etc/init.d/docker-pgsql-init"]
RUN echo 'source /etc/init.d/docker-pgsql-init' >> /etc/environment
# add script to autoload on system start
#RUN ["rc-update", "add", "docker-pgsql-init", "default"]
