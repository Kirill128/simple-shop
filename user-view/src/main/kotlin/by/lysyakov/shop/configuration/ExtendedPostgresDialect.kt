package by.lysyakov.shop.configuration

import org.hibernate.dialect.PostgreSQLDialect
import org.hibernate.dialect.function.StandardSQLFunction
import org.hibernate.query.spi.QueryEngine

class ExtendedPostgresDialect: PostgreSQLDialect() {

    override fun initializeFunctionRegistry(queryEngine: QueryEngine?) {
        super.initializeFunctionRegistry(queryEngine)
        queryEngine?.sqmFunctionRegistry?.register("categories_children", StandardSQLFunction("categories_children"))
    }
}

//class CategoriesChildrenFunction: SqlFunction() {
//    private val DEFAULT_COLLECTION_TYPE = "numeric"
//
//    fun hasArguments(): Boolean {
//        return true
//    }
//
//    fun hasParenthesesIfNoArguments(): Boolean {
//        return true
//    }
//
//    @Throws(QueryException::class)
//    fun getReturnType(firstArgumentType: Type?, mapping: Mapping?): Type? {
//        return firstArgumentType
//    }
//
//    @Throws(QueryException::class)
//    fun render(firstArgumentType: Type?, arguments: List<*>, factory: SessionFactoryImplementor?): String? {
//        val collectionName = determineOverriddenCollectionName(arguments)
//        return String.format("ANY (?::%s[])", collectionName)
//    }
//
//    private fun determineOverriddenCollectionName(arguments: List<*>): String {
//        return if (arguments.size > 2) {
//            when (arguments[2] as String?) {
//                "CCM_NUMBERTABLE" -> "numeric"
//                "CCM_VARCHAR255TABLE" -> "varchar(255)"
//                else -> arguments[2] as String
//            }
//        } else DEFAULT_COLLECTION_TYPE
//    }
//}