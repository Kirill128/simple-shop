package by.lysyakov.shop.configuration

import by.lysyakov.shop.service.UserService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain


@Configuration
//@EnableWebSecurity
open class SecurityConfig {

    @Bean
    open fun securityFilterChain(http: HttpSecurity, userService: UserService): SecurityFilterChain {
        http
//            .rememberMe().userDetailsService(userService).and()
//            .addFilterAfter(UsernamePasswordAuthenticationFilter(), CorsFilter::class.java)
            .csrf().disable()
            .cors().and()
            .formLogin().disable()

//            .authorizeHttpRequests().requestMatchers(HttpMethod.GET, "/rest/orders").authenticated()
//            .and()
//            .authorizeHttpRequests().requestMatchers(HttpMethod.POST, "/rest/users/login").authenticated()
//            .and()
//            .authorizeHttpRequests().requestMatchers(HttpMethod.POST, "/rest/users/singUp").anonymous()
//            .and()
//            .authorizeHttpRequests().requestMatchers(HttpMethod.POST, "/rest/users/session").anonymous()
//            .and()
//            .authorizeHttpRequests().requestMatchers("/rest/categories/**", "/rest/products/**").permitAll()
//            .and()
//            .authorizeHttpRequests().requestMatchers(HttpMethod.POST, "/rest/products/**", "/rest/users/login").permitAll()

        return http.build()
    }

    @Bean
    open fun authenticationProvider(userService: UserService, passwordEncoder: PasswordEncoder): DaoAuthenticationProvider {
        return DaoAuthenticationProvider(passwordEncoder).also {
            it.setUserDetailsService(userService)
        }
    }

    @Bean
    open fun passwordEncoder() = BCryptPasswordEncoder()

}
