package by.lysyakov.shop.util

object ApiConfigs {

    const val CREATE_ORDER_DEF_VALUE = """
{
  "userFirstName": "Kirill",
  "userLastName": "Lysiakou",
  "phone": "+375990000290",
  "email": "some@gmail.com",
  "comment": "I wanna byu it",
  "productIdToCount": {
    "1": 1,
    "2": 3
  }
}
    """

    const val GET_PRODUCT_BY_CRITERIA = """
{
  "productsCount": 20,
  "pageNum": 0,
  "sortDirection": "ASC",
  "sortBy": "price",
  "priceFrom": 0,
  "priceTo": 200,
  "searchName": "лампочка",
  "categoryId": 1,
  "attrIdToCriteria": [
    {
      "attrId": 1,
      "operationTypeId": 4,
      "userVal": [2, 3]
    }
  ]
}

    """
    // userVal : [2,3] - id из attribute_possible_val.id

}
