package by.lysyakov.shop.repository

import by.lysyakov.shop.entities.Category
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface CategoryRepository: JpaRepository<Category, Long> {
    fun findAllByVisibleIsTrue(): List<Category>

    @Query(value = "select * from categories_children(:parentCategoryId)", nativeQuery = true)
    fun findAllChildrenIds(@Param("parentCategoryId") parentCategoryId: Int): List<Int>
}
