package by.lysyakov.shop.repository

import by.lysyakov.shop.entities.OrderProduct
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface OrderProductRepository: JpaRepository<OrderProduct, OrderProduct.PrimaryKey>
