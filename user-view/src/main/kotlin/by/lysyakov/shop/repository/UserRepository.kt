package by.lysyakov.shop.repository

import by.lysyakov.shop.entities.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface UserRepository: JpaRepository<User, Long> {

    fun findUserByEmail(email: String): Optional<User>

}
