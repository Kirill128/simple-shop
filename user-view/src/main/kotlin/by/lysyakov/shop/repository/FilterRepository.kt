package by.lysyakov.shop.repository

import by.lysyakov.shop.entities.Filter
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface FilterRepository: JpaRepository<Filter, Long> {

    @Query("""from Filter f join fetch f.categoryFilters cf 
                            join fetch f.attributeValues av 
                            where cf.category.id = :categoryId and av.visibleOnFilter = true""")
    fun findAllByCategory(@Param("categoryId") categoryId: Long): List<Filter>

}