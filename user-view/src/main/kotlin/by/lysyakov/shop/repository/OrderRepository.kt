package by.lysyakov.shop.repository

import by.lysyakov.shop.entities.Order
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface OrderRepository: JpaRepository<Order, Long> {

    @EntityGraph(attributePaths = ["orderProducts", "orderProducts.product"])
    fun findAllByUserId(id: Long): List<Order>

}
