package by.lysyakov.shop.repository

import by.lysyakov.shop.entities.Product
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface ProductRepository: JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {
    @EntityGraph(attributePaths = ["productAttributes", "productAttributes.attribute"])
    override fun findById(id: Long): Optional<Product>
}
