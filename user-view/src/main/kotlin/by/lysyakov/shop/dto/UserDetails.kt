package by.lysyakov.shop.dto

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User

class UserDetails(
    val id: Long,
    name: String,
    password: String,
    authorities: MutableList<GrantedAuthority>
): User(name, password, authorities)