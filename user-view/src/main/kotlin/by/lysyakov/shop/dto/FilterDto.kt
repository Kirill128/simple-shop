package by.lysyakov.shop.dto

class FilterDto(
    val title: String,
    val attributeId: Long,
    val filtrationTypeId: Int,
    val filterValues: List<FilterValue>
)
