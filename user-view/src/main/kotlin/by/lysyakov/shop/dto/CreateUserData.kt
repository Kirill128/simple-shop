package by.lysyakov.shop.dto

data class CreateUserData(val name: String, val email: String, val phone: String, val notEncryptedPassword: String)
data class LoginUserData(val email: String, val notEncryptedPassword: String)
