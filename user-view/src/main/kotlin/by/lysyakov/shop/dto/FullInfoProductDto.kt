package by.lysyakov.shop.dto

data class FullInfoProductDto(
    val shortName: String,
    val imagesPaths: Set<String>,
    val price: Double,
    val productAttributes: List<AttributeValue>,
    val recommendedProducts: MutableList<ProductDto> = mutableListOf()
)

data class AttributeValue(
    val attributeName: String,
    val attributeValue: String
)