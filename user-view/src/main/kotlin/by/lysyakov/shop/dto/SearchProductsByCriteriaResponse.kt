package by.lysyakov.shop.dto

import org.springframework.data.domain.Page

class SearchProductsByCriteriaResponse(
    val products: Page<ProductDto>,
    val filters: List<FilterDto>
)
