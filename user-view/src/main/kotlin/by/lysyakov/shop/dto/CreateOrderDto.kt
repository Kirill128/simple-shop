package by.lysyakov.shop.dto

import by.lysyakov.shop.entities.Order
import by.lysyakov.shop.entities.OrderProduct
import by.lysyakov.shop.entities.Product
import by.lysyakov.shop.entities.Status
import by.lysyakov.shop.exceptions.OrderValidationException
import by.lysyakov.shop.entities.Status.IDS.CREATED
import by.lysyakov.shop.entities.User
import java.time.LocalDateTime
import java.util.regex.Pattern

data class CreateOrderDto(
    val productIdToCount: Map<Long, Int>,
    val userName: String,
    val phone: String,
    val email: String,
    val comment: String,
    var userId: Long? = null
) {

    init {
        validate()
    }

    private fun validate() {
        var resultMessage = ""
        if (productIdToCount.isEmpty()) {
            resultMessage += "Orders list must be not empty. "
        }
        if (userName.isBlank()) {
            resultMessage += "User name must be not empty."
        }
        if (!PHONE_PATTERN.matcher(phone).matches()) {
            resultMessage += "User phone must contains 9 digits (251234567). Current: $phone."
        }
//        if (EMAIL_PATTERN.matcher(email).matches()) {
//            resultMessage += "Not valid email '$email'."
//        }
        if (resultMessage.isNotEmpty()) {
            throw OrderValidationException(resultMessage)
        }
    }

    fun toOrder(productIdToEntity: (Long) -> Product = { Product.builder().id(it).build() }): Order =
        Order.builder()
             .message(this.comment)
             .userName(this.userName)
             .phone(this.phone)
             .email(this.email)
             .startTime(LocalDateTime.now())
             .lastModified(LocalDateTime.now())
             .status(Status(CREATED.id))
             .user(userId?.let { User.builder().id(it).build() })
             .build()
             .also { it.orderProducts = getOrderProducts(productIdToEntity, it) }

    private fun getOrderProducts(
        productIdToEntity: (Long) -> Product,
        order: Order
    ) = productIdToCount.map { (id, count) ->
            val product = productIdToEntity(id)
            OrderProduct.builder()
                        .count(count)
                        .priceForOne(product.price)
                        .product(product)
                        .order(order)
                        .build()
        }

    companion object {
        private val PHONE_PATTERN = Pattern.compile("\\d{9}")
        private val EMAIL_PATTERN = Pattern.compile("^(.+)@(\\S+)$")
    }
}