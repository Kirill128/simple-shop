package by.lysyakov.shop.dto

import by.lysyakov.shop.entities.FilterOperationType
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

data class SearchCriteria(
    val productsCount: Int,
    val pageNum: Int,
    val sortDirection: String,
    val sortBy: String,
    val searchAttributes: List<AttributeSearchCriteria> = emptyList(),
    val priceFrom: Double?,
    val priceTo: Double?,
    val searchName: String?,
    val categoryId: Long?
)

data class AttributeSearchCriteria(
    val attrId: Long,
    private val operationTypeId: Int,
    private val userVal: JsonNode
) {
    val operationType: FilterOperationType = FilterOperationType.of(operationTypeId)

    fun <T> getUserValue(): T = objMapper.convertValue(userVal, object: TypeReference<T>() {})

    companion object {
        val objMapper = ObjectMapper()
    }
}
