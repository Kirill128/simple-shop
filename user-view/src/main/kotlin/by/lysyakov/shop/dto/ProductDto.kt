package by.lysyakov.shop.dto

data class ProductDto(
    val id: Long,
    val shortName: String,
    val imagePath: String,
    val price: Double
)
