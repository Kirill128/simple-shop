package by.lysyakov.shop.dto

import by.lysyakov.shop.entities.Status
import java.time.LocalDateTime

class OrderDto(val orderId: Long, val creationDate: LocalDateTime, val status: String, val orderedProducts: List<OrderedProduct>)

class OrderedProduct(val productId: Long, val count: Int, val name: String, val price: Double, val imagePath: String)
