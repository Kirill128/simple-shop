package by.lysyakov.shop.dto

class FilterValue(val id: Long, val value: String)
