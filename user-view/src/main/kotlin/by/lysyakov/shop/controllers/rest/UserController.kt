package by.lysyakov.shop.controllers.rest

import by.lysyakov.shop.dto.CreateUserData
import by.lysyakov.shop.dto.UserDetails
import by.lysyakov.shop.service.UserService
import jakarta.mail.AuthenticationFailedException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RequestMethod.POST
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.context.request.RequestContextHolder
import java.util.concurrent.ConcurrentHashMap

@RestController
@RequestMapping("/rest/users")
@CrossOrigin(methods = [GET, POST])
class UserController(private val userService: UserService,
                     private val bCryptPasswordEncoder: BCryptPasswordEncoder) {

    @PostMapping("/signUp")
    fun register(@RequestBody newUser: CreateUserData,
                 @RequestHeader("JSESSIONID") sessionId: String): String {
        SessionsHolder.checkNotAuthenticated(sessionId)
        userService.addCustomer(newUser)
        return SessionsHolder.sessionId()
    }

    @PostMapping("/login")
    fun login(@RequestBody loginData: LoginData,
              @RequestHeader("JSESSIONID") sessionId: String): LoginToken {
        if (SessionsHolder.isAuthenticated(sessionId)) return LoginToken(sessionId)

        val userDetails = userService.findUserByEmail(loginData.email)
        if (!bCryptPasswordEncoder.matches(loginData.password, userDetails.password)) {
            throw AuthenticationFailedException("Not matcher password")
        }
        SessionsHolder.authenticate(userDetails)
        return LoginToken(SessionsHolder.sessionId())
    }

    class LoginData(val email: String, val password: String)
    class LoginToken(val token: String)
}

object SessionsHolder {
    // add session expiration
    private val sessionToAuthUser = ConcurrentHashMap<String, UserDetails>()

    fun checkNotAuthenticated(sessionId: String) {
        if (isAuthenticated(sessionId)) {
            throw AuthenticationFailedException("Already authenticated")
        }
    }
    fun checkAuthenticated(sessionId: String) {
        if (isAuthenticated(sessionId).not()) {
            throw AuthenticationFailedException("Not authenticated")
        }
    }

    fun isAuthenticated(sessionId: String): Boolean {
        return sessionToAuthUser.containsKey(sessionId)
    }

    fun sessionId(): String {
        return RequestContextHolder.currentRequestAttributes().sessionId
    }

    fun authenticate(userDetails: UserDetails) {
        sessionToAuthUser[sessionId()] = userDetails
    }

    fun userDetails(sessionId: String) = sessionToAuthUser[sessionId]

    fun userId(sessionId: String) = userDetails(sessionId)?.id

}
