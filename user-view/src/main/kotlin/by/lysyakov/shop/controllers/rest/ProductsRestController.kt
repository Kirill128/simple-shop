package by.lysyakov.shop.controllers.rest

import by.lysyakov.shop.dto.SearchCriteria
import by.lysyakov.shop.dto.SearchProductsByCriteriaResponse
import by.lysyakov.shop.service.ProductService
import by.lysyakov.shop.util.ApiConfigs.GET_PRODUCT_BY_CRITERIA
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RequestMethod.POST

@RestController
@RequestMapping("/rest/products")
@CrossOrigin(methods = [GET, POST])
class ProductsRestController(private val productService: ProductService) {

    @PostMapping("/search/criteria")
    fun getByCriteria(
        @RequestBody(required = false)
        @ApiParam(value = GET_PRODUCT_BY_CRITERIA)
        criteria: SearchCriteria
    ): SearchProductsByCriteriaResponse = productService.findVisibleByCriteria(criteria)

    @PostMapping("/search/ids")
    fun getByIds(@RequestBody ids: Set<Long>) = productService.findByIds(ids)

    @GetMapping("/search/{id}")
    fun getById(
        @PathVariable id: Long,
        @RequestParam(required = false, defaultValue = "6") recommendedCount: Int
    ) = productService.findById(id, recommendedCount)

}
