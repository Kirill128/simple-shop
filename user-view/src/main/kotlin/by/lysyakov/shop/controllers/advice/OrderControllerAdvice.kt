package by.lysyakov.shop.controllers.advice

import by.lysyakov.shop.exceptions.NoSuchProductException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class OrderControllerAdvice: ResponseEntityExceptionHandler() {

    @ExceptionHandler(NoSuchProductException::class)
    fun productForOrderNotFound(exception: NoSuchProductException, request: WebRequest): ResponseEntity<Any> =
        handleExceptionInternal(
            exception,
            ExceptionResult(NoSuchProductException.TAG, exception.invalidIds),
            HttpHeaders(),
            HttpStatus.NOT_FOUND,
            request
        )

    internal data class ExceptionResult(val errorTag: String, val invalidIds: Set<Long>)
}