package by.lysyakov.shop.controllers.rest

import by.lysyakov.shop.dto.CreateOrderDto
import by.lysyakov.shop.dto.OrderDto
import by.lysyakov.shop.service.OrderService
import by.lysyakov.shop.util.ApiConfigs.CREATE_ORDER_DEF_VALUE
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RequestMethod.POST
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/rest/orders")
@CrossOrigin(methods = [GET, POST])
class OrderRestController(private val orderService: OrderService) {

    @PostMapping
    fun createNewOrder(
        @RequestBody
        @ApiParam(value = CREATE_ORDER_DEF_VALUE)
        newOrderDto: CreateOrderDto,
        @RequestHeader("JSESSIONID", required = false, defaultValue = "") sessionId: String
    ) = ResponseOrder(orderService.createNewOrder(newOrderDto.also { it.userId = SessionsHolder.userId(sessionId) }))

    @GetMapping
    fun getAllOrders(@RequestHeader("JSESSIONID") sessionId: String): List<OrderDto> {
        SessionsHolder.checkAuthenticated(sessionId)
        return orderService.findUserOrders(SessionsHolder.userId(sessionId)!!)
    }

    inner class ResponseOrder(val id: Long)

}
