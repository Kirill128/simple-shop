package by.lysyakov.shop.controllers.rest

import by.lysyakov.shop.common.dto.TreeCategoryDto
import by.lysyakov.shop.service.CategoryService
import org.springframework.data.domain.Range
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RequestMethod.*

@RestController
@RequestMapping("/rest/categories")
@CrossOrigin(methods = [GET, POST])
class CategoriesRestController(private val categoryService: CategoryService) {

    @GetMapping
    fun visibleCategoriesTree(): Set<TreeCategoryDto> = categoryService.visibleCategoriesTree()

}