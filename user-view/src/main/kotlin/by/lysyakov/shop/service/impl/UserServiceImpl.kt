package by.lysyakov.shop.service.impl

import by.lysyakov.shop.dto.CreateUserData
import by.lysyakov.shop.entities.Role
import by.lysyakov.shop.entities.Roles.CUSTOMER
import by.lysyakov.shop.entities.User
import by.lysyakov.shop.repository.UserRepository
import by.lysyakov.shop.service.UserService
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(private val userRepository: UserRepository,
                      private val encryptor: BCryptPasswordEncoder): UserService {

    override fun addCustomer(newUser: CreateUserData): Long =
        userRepository.save(User.builder()
                                .name(newUser.name)
                                .email(newUser.email)
                                .password(encryptor.encode(newUser.notEncryptedPassword))
                                .phone(newUser.phone)
                                .roles(mutableSetOf(Role(CUSTOMER.id)))
                                .build())
                      .id

    override fun loadUserByUsername(userName: String): UserDetails = findUserByEmail(userName)

    override fun findUserByEmail(email: String): by.lysyakov.shop.dto.UserDetails =
        userRepository.findUserByEmail(email)
                      .map {
                          by.lysyakov.shop.dto.UserDetails(it.id,
                                                           it.name,
                                                           it.password,
                                                           mutableListOf(GrantedAuthority { "AUTHENTICATED" }))
                      }
                      .orElseThrow { UsernameNotFoundException("User '${email}' not found.") }

}