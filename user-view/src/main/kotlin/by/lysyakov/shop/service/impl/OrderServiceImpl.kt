package by.lysyakov.shop.service.impl

import by.lysyakov.shop.common.mail.OrderSavedMailMessage
import by.lysyakov.shop.dto.CreateOrderDto
import by.lysyakov.shop.dto.OrderDto
import by.lysyakov.shop.entities.Product
import by.lysyakov.shop.exceptions.NoSuchProductException
import by.lysyakov.shop.mapper.OrderMapper
import by.lysyakov.shop.repository.OrderRepository
import by.lysyakov.shop.repository.ProductRepository
import by.lysyakov.shop.service.OrderService
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class OrderServiceImpl(
    private val orderRepository: OrderRepository,
    private val productRepository: ProductRepository,
    private val mailSender: JavaMailSender,
    private val orderMapper: OrderMapper
): OrderService {

    @Transactional
    override fun createNewOrder(newOrderDto: CreateOrderDto): Long {
        val idToProduct = findExistingProductsOrThrow(newOrderDto.productIdToCount.keys)

        return orderRepository.save(newOrderDto.toOrder { id -> idToProduct[id]!! })
                              .also { mailSender.send(OrderSavedMailMessage(it, mailSender.createMimeMessage())) }
                              .id
    }

    @Transactional(readOnly = true)
    override fun findUserOrders(userId: Long): List<OrderDto> {
        return orderMapper.mapToDtos(orderRepository.findAllByUserId(userId))
    }

    private fun findExistingProductsOrThrow(ids: Set<Long>): Map<Long, Product> {
        val idToProduct = productRepository.findAllById(ids).associateBy { it.id }

        val notFoundProductIds = ids - idToProduct.keys
        if (notFoundProductIds.isNotEmpty()) {
            throw NoSuchProductException("No product by ids=[$notFoundProductIds] found.", notFoundProductIds)
        }

        return idToProduct
    }
}