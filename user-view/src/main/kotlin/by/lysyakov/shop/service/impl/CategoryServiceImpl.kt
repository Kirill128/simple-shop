package by.lysyakov.shop.service.impl

import by.lysyakov.shop.common.CategoryTreeGenerator
import by.lysyakov.shop.common.dto.TreeCategoryDto
import by.lysyakov.shop.common.dto.TreeCategoryDto.Companion.toCategoriesTree
import by.lysyakov.shop.repository.CategoryRepository
import by.lysyakov.shop.service.CategoryService
import org.springframework.stereotype.Service

@Service
class CategoryServiceImpl(
    private val categoryDao: CategoryRepository,
    private val categoryTreeGenerator: CategoryTreeGenerator
): CategoryService {

    override fun visibleCategoriesTree(): Set<TreeCategoryDto> =
        categoryTreeGenerator.generateTree(categoryDao.findAllByVisibleIsTrue())
                             .toCategoriesTree()

}
