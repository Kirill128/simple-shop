package by.lysyakov.shop.service

import by.lysyakov.shop.dto.CreateOrderDto
import by.lysyakov.shop.dto.OrderDto

interface OrderService {
    fun createNewOrder(newOrderDto: CreateOrderDto): Long
    fun findUserOrders(userId: Long): List<OrderDto>
}