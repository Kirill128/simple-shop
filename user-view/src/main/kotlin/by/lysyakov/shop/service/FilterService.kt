package by.lysyakov.shop.service

import by.lysyakov.shop.dto.FilterDto

interface FilterService {

    fun findAllByCategory(categoryId: Long): List<FilterDto>

}