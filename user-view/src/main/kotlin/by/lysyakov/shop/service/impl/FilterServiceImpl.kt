package by.lysyakov.shop.service.impl

import by.lysyakov.shop.mapper.FilterMapper.toDto
import by.lysyakov.shop.repository.FilterRepository
import by.lysyakov.shop.service.FilterService
import org.springframework.stereotype.Service

@Service
open class FilterServiceImpl(private val filterRepository: FilterRepository) : FilterService {

    override fun findAllByCategory(categoryId: Long) = filterRepository.findAllByCategory(categoryId)
                                                                       .map { it.toDto() }
}