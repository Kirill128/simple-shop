package by.lysyakov.shop.service

import by.lysyakov.shop.dto.CreateUserData
import by.lysyakov.shop.dto.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService

interface UserService: UserDetailsService {
    fun addCustomer(newUser: CreateUserData): Long
    fun findUserByEmail(email: String): UserDetails
}