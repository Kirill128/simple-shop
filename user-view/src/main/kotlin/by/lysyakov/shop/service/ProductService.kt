package by.lysyakov.shop.service

import by.lysyakov.shop.dto.FullInfoProductDto
import by.lysyakov.shop.dto.ProductDto
import by.lysyakov.shop.dto.SearchCriteria
import by.lysyakov.shop.dto.SearchProductsByCriteriaResponse
import java.util.Optional

interface ProductService {
    fun findVisibleByCriteria(criteria: SearchCriteria): SearchProductsByCriteriaResponse
    fun findByIds(ids: Set<Long>): List<ProductDto>
    fun findById(id: Long, recommendedCount: Int): Optional<FullInfoProductDto>
}