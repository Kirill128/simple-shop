package by.lysyakov.shop.service

import by.lysyakov.shop.common.dto.TreeCategoryDto

interface CategoryService {
    fun visibleCategoriesTree(): Set<TreeCategoryDto>
}