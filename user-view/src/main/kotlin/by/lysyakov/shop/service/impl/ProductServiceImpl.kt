package by.lysyakov.shop.service.impl

import by.lysyakov.shop.common.util.Extensions.isNotEmpty
import by.lysyakov.shop.common.util.Extensions.isNotNull
import by.lysyakov.shop.dto.FullInfoProductDto
import by.lysyakov.shop.dto.ProductDto
import by.lysyakov.shop.dto.SearchCriteria
import by.lysyakov.shop.dto.SearchProductsByCriteriaResponse
import by.lysyakov.shop.mapper.ProductMapper
import by.lysyakov.shop.mapper.ProductMapper.Companion.toProductDtos
import by.lysyakov.shop.repository.ProductRepository
import by.lysyakov.shop.service.FilterService
import by.lysyakov.shop.service.ProductService
import by.lysyakov.shop.specification.ProductSpecification
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.Optional

@Service
open class ProductServiceImpl(
    private val productDao: ProductRepository,
    private val productMapper: ProductMapper,
    private val filterService: FilterService,
    private val productSpecificationFactory: ProductSpecification
): ProductService {

    override fun findVisibleByCriteria(criteria: SearchCriteria): SearchProductsByCriteriaResponse {
        val products = productDao.findAll(productSpecificationFactory.productSpecification(criteria),
                                          PageRequest.of(criteria.pageNum,
                                                         criteria.productsCount,
                                                         Sort.Direction.fromString(criteria.sortDirection),
                                                         criteria.sortBy))
                                 .toProductDtos(productMapper)
        val filters = when(products.isNotEmpty() && criteria.categoryId.isNotNull()) {
            true -> filterService.findAllByCategory(criteria.categoryId!!)
            else -> emptyList()
        }
        return SearchProductsByCriteriaResponse(products, filters)
    }

    override fun findByIds(ids: Set<Long>): List<ProductDto> =
        productMapper.mapToProductDtos(productDao.findAllById(ids))

    override fun findById(id: Long, recommendedCount: Int): Optional<FullInfoProductDto> =
        productDao.findById(id).map { productMapper.mapToFullInfoProductDto(it)
            .also { it.recommendedProducts += productDao.findAll(Pageable.ofSize(recommendedCount))
                                                        .toProductDtos(productMapper) }
        }
}