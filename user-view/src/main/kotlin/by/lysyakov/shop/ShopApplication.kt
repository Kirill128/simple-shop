package by.lysyakov.shop

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories
open class ShopApplication

fun main(args: Array<String>) {
    SpringApplication.run(ShopApplication::class.java, *args)
}
