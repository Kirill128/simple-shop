package by.lysyakov.shop.specification

import by.lysyakov.shop.dto.AttributeSearchCriteria
import by.lysyakov.shop.dto.SearchCriteria
import by.lysyakov.shop.entities.*
import by.lysyakov.shop.entities.FilterOperationType.*
import by.lysyakov.shop.repository.CategoryRepository
import com.fasterxml.jackson.databind.JsonNode
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Root
import org.hibernate.query.TypedParameterValue
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component

@Component
class ProductSpecification(private val categoryRepository: CategoryRepository) {
    fun productSpecification(criteria: SearchCriteria): Specification<Product> =
        Specification.where(categoryId(criteria.categoryId))
                     .and(name(criteria.searchName))
                     .and(priceFrom(criteria.priceFrom))
                     .and(priceTo(criteria.priceTo))
                     .and(attributes(criteria.searchAttributes))
                     .and(visible())


    fun categoryId(id: Long?): Specification<Product>? =
        id?.let {
            Specification { root: Root<Product>, cq: CriteriaQuery<*>, cb: CriteriaBuilder ->
                root.get(Product_.category)
                    .get(Category_.id)
                    .`in`(categoryRepository.findAllChildrenIds(id.toInt()))
            }
        }

    fun name(name: String?): Specification<Product>? =
        name?.takeIf { name.isNotBlank() }
            ?.let { prName ->
                Specification { root: Root<Product>, cq: CriteriaQuery<*>?, cb: CriteriaBuilder ->
                    cb.like(cb.lower(root.get(Product_.shortName)),
                            frontNameToSQLName(prName))
                }
            }

    fun priceFrom(priceFrom: Double?): Specification<Product>? =
        priceFrom?.let {
            Specification { root: Root<Product>, cq: CriteriaQuery<*>?, cb: CriteriaBuilder ->
                cb.greaterThanOrEqualTo(root.get(Product_.price), priceFrom)
            }
        }

    fun priceTo(priceTo: Double?): Specification<Product>? =
        priceTo?.let {
            Specification { root: Root<Product>, cq: CriteriaQuery<*>?, cb: CriteriaBuilder ->
                cb.lessThanOrEqualTo(root.get(Product_.price), priceTo)
            }
        }

    fun visible(): Specification<Product> =
        Specification { root: Root<Product>, cq: CriteriaQuery<*>?, cb: CriteriaBuilder ->
            cb.equal(root.get(Product_.visible), true)
        }

    fun frontNameToSQLName(source: String): String =
        source.split(" +".toRegex())
              .filter { it.isNotBlank() }
              .joinToString("%", "%", "%")

    fun attributes(attrIdToCriteria: List<AttributeSearchCriteria>): Specification<Product>? {
        return attrIdToCriteria.takeIf { it.isNotEmpty() }
            ?.map { criteria ->
                when(criteria.operationType) {
                    GREATER -> attrGreater(criteria.attrId, criteria.getUserValue())
                    LESS -> attrLess(criteria.attrId, criteria.getUserValue())
                    EQUALS -> attrEqual(criteria.attrId, criteria.getUserValue())
                    IN -> attrIn(criteria.attrId, criteria.getUserValue())
                    NOT_IN -> attrNotIn(criteria.attrId, criteria.getUserValue())
                }
            }?.reduce { spec1, spec2 -> spec1.and(spec2) }
    }

    internal fun attrGreater(attrId: Long, userVal: JsonNode): Specification<Product> {
        TODO()
    }

    internal fun attrLess(attrId: Long, userVal: JsonNode): Specification<Product> {
        TODO()
    }

    internal fun attrEqual(attrId: Long, userVal: JsonNode): Specification<Product> {
        TODO()
    }

    internal fun attrIn(attributeId: Long, userSelectedPossibleValuesIds: List<Long>): Specification<Product> {
        return Specification { root: Root<Product>, cq: CriteriaQuery<*>, cb: CriteriaBuilder ->
            val productAttrs = root.join(Product_.productAttributes)

            cb.and(cb.equal(productAttrs.get(AttributePossibleValue_.attribute)
                                        .get(Attribute_.id),
                            attributeId),
                   cb.`in`(productAttrs.get(AttributePossibleValue_.id))
                     .values(userSelectedPossibleValuesIds))
        }
    }

    internal fun attrNotIn(attrId: Long, userVal: JsonNode): Specification<Product> {
        TODO()
    }

    private fun<T> CriteriaBuilder.In<T>.values(values: Iterable<T>): CriteriaBuilder.In<T> =
        this.also {
            values.forEach { this.value(it) }
        }
}