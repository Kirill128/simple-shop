package by.lysyakov.shop.mapper

import by.lysyakov.shop.common.img.ProductImagePathGenerator
import by.lysyakov.shop.common.Switchers
import by.lysyakov.shop.common.dao.SwitcherRepository
import by.lysyakov.shop.dto.AttributeValue
import by.lysyakov.shop.dto.FullInfoProductDto
import by.lysyakov.shop.entities.Product
import by.lysyakov.shop.dto.ProductDto
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
class ProductMapper(
    private val productImagePathParser: ProductImagePathGenerator,
    private val switchers: Switchers
) {

    fun mapToProductDtos(source: List<Product>): List<ProductDto> {
        val imgFolder = switchers.imagesFolderBasePath()
        val defaultImg = switchers.defaultProductImagePath()
        return source.map { product -> product.mapToProductDto(imgFolder, defaultImg)}
    }

    fun mapToFullInfoProductDto(source: Product): FullInfoProductDto {
        return FullInfoProductDto(
            source.shortName,
            productImagePathParser.allImagesRelativePaths(source.id,
                                                          switchers.imagesFolderBasePath(),
                                                          switchers.defaultProductImagePath()),
            source.price,
            source.productAttributes.map { AttributeValue(it.attribute.name, it.value) }
        )
    }

    private fun Product.mapToProductDto(imagesFolderPath: Lazy<String>, defaultImageFileName: Lazy<String>) =
        ProductDto(
            this.id,
            this.shortName,
            productImagePathParser.mainImgRelativePathOrDefault(this.id, imagesFolderPath, defaultImageFileName),
            this.price
        )

    fun mapToProductDtos(source: Page<Product>): Page<ProductDto> {
        val imgFolder = switchers.imagesFolderBasePath()
        val defaultImg = switchers.defaultProductImagePath()
        return source.map {
            ProductDto(it.id,
                       it.shortName,
                       productImagePathParser.mainImgRelativePathOrDefault(it.id, imgFolder, defaultImg),
                       it.price)
        }
    }

    companion object {
        fun Page<Product>.toProductDtos(mapper: ProductMapper) =
            mapper.mapToProductDtos(this)
    }
}


