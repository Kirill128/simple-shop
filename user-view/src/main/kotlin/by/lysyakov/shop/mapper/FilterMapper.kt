package by.lysyakov.shop.mapper

import by.lysyakov.shop.dto.FilterDto
import by.lysyakov.shop.dto.FilterValue
import by.lysyakov.shop.entities.Filter

object FilterMapper {
    fun Filter.toDto() = FilterDto(
        this.title,
        this.attribute.id,
        this.operationType.id,
        this.attributeValues.map { FilterValue(it.id, it.value) }
    )
}