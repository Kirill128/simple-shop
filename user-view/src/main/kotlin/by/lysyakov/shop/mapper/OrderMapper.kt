package by.lysyakov.shop.mapper

import by.lysyakov.shop.common.Switchers
import by.lysyakov.shop.common.img.ProductImagePathGenerator
import by.lysyakov.shop.dto.OrderDto
import by.lysyakov.shop.dto.OrderedProduct
import by.lysyakov.shop.entities.Order
import org.springframework.stereotype.Component

@Component
class OrderMapper(private val productImagePathParser: ProductImagePathGenerator,
                  private val switchers: Switchers
) {
    fun mapToDtos(orders: List<Order>): List<OrderDto> {
        val imagesFolderPath = switchers.imagesFolderBasePath()
        val defaultImageFileName = switchers.defaultProductImagePath()
        return orders.map { it.toDto(imagesFolderPath, defaultImageFileName) }
    }

    fun Order.toDto(imagesFolderPath: Lazy<String>, defaultImageFileName: Lazy<String>) = OrderDto(
        this.id,
        this.startTime,
        this.status.text,
        this.orderProducts.map {
            OrderedProduct(it.product.id,
                           it.count,
                           it.product.shortName,
                           it.priceForOne,
                           productImagePathParser.mainImgRelativePathOrDefault(it.product.id,
                                                                               imagesFolderPath,
                                                                               defaultImageFileName))
        }
    )
}