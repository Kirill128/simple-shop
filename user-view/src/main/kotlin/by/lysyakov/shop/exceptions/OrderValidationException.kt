package by.lysyakov.shop.exceptions

class OrderValidationException(message: String): RuntimeException(message)
