package by.lysyakov.shop.exceptions

class NoSuchProductException(message: String, val invalidIds: Set<Long>): NoSuchElementException(message) {

    companion object {
        const val TAG = "productNotFoundById"
    }
}
