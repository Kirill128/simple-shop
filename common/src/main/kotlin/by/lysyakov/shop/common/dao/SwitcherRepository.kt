package by.lysyakov.shop.common.dao

import by.lysyakov.shop.entities.Switcher
import java.util.Optional
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Component

@Component
interface SwitcherRepository: JpaRepository<Switcher, Int> {
    fun findSwitcherByTag(@Param("tag") tag: String): Optional<Switcher>
}