package by.lysyakov.shop.common.img.impl

import by.lysyakov.shop.common.img.ProductImagePathGenerator
import by.lysyakov.shop.common.util.Extensions.mapToSet
import by.lysyakov.shop.entities.Product
import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths

@Component
class ProductImagePathParserImpl: ProductImagePathGenerator {
    private val productMainImgFileName = "main"

    override fun mainImgAbsolutePath(productId: Long, imagesFolderPath: Lazy<String>): Path =
        Paths.get(imagesFolderPath.value + mainImgRelativePath(productId))

    override fun imageAbsPath(productId: Long, imageName: String, imagesFolderPath: Lazy<String>): Path =
        Paths.get(productImagesFolderAbsPath(productId, imagesFolderPath) + "/$imageName")

    override fun allAdditionalImagesAbsolutePaths(productId: Long, imagesFolderPath: Lazy<String>): Set<Path> {
        return File(productImagesFolderAbsPath(productId, imagesFolderPath))
                .listFiles { file -> file.nameWithoutExtension != productMainImgFileName }
                ?.mapToSet { it.toPath() }
                .orEmpty()
    }

    override fun allImagesRelativePaths(productId: Long, imagesFolderPath: Lazy<String>, defaultImageFileName: Lazy<String>): Set<String> =
        setOf(mainImgRelativePathOrDefault(productId, imagesFolderPath, defaultImageFileName)) +
        allAdditionalImagesRelativePaths(productId, imagesFolderPath)

    override fun allAdditionalImagesRelativePaths(productId: Long, imagesFolderPath: Lazy<String>): Set<String> {
        return File(productImagesFolderAbsPath(productId, imagesFolderPath))
                .listFiles { file -> file.nameWithoutExtension != productMainImgFileName }
                ?.mapToSet { productImagesFolderRelPath(productId) + "/${it.name}" }
                .orEmpty()
    }

    override fun mainImgRelativePathOrDefault(productId: Long, imagesFolderPath: Lazy<String>, defaultImageFileName: Lazy<String>): String = when {
        isMainImgExists(productId, imagesFolderPath) -> mainImgRelativePath(productId)
        else -> defaultImgRelativePath(defaultImageFileName)
    }

    private fun mainImgRelativePath(productId: Long): String =
        "${productImagesFolderRelPath(productId)}/$productMainImgFileName"

    private fun productImagesFolderRelPath(productId: Long) = "/products/$productId"
    private fun productImagesFolderAbsPath(productId: Long, imagesFolderPath: Lazy<String>) =
        imagesFolderPath.value + "/products/$productId"

    private fun isMainImgExists(productId: Long, imagesFolderPath: Lazy<String>) =
        mainImgAbsolutePath(productId, imagesFolderPath).toFile().exists()

    private fun defaultImgRelativePath(defaultImageFileName: Lazy<String>) = "/products/${defaultImageFileName.value}"
}
