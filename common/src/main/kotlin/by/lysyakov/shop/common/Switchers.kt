package by.lysyakov.shop.common

interface Switchers {
    fun defaultProductImagePath(): Lazy<String>
    fun imagesFolderBasePath(): Lazy<String>
}