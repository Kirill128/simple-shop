package by.lysyakov.shop.common.img

import java.nio.file.Path

interface ProductImagePathGenerator {
    fun mainImgAbsolutePath(productId: Long, imagesFolderPath: Lazy<String>): Path
    fun allAdditionalImagesAbsolutePaths(productId: Long, imagesFolderPath: Lazy<String>): Set<Path>
    fun imageAbsPath(productId: Long, imageName: String, imagesFolderPath: Lazy<String>): Path
    fun allImagesRelativePaths(productId: Long, imagesFolderPath: Lazy<String>, defaultImageFileName: Lazy<String>): Set<String>
    fun mainImgRelativePathOrDefault(productId: Long, imagesFolderPath: Lazy<String>, defaultImageFileName: Lazy<String>): String
    fun allAdditionalImagesRelativePaths(productId: Long, imagesFolderPath: Lazy<String>): Set<String>
}