package by.lysyakov.shop.common.mail

import by.lysyakov.shop.entities.Order
import by.lysyakov.shop.entities.Product
import org.springframework.mail.javamail.MimeMessageHelper
import jakarta.mail.internet.MimeMessage

class OrderSavedMailMessage(order: Order, mimeMessage: MimeMessage): MimeMessage(mimeMessage.session) {

    init {
        MimeMessageHelper(this, "utf-8").also { helper ->
            helper.setText(message(order), true)
            helper.setTo(order.email)
            helper.setSubject("Simple Shop - заказ номер ${order.id} принят")
        }
    }

    companion object {
        private val message: (Order) -> String = { order ->
            """
<body>
    <h1>Заказ ${order.id}</h1>
    <table style="max-width: 600px; border-collapse: collapse; border: 2px solid white;">
        <thead>
            <tr>
                ${td("Id")}
                ${td("Название")}
                ${td("Цена за шт. (BYN)")}
                ${td("Кол-во")}
            </tr>
        </thead>
        <tbody>
            ${order.orderProducts.joinToString("") { productRow(it.product, it.count) }}
        </tbody>
    </table>
    <h3>Итого: ${order.orderProducts.sumOf { it.count * it.product.price }} BYN</h3>
</body>
            """
        }
        private val productRow: (Product, Int) -> String = { product, quantity ->
            """
            <tr>
                ${td(product.id)}
                ${td(product.shortName)}
                ${td(product.price)}
                ${td(quantity)}
            </tr>
            """
        }
        private val td: (Any) -> String = {
            """
                <td style="padding: 3px; border: 1px solid maroon; text-align: left;">
                  $it
                </td>
            """
        }
    }
}