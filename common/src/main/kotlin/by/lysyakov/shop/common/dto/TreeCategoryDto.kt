package by.lysyakov.shop.common.dto

import by.lysyakov.shop.common.util.Extensions.mapToSet
import by.lysyakov.shop.entities.Category
import by.lysyakov.shop.entities.Category_
import java.util.Objects

data class TreeCategoryDto(val id: Long? = null,
                           var name: String? = null,
                           var isVisible: Boolean = false,
                           val parentCategory: TreeCategoryDto? = null) {
    var subcategories: Set<TreeCategoryDto> = setOf()

    fun fullName(): String? = parentCategory?.fullName()?.let { "$it/$name" } ?: name

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val that = o as TreeCategoryDto
        return id == that.id
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }

    companion object {
        fun Set<Category>.toCategoriesTree(parentCategory: TreeCategoryDto? = null): Set<TreeCategoryDto> =
            this.mapToSet { category ->
                TreeCategoryDto(
                    category.id,
                    category.name,
                    category.isVisible,
                    parentCategory
                ).also {
                    it.subcategories = category.subCategories.toCategoriesTree(it)
                }
            }

    }
}