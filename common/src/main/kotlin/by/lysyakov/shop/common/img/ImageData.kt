package by.lysyakov.shop.common.img

import java.io.InputStream

data class ImageData(val fileName: String, val inputStream: InputStream)
