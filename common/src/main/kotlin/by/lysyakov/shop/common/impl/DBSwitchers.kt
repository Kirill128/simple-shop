package by.lysyakov.shop.common.impl

import by.lysyakov.shop.common.Switchers
import by.lysyakov.shop.common.dao.SwitcherRepository
import org.springframework.stereotype.Component

@Component
class DBSwitchers(private val switcherRepository: SwitcherRepository): Switchers {
    override fun defaultProductImagePath(): Lazy<String> =
        dbLazyInitValueHolder("defaultProductImagePath")


    override fun imagesFolderBasePath(): Lazy<String> =
        dbLazyInitValueHolder("imagesFolderBasePath")


    fun dbLazyInitValueHolder(tagName: String): Lazy<String> = lazy {
        switcherRepository.findSwitcherByTag(tagName)
                          .map {it.value }
                          .get()
    }

}