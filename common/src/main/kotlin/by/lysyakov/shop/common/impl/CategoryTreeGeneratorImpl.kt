package by.lysyakov.shop.common.impl

import by.lysyakov.shop.common.CategoryTreeGenerator
import by.lysyakov.shop.common.util.Extensions.mapValuesToSet
import by.lysyakov.shop.common.util.Extensions.filterNull
import by.lysyakov.shop.entities.Category
import java.util.Optional
import org.springframework.stereotype.Component

@Component
class CategoryTreeGeneratorImpl: CategoryTreeGenerator {

    override fun generateTree(source: List<Category>): Set<Category> {
        val categoryIdToSubcategories = source.groupBy { Optional.ofNullable(it.parentCategory).map { cat -> cat.id } }
                                              .mapValuesToSet()

        return source.onEach { it.subCategories = categoryIdToSubcategories[Optional.of(it.id)] ?: emptySet() }
                     .filterNull { it.parentCategory }
                     .toSet()

    }

}