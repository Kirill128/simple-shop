package by.lysyakov.shop.common.util

import org.springframework.data.domain.Page

object Extensions {
    fun Any?.isNotNull() = this != null
    fun Any?.isNull() = this == null

    fun <T> T?.ifNull(doIfNull: () -> T): T = this ?: doIfNull()

    fun <T, R> Iterable<T>.mapToSet(mapper: (T) -> R): Set<R> = this.map { mapper(it) }.toSet()
    fun <T> Page<T>.isNotEmpty(): Boolean = this.isEmpty.not()
    fun <T, R> Array<T>.mapToSet(mapper: (T) -> R): Set<R> = this.map { mapper(it) }.toSet()
    inline fun <T, reified R> Iterable<T>.mapToArray(mapper: (T) -> R): Array<R> = this.map { mapper(it) }.toTypedArray()

    fun <T> Iterable<T>.filterNotNull(provider: (T) -> Any?): Iterable<T> = this.filter { provider(it) != null }
    fun <T> Iterable<T>.filterNull(provider: (T) -> Any?): Iterable<T> = this.filter { provider(it) == null }
    fun <T, R> Map<T, Iterable<R>>.mapValuesToSet(): Map<T, Set<R>> = this.mapValues { it.value.toSet() }

}