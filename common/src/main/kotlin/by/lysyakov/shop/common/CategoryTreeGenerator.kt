package by.lysyakov.shop.common

import by.lysyakov.shop.entities.Category

interface CategoryTreeGenerator {
    fun generateTree(source: List<Category>): Set<Category>
}