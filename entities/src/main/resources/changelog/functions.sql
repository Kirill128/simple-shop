create or replace function categories_children(start_parent_category_id integer)
   returns TABLE(child_category_id integer)
   language plpgsql
  as
$$
begin
    return query
        WITH RECURSIVE r (id, parent_category_id, name, level) AS
                           -- Initial Subquery
                           (SELECT id, parent_category_id, name, 1
                            FROM category
                            WHERE id = start_parent_category_id
                            UNION ALL
                            -- Recursive Subquery
                            SELECT t.id, t.parent_category_id, t.name, r.level + 1
                            FROM r
                                INNER JOIN category t ON r.id = t.parent_category_id)
                       -- Result Query
        SELECT id FROM r;
end;
$$;