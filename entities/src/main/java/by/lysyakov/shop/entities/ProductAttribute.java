package by.lysyakov.shop.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

import static by.lysyakov.shop.entities.ProductAttribute.TABLE_NAME;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@IdClass(ProductAttribute.PrimaryKey.class)
@Entity
@Table(name = TABLE_NAME)
public class ProductAttribute {

    public static final String TABLE_NAME = "product_attribute_val";

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attr_val_id")
    private AttributePossibleValue attributeValue;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductAttribute that = (ProductAttribute) o;
        return Objects.equals(product, that.product) && Objects.equals(attributeValue,
                                                                       that.attributeValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, attributeValue);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    public static class PrimaryKey implements Serializable {
        private Product product;
        private AttributePossibleValue attributeValue;
    }
}
