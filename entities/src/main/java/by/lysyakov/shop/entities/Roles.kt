package by.lysyakov.shop.entities

enum class Roles(val id: Long) {
    ADMIN(1),
    MODERATOR(2),
    SELLER(3),
    CUSTOMER(4)
}