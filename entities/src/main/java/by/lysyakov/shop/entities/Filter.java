package by.lysyakov.shop.entities;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Converter;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.BatchSize;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "filter")
public class Filter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    protected Long id;

    @Column
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attr_id")
    @ToString.Exclude
    private Attribute attribute;

    @Column(name = "operation_type_id")
    @Convert(converter = FilterOperationTypeConverter.class)
    private FilterOperationType operationType;

//    @ManyToMany
//    @JoinTable(name = CategoryFilter.TABLE_NAME,
//               joinColumns = @JoinColumn(name = "filter_id"),
//               inverseJoinColumns = @JoinColumn(name = "category_id"))
//    @BatchSize(size = 100)
//    private List<Category> categories;

    @OneToMany(mappedBy = "filter", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CategoryFilter> categoryFilters;

    @OneToMany
    @JoinColumn(name = "attribute_id", referencedColumnName = "attr_id", insertable = false, updatable = false)
    private Set<AttributePossibleValue> attributeValues;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Filter filter = (Filter) o;
        return id != null && Objects.equals(id, filter.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

@Converter
class FilterOperationTypeConverter implements AttributeConverter<FilterOperationType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(FilterOperationType attribute) {
        return attribute.id;
    }

    @Override
    public FilterOperationType convertToEntityAttribute(Integer dbData) {
        return FilterOperationType.of(dbData);
    }
}
