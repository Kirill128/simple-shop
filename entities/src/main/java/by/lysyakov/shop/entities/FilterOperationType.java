package by.lysyakov.shop.entities;

import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public enum FilterOperationType {

    GREATER(1, "GREATER"),
    LESS(2, "LESS"),
    EQUALS(3, "EQUALS"),
    IN(4, "IN"),
    NOT_IN(5, "NOT_IN");

    public final int id;
    public final String code;

    private static final Map<Integer, FilterOperationType> idToOperationType = stream(values())
                                                                                     .collect(toMap(op -> op.id, identity()));

    FilterOperationType(int id, String code) {
        this.id = id;
        this.code = code;
    }

    public static FilterOperationType of(int id) {
        return idToOperationType.get(id);
    }

}
