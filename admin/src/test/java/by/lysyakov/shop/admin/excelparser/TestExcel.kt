package by.lysyakov.shop.admin.excelparser

import by.lysyakov.shop.dto.productsview.ProductsImportFileData
import by.lysyakov.shop.exelparser.core.Transformer
import by.lysyakov.shop.exelparser.core.report.TransformReport
import by.lysyakov.shop.exelparser.grouper.Grouper
import by.lysyakov.shop.parsers.ProductParsableField.DESCRIPTION
import by.lysyakov.shop.parsers.ProductParsableField.ID
import by.lysyakov.shop.parsers.ProductParsableField.NAME
import by.lysyakov.shop.parsers.ProductParsableField.PRICE
import by.lysyakov.shop.parsers.ProductParsingMetadata
import by.lysyakov.shop.parsers.impl.ExcelProductParser
import by.lysyakov.shop.service.productsview.CategoryService
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import java.io.FileInputStream

class TestExcel {

    @Test
    @Disabled
    fun test() {
        val excelProductParser = ExcelProductParser(Transformer(mock(TransformReport::class.java)), Grouper(), mock(CategoryService::class.java))
        try {
            FileInputStream("/home/kirill/Downloads/Telegram Desktop/Прайс_Фото_6Колонка (1).xlsx").use { file ->
                val result = excelProductParser.parse(ProductsImportFileData(file, "", productParsingMetadata))
                println(result)
            }
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    private val productParsingMetadata = ProductParsingMetadata(
        sheet = "Данные",
        firstRow = 5, lastRow = 2728,
        categoryNameColumn = "C",
        columnToFieldMapping = mutableMapOf(
            "A" to ID,
            "B" to NAME,
            "C" to DESCRIPTION,
            "I" to PRICE
        )
    )

}
