package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.Category
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface CategoryRepository: JpaRepository<Category, Long>, CustomizedCategoryRepository {
    @Modifying
    @Query("update Category c set c.name = :newName where c.id = :id")
    fun updateCategoryName(@Param("id") id: Long, @Param("newName") newName: String)

    @Modifying
    @Query("update Category c set c.visible = :newValue where c.id = :id")
    fun updateIsVisible(@Param("id") id: Long, @Param("newValue") newValue: Boolean)

    @Modifying
    @Query("update Category c set c.visible = :newValue where c.id not in (:ids)")
    fun updateIsVisibleIfNotIn(@Param("ids") ids: List<Long>, @Param("newValue") newValue: Boolean)

}

interface CustomizedCategoryRepository {
    fun getTreeWithProducts(): Set<Category>
}
