package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.Attribute
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface AttributeRepository: JpaRepository<Attribute, Long> {

    @Query("from Attribute a left join fetch a.values")
    fun findAllWithValues(): List<Attribute>

}