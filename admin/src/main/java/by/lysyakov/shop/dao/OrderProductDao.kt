package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.OrderProduct
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface OrderProductDao: JpaRepository<OrderProduct, OrderProduct.PrimaryKey> {

    @EntityGraph(attributePaths = ["product", "product.category"])
    fun findAllByOrderIdIsIn(orderIds: List<Long>): List<OrderProduct>

}