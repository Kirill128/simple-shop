package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.ProductAttribute
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ProductAttributeValRepository: JpaRepository<ProductAttribute, ProductAttribute.PrimaryKey> {

    @Modifying
    @Query("delete from ProductAttribute where product.id = :productId and attributeValue.id = :attributeValId")
    fun remove(@Param("productId") productId: Long, @Param("attributeValId") attributeValId: Long)

    @Modifying
    @Query("insert into ${ProductAttribute.TABLE_NAME} values (:productId, :attributeValId)", nativeQuery = true)
    fun save(@Param("productId") productId: Long, @Param("attributeValId") attributeValId: Long)

    @Modifying
    @Query("delete from ProductAttribute where attributeValue.id = :attributeValId")
    fun remove(@Param("attributeValId") attributeValId: Long)

    @Modifying
    @Query("delete from ProductAttribute pa where pa.attributeValue.attribute.id = :attributeId")
    fun removeByAttributeId(@Param("attributeId") attributeId: Long)
}