package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.AttributePossibleValue
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface AttributeValueRepository: JpaRepository<AttributePossibleValue, Long> {

    @Modifying
    @Query("insert into ${AttributePossibleValue.TABLE_NAME} (attribute_id, value) values (:attributeId, :value)", nativeQuery = true)
    fun save(@Param("attributeId") attributeId: Long, @Param("value") value: String): Int

    @Modifying
    @Query("update AttributePossibleValue a set a.visibleOnFilter = :newValue where a.id = :id")
    fun updateIsVisibleOnFilter(@Param("id") id: Long, @Param("newValue") newValue: Boolean)
}