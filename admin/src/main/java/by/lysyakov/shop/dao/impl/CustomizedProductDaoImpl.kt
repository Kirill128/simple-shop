package by.lysyakov.shop.dao.impl

import by.lysyakov.shop.dao.CustomizedProductDao
import by.lysyakov.shop.entities.Product
import by.lysyakov.shop.entities.Product_
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import jakarta.persistence.criteria.CriteriaUpdate
import jakarta.persistence.metamodel.SingularAttribute

class CustomizedProductDaoImpl : CustomizedProductDao {

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun updateNotNullFields(product: Product) {
        val criteriaBuilder = entityManager.criteriaBuilder
        val updateQuery = criteriaBuilder.createCriteriaUpdate(Product::class.java)
        val root = updateQuery.from(Product::class.java)

        val anyValUpdated = setProductFields(product, updateQuery)
        if (!anyValUpdated) return

        updateQuery.where(criteriaBuilder.equal(root[Product_.id], product.id))

        entityManager.createQuery(updateQuery).executeUpdate()
    }

    private fun setProductFields(newProductStateVector: Product, updateQuery: CriteriaUpdate<Product>): Boolean =
        sequenceOf(set(newProductStateVector.shortName, Product_.shortName, updateQuery),
                   set(newProductStateVector.price, Product_.price, updateQuery),
                   set(newProductStateVector.category, Product_.category, updateQuery),
                   set(newProductStateVector.visible, Product_.visible, updateQuery))
            .reduce { acc, next -> acc || next }

    private fun <V> set(value: V?,  attribute: SingularAttribute<Product, V>, criteriaUpdate: CriteriaUpdate<Product>) =
        value?.let {
            criteriaUpdate.set(attribute, it)
            true
        } ?: false

}