package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.Filter
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FilterRepository: JpaRepository<Filter, Long> {
}