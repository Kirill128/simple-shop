package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.Product
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ProductDao: JpaRepository<Product, Long>, JpaSpecificationExecutor<Product>, CustomizedProductDao {

    @EntityGraph(attributePaths = ["category"])
    override fun findAll(pageable: Pageable): Page<Product>

    @Modifying
    @Query("update Product p set p.visible = :newValue where p.id = :id")
    fun updateIsVisible(@Param("id") id: Long, @Param("newValue") newValue: Boolean)

    @Modifying
    @Query("update Product p set p.visible = :newValue where p.id in (:ids)")
    fun updateIsVisibleIfNotIn(@Param("ids") ids: List<Long>, @Param("newValue") newValue: Boolean)
}

interface CustomizedProductDao {
    fun updateNotNullFields(product: Product)
}
