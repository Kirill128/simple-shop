package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.Order
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface OrderDao: JpaRepository<Order, Long> {

    @EntityGraph(attributePaths = ["status"])
    override fun findAll(): List<Order>

    @Modifying
    @Query("update Order o set o.status.id = :newStatusId, o.lastModified = :modificationDate where o.id = :orderId")
    fun updateOrderStatus(@Param("orderId") orderId: Long,
                          @Param("newStatusId") newStatusId: Long,
                          @Param("modificationDate") modificationDate: LocalDateTime)
}