package by.lysyakov.shop.dao

import by.lysyakov.shop.entities.CategoryFilter
import org.springframework.data.jpa.repository.JpaRepository

interface CategoryFilterRepository: JpaRepository<CategoryFilter, CategoryFilter.PrimaryKey>