package by.lysyakov.shop.dao.impl

import by.lysyakov.shop.common.CategoryTreeGenerator
import by.lysyakov.shop.dao.CustomizedCategoryRepository
import by.lysyakov.shop.entities.Category
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext

class CustomizedCategoryRepositoryImpl(private val categoryTreeGenerator: CategoryTreeGenerator): CustomizedCategoryRepository {

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun getTreeWithProducts(): Set<Category> {
        val allCategories = entityManager.createQuery("from Category c left join fetch c.products", Category::class.java)
                                         .resultList
        return categoryTreeGenerator.generateTree(allCategories)
    }

}