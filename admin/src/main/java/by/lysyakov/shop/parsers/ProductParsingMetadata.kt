package by.lysyakov.shop.parsers

import by.lysyakov.shop.exelparser.core.ColumnData
import by.lysyakov.shop.exelparser.core.Metadata
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.CellValueExtractor
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.combiner.ExtractorCombiner.or
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl.DoubleValueExtractor
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl.LongValueExtractor
import by.lysyakov.shop.exelparser.core.cellvalueextractor.extractor.impl.StringValueExtractor
import by.lysyakov.shop.exelparser.core.validation.impl.ValidationRuleFactory.emptyValidationRule
import by.lysyakov.shop.parsers.ProductParsableField.CATEGORY_NAME

data class ProductParsingMetadata(
    val sheet: String,
    val firstRow: Int, val lastRow: Int?,
    val columnToFieldMapping: Map<String, ProductParsableField>,
    val categoryNameColumn: String
) {
    val fieldToIndex = fieldToIndex()

    fun toMetadata(): Metadata<*> {
        val categoryNameExtractor = columnToFieldMapping[categoryNameColumn]
                                                        ?.let { or(it.cellValueExtractor(), CATEGORY_NAME.cellValueExtractor()) }
                                                        ?: CATEGORY_NAME.cellValueExtractor()

        val categoryColumnData = ColumnData(categoryNameExtractor, CATEGORY_NAME.validator())

        return Metadata(sheet,
                        firstRow, lastRow,
                        columnToFieldMapping.mapValues { it.value.columnData } + (categoryNameColumn to categoryColumnData))
    }


    private fun fieldToIndex(): Map<ProductParsableField, Int> {
        val columnToFields = this.columnToFieldMapping.mapValuesTo(mutableMapOf()) { mutableListOf(it.value) }
        columnToFields.computeIfAbsent(categoryNameColumn) { mutableListOf() }.add(CATEGORY_NAME)
        return columnToFields.entries.flatMapIndexed { index, entry -> entry.value.map { it to index } }.toMap(mutableMapOf())
    }
}

enum class ProductParsableField(val columnData: ColumnData<Any>) {
    ID(ColumnData(LongValueExtractor() as CellValueExtractor<Any>, emptyValidationRule())),
    NAME(ColumnData(StringValueExtractor() as CellValueExtractor<Any>, emptyValidationRule())),
    DESCRIPTION(ColumnData(StringValueExtractor() as CellValueExtractor<Any>, emptyValidationRule())),
    PRICE(ColumnData(DoubleValueExtractor() as CellValueExtractor<Any>, emptyValidationRule())),
    CATEGORY_NAME(ColumnData(StringValueExtractor() as CellValueExtractor<Any>, emptyValidationRule()));

    fun cellValueExtractor() = columnData.cellValueExtractor
    fun validator() = columnData.validator
}
