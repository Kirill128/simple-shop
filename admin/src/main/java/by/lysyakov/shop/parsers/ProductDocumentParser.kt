package by.lysyakov.shop.parsers

import by.lysyakov.shop.dto.productsview.ProductsImportFileData
import by.lysyakov.shop.dto.productsview.TreeCategoryDtoWithProducts
import java.io.InputStream

interface ProductDocumentParser {
    fun parse(productsImportFileData: ProductsImportFileData): List<TreeCategoryDtoWithProducts>
}