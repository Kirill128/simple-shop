package by.lysyakov.shop.parsers.impl

import by.lysyakov.shop.dto.productsview.ProductsImportFileData
import by.lysyakov.shop.dto.productsview.TreeCategoryDtoWithProducts
import by.lysyakov.shop.exceptions.FileTypeNotSupported
import by.lysyakov.shop.parsers.ProductDocumentParser
import by.lysyakov.shop.parsers.ProductParsingMetadata
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component
import java.io.InputStream

@Component
@Primary
class ProductsDocumentParsingComponent(private val parsers: List<ProductDocumentParser>): ProductDocumentParser {
    private val log = LoggerFactory.getLogger(ProductsDocumentParsingComponent::class.java)

    override fun parse(productsImportFileData: ProductsImportFileData): List<TreeCategoryDtoWithProducts> {
        parsers.forEach { parser ->
            try {
                return parser.parse(productsImportFileData)
            } catch (e: Exception) {
                log.error("Error parsing file by ${parser.javaClass}. Exception $e.", e)
            }
        }
        throw FileTypeNotSupported("No one parser couldn't parse selected file.")
    }

}