package by.lysyakov.shop.parsers.impl

import by.lysyakov.shop.common.util.Extensions.isNotNull
import by.lysyakov.shop.dto.productsview.ProductDto
import by.lysyakov.shop.dto.productsview.ProductsImportFileData
import by.lysyakov.shop.dto.productsview.TreeCategoryDtoWithProducts
import by.lysyakov.shop.exelparser.core.DataRow
import by.lysyakov.shop.exelparser.core.Transformer
import by.lysyakov.shop.exelparser.grouper.Group
import by.lysyakov.shop.exelparser.grouper.Grouper
import by.lysyakov.shop.parsers.ProductDocumentParser
import by.lysyakov.shop.parsers.ProductParsableField
import by.lysyakov.shop.parsers.ProductParsableField.CATEGORY_NAME
import by.lysyakov.shop.parsers.ProductParsableField.ID
import by.lysyakov.shop.parsers.ProductParsableField.NAME
import by.lysyakov.shop.parsers.ProductParsableField.PRICE
import by.lysyakov.shop.parsers.ProductParsingMetadata
import by.lysyakov.shop.service.productsview.CategoryService
import org.springframework.stereotype.Component
import java.io.InputStream

@Component
class ExcelProductParser(
    private val transformer: Transformer,
    private val grouper: Grouper,
    private val categoryService: CategoryService
) : ProductDocumentParser {

    override fun parse(productsImportFileData: ProductsImportFileData): List<TreeCategoryDtoWithProducts> =
        parse(productsImportFileData.file, productsImportFileData.productsParsingMetadata)

    private fun parse(file: InputStream, productParsingMetadata: ProductParsingMetadata): List<TreeCategoryDtoWithProducts> {
        val categoryIdSelector = categoryIdSelector()
        return grouper.group(transformer.transform(file, productParsingMetadata.toMetadata())
                                        .filter { it.rowData.isNotEmpty() && it.rowData.any { it.isNotNull() } },
                             {  it.isProductRow(productParsingMetadata.fieldToIndex) })
                      .toTreeCategoryDtos(productParsingMetadata.fieldToIndex, categoryIdSelector)

    }

    // если цена спарсилась как тип double, то это 100% продукт
    private fun DataRow.isProductRow(fieldToIndex: Map<ProductParsableField, Int>): Boolean {
        val price: Any? = this.field(PRICE, fieldToIndex)
        return price.isNotNull() && price is Double
    }

    private fun List<Group>.toTreeCategoryDtos(
        fieldToIndex: Map<ProductParsableField, Int>,
        categoryIdSelect: (String, Long?) -> Long?,
        parentCategoryId: Long? = null
    ): List<TreeCategoryDtoWithProducts> {
        return this.map { it.toTreeCategoryDto(fieldToIndex, categoryIdSelect, parentCategoryId) }
    }

    private fun Group.toTreeCategoryDto(
        fieldToIndex: Map<ProductParsableField, Int>,
        categoryIdSelect: (String, Long?) -> Long?,
        parentCategoryId: Long?
    ): TreeCategoryDtoWithProducts {
        val categoryName: String = headRow.field(CATEGORY_NAME, fieldToIndex)
        val categoryId = categoryIdSelect(categoryName, parentCategoryId)
        return TreeCategoryDtoWithProducts(
            id = categoryId,
            name = categoryName.trim(),
            subcategories = this.subgroups.toTreeCategoryDtos(fieldToIndex, categoryIdSelect, categoryId),
            items = items.toProducts(fieldToIndex)
        )
    }

    private fun List<DataRow>.toProducts(fieldToIndex: Map<ProductParsableField, Int>): List<ProductDto> {
        return this.map { it.toProduct(fieldToIndex) }
    }

    private fun DataRow.toProduct(fieldToIndex: Map<ProductParsableField, Int>): ProductDto {
        return ProductDto(
            id = field(ID, fieldToIndex),
            shortName = field<String?>(NAME, fieldToIndex)?.trim(),
            price = field(PRICE, fieldToIndex),
        )
    }

    private fun <T> DataRow.field(fieldEnum: ProductParsableField, fieldToIndex: Map<ProductParsableField, Int>): T =
        fieldToIndex[fieldEnum]?.let { rowData[it] } as T

    private fun categoryIdSelector(): (String, Long?) -> Long? {
        val nameToParentIdToId = categoryService.findAll().groupBy { it.name }
                                                .mapValues { (_, categories) -> categories.associate { it.parentCategoryId to it.id } }
        return { name, parentId -> nameToParentIdToId[name]?.let { it[parentId] } }
    }
}
