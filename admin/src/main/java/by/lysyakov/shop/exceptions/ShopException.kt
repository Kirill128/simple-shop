package by.lysyakov.shop.exceptions

open class ShopException(message: String): RuntimeException(message) {
}