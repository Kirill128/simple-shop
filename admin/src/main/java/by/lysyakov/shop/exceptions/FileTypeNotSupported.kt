package by.lysyakov.shop.exceptions

class FileTypeNotSupported(message: String) : ShopException(message) {
}