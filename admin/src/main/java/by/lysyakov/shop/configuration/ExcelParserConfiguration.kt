package by.lysyakov.shop.configuration

import by.lysyakov.shop.exelparser.core.Transformer
import by.lysyakov.shop.exelparser.core.report.TransformReport
import by.lysyakov.shop.exelparser.grouper.Grouper
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ExcelParserConfiguration {

    @Bean
    open fun excelTransformer() = Transformer(object : TransformReport {
        private val log = LoggerFactory.getLogger(TransformReport::class.java)

        override fun close() {}
        override fun hasErrors(): Boolean = false
        override fun error(row: Int, column: Int, message: String?) {
            log.error("Error on parsing excel: row '$row', column '$column', message '$message'.")
        }
    })

    @Bean
    open fun grouper() = Grouper()
}