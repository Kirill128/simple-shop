package by.lysyakov.shop.service.productsview.impl

import by.lysyakov.shop.common.util.Extensions.isNotNull
import by.lysyakov.shop.dao.CategoryRepository
import by.lysyakov.shop.dao.ProductDao
import by.lysyakov.shop.dto.productsview.ProductDto
import by.lysyakov.shop.dto.productsview.ProductsImportFileData
import by.lysyakov.shop.dto.productsview.TreeCategoryDtoWithProducts
import by.lysyakov.shop.entities.Category
import by.lysyakov.shop.entities.Product
import by.lysyakov.shop.export.ProductsToFileExporter
import by.lysyakov.shop.mapper.ProductMapper
import by.lysyakov.shop.parsers.ProductDocumentParser
import by.lysyakov.shop.service.productsview.ProductService
import by.lysyakov.shop.util.ProductImageSaver
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.io.InputStream


@Service
open class ProductServiceImpl(
    private val productDao: ProductDao,
    private val imageSaver: ProductImageSaver,
    private val productMapper: ProductMapper,
    private val productDocumentParser: ProductDocumentParser,
    private val productsToFileExporter: ProductsToFileExporter,
    private val categoryDao: CategoryRepository
//    private val attributeValueService: AttributeValueService
) : ProductService {

//    override fun allProductsWithAttributes(): ProductsAndAttributes {
//        val (allAttributes, allAttributeValues) = attributeValueService.findAllAttrValueFullInfo()
//        val allProducts = productMapper.toProductViewProductWithAttrDtos(productDao.findAll())
//        return ProductsAndAttributes(allProducts, allAttributes, allAttributeValues)
//    }

    @Transactional(readOnly = true)
    override fun findAll(): List<ProductDto> = productMapper.toProductViewProductDtos(productDao.findAll())

//    override fun findAllByPageRequest(pageRequest: PageRequest): Page<ProductDto> =
//        productMapper.toProductViewProductDtos(productDao.findAll(pageRequest))
//

    @Transactional
    override fun save(source: ProductDto) {
        productDao.save(productMapper.toProduct(source))
    }

    @Transactional
    override fun delete(id: Long) {
        productDao.deleteById(id)
    }

    @Transactional
    override fun updateVisible(productId: Long, newValue: Boolean) {
        productDao.updateIsVisible(productId, newValue)
    }

    @Transactional
    override fun saveWithImage(source: ProductDto) {
        var product = productMapper.toProduct(source)
        when {
            product.id.isNotNull() -> productDao.updateNotNullFields(product)
            else -> product = productDao.save(product)
        }

        imageSaver.save(product.id, source.mainImageFile, source.additionalImagesFiles)
    }

    @Transactional
    override fun importTree(categoriesTree: List<TreeCategoryDtoWithProducts>) {
        categoriesTree.allCategoriesIds().takeIf { it.isNotEmpty() }?.let { categoryDao.updateIsVisibleIfNotIn(it, false) }
        categoriesTree.allProductsIds().takeIf { it.isNotEmpty() }?.let { productDao.updateIsVisibleIfNotIn(it, false) }

        categoriesTree.importTree()
    }

    private fun List<TreeCategoryDtoWithProducts>.allCategoriesIds(): List<Long> {
        return this.flatMap { it.subcategories.allCategoriesIds() + listOfNotNull(it.id) }
    }

    private fun List<TreeCategoryDtoWithProducts>.allProductsIds(): List<Long> {
        return this.flatMap { it.subcategories.allProductsIds() + it.items.mapNotNull { it.id } }
    }

    private fun List<TreeCategoryDtoWithProducts>.importTree(parentCategory: Category? = null) {
        this.forEach {
            val savedCategory = categoryDao.save(Category.builder()
                                                         .id(it.id)
                                                         .name(it.name)
                                                         .visible(true)
                                                         .parentCategory(parentCategory)
                                                         .build())
            it.subcategories.importTree(savedCategory)
            it.items.import(savedCategory)
        }
    }

    private fun List<ProductDto>.import(category: Category) {
        val (existsProducts, newProducts) = this.partition { it.id.isNotNull() }
        existsProducts.forEach { productDao.updateNotNullFields(it.toProduct(category)) }
        newProducts.forEach { productDao.save(it.toProduct(category)) }
    }

    private fun ProductDto.toProduct(category: Category): Product =
        Product.builder()
               .id(this.id)
               .shortName(this.shortName)
               .price(this.price)
               .visible(true)
               .category(category)
               .build()

    override fun parserFromDocument(productsImportFileData: ProductsImportFileData): List<TreeCategoryDtoWithProducts> =
        productDocumentParser.parse(productsImportFileData)

    @Transactional(readOnly = true)
    override fun exportToFile(fileType: String): InputStream {
        return productsToFileExporter.export(fileType)
    }
}
