package by.lysyakov.shop.service.productsview

import by.lysyakov.shop.common.dto.TreeCategoryDto
import by.lysyakov.shop.dto.productsview.CategoryDto
import by.lysyakov.shop.dto.productsview.SaveCategoryDto

interface CategoryService {
    fun findAll(): List<CategoryDto>
    fun getCategoriesTree(): Set<TreeCategoryDto>
    fun save(saveCategoryDto: SaveCategoryDto): CategoryDto
    fun updateCategoryName(id: Long, newName: String)
    fun remove(id: Long)
    fun updateVisible(id: Long, newValue: Boolean)
}