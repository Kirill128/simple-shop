package by.lysyakov.shop.service.productsview

import by.lysyakov.shop.dto.productsview.OrderDto
import by.lysyakov.shop.dto.productsview.OrderStatus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.time.LocalDateTime

interface OrderService {

    fun findPage(page: Pageable): Page<OrderDto>

    fun allOrdersCount(): Long

    fun updateOrderStatus(orderId: Long, newStatus: OrderStatus, modificationDate: LocalDateTime)
}