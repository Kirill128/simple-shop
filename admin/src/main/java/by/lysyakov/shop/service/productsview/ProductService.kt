package by.lysyakov.shop.service.productsview

import by.lysyakov.shop.dto.productsview.ProductDto
import by.lysyakov.shop.dto.productsview.ProductsImportFileData
import by.lysyakov.shop.dto.productsview.TreeCategoryDtoWithProducts
import by.lysyakov.shop.parsers.ProductParsingMetadata
import java.io.InputStream

interface ProductService {
//    fun allProductsWithAttributes(): ProductsAndAttributes
    fun findAll(): List<ProductDto>
//    fun findAllByPageRequest(pageRequest: PageRequest): Page<ProductDto>
    fun save(source: ProductDto)
    fun delete(id: Long)
    fun updateVisible(productId: Long, newValue: Boolean)
    fun saveWithImage(source: ProductDto)
    fun parserFromDocument(productsImportFileData: ProductsImportFileData): List<TreeCategoryDtoWithProducts>
    fun importTree(categoriesTree: List<TreeCategoryDtoWithProducts>)
    fun exportToFile(fileType: String): InputStream
}