package by.lysyakov.shop.service.productsview.impl

import by.lysyakov.shop.common.util.Extensions.mapToSet
import by.lysyakov.shop.dao.AttributeRepository
import by.lysyakov.shop.dao.AttributeValueRepository
import by.lysyakov.shop.dao.ProductAttributeValRepository
import by.lysyakov.shop.dto.productsview.AttributeStatistic
import by.lysyakov.shop.dto.productsview.AttributeDto
import by.lysyakov.shop.mapper.AttributeMapper.toAttribute
import by.lysyakov.shop.mapper.AttributeMapper.toAttributeDto
import by.lysyakov.shop.mapper.AttributeMapper.toAttributeDtoWithValues
import by.lysyakov.shop.service.productsview.AttributeService
import by.lysyakov.shop.service.productsview.AttributeValueService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class AttributeServiceImpl(private val attributeRepository: AttributeRepository,
                                private val attributeValueRepository: AttributeValueRepository,
                                private val productAttributeValRepository: ProductAttributeValRepository) : AttributeService {
    override fun findAll(): Map<Long, AttributeDto> {
        return attributeRepository.findAll()
                                  .map { it.toAttributeDto() }
                                  .associateBy { it.id!! }
    }

    @Transactional(readOnly = true)
    override fun findAllWithValues(): List<AttributeDto> {
        val allAttributes = attributeRepository.findAllWithValues()
        val attrStatistics = productAttributeValRepository.findAll()
                                                          .groupBy { it.attributeValue.id }
                                                          .mapValues { (_, productAttr) -> AttributeStatistic(productAttr.mapToSet { it.product.id }) }
        return allAttributes.map { it.toAttributeDtoWithValues(attrStatistics) }
    }

    @Transactional
    override fun save(newAttribute: AttributeDto): AttributeDto =
        attributeRepository.save(newAttribute.toAttribute())
                           .toAttributeDto()
                           .also { it.values = newAttribute.values }

    @Transactional
    override fun delete(attributeId: Long) {
        attributeRepository.deleteById(attributeId)
    }
}