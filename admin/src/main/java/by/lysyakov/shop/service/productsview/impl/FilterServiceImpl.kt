package by.lysyakov.shop.service.productsview.impl

import by.lysyakov.shop.dao.CategoryRepository
import by.lysyakov.shop.dao.CategoryFilterRepository
import by.lysyakov.shop.dao.FilterRepository
import by.lysyakov.shop.dto.productsview.FilterDto
import by.lysyakov.shop.dto.productsview.FiltersViewData
import by.lysyakov.shop.mapper.FilterMapper.toDto
import by.lysyakov.shop.mapper.FilterMapper.toDtos
import by.lysyakov.shop.mapper.FilterMapper.toFilter
import by.lysyakov.shop.service.productsview.AttributeService
import by.lysyakov.shop.service.productsview.CategoryService
import by.lysyakov.shop.service.productsview.FilterService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class FilterServiceImpl(private val filterRepository: FilterRepository,
                             private val categoryFilterRepository: CategoryFilterRepository,
                             private val categoryRepository: CategoryRepository,
                             private val attributeService: AttributeService,
                             private val categoryService: CategoryService): FilterService {

    override fun findAllWithAttrs(): FiltersViewData {
        val attributes = attributeService.findAll()
        val categories = categoryService.findAll().associateBy { it.id }
        val filters = filterRepository.findAll().toDtos({ attributes[it]!! }, { categories[it]!! })
        return FiltersViewData(filters, attributes.values, categories.values)
    }

    @Transactional
    override fun save(filter: FilterDto): FilterDto {
        return filterRepository.save(filter.toFilter { categoryRepository.getReferenceById(it) })
                               .toDto(filter)
    }

    @Transactional
    override fun remove(filterId: Long) {
        filterRepository.deleteById(filterId)
    }
}