package by.lysyakov.shop.service.productsview.impl

import by.lysyakov.shop.dao.ProductAttributeValRepository
import by.lysyakov.shop.dto.productsview.AttributeValueDto
import by.lysyakov.shop.service.productsview.AttributeService
import by.lysyakov.shop.service.productsview.AttributeValueService
import by.lysyakov.shop.service.productsview.ProductAttributeValueService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class ProductAttributeValueServiceImpl(
    private val productAttrValRepository: ProductAttributeValRepository,
    private val attributeValueService: AttributeValueService,
    private val attributeService: AttributeService,
) : ProductAttributeValueService {

    @Transactional
    override fun addWithNewAttrAndVal(productId: Long, attributeValue: AttributeValueDto): AttributeValueDto {
        val newAttrVal = attributeValueService.save(attributeValue)
        productAttrValRepository.save(productId, newAttrVal.id!!)
        return newAttrVal
    }
    @Transactional
    override fun addWithNewAttrVal(productId: Long, attributeValue: AttributeValueDto): AttributeValueDto {
        val newAttrVal = attributeValueService.save(attributeValue)
        productAttrValRepository.save(productId, newAttrVal.id!!)
        return newAttrVal
    }

    @Transactional
    override fun addExisting(productId: Long, attributeValueId: Long) {
        productAttrValRepository.save(productId, attributeValueId)
    }

    @Transactional
    override fun remove(productId: Long, attributeValueId: Long) {
        productAttrValRepository.remove(productId, attributeValueId)
    }
}