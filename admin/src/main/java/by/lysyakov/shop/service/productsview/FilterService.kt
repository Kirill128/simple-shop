package by.lysyakov.shop.service.productsview

import by.lysyakov.shop.dto.productsview.FilterDto
import by.lysyakov.shop.dto.productsview.FiltersViewData

interface FilterService {

    fun findAllWithAttrs(): FiltersViewData

    fun save(filter: FilterDto): FilterDto

    fun remove(filterId: Long)

}