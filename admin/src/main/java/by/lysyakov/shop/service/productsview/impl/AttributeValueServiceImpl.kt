package by.lysyakov.shop.service.productsview.impl

import by.lysyakov.shop.common.util.Extensions.isNotNull
import by.lysyakov.shop.dao.AttributeRepository
import by.lysyakov.shop.dao.AttributeValueRepository
import by.lysyakov.shop.dao.ProductAttributeValRepository
import by.lysyakov.shop.dto.productsview.AttributeDto
import by.lysyakov.shop.dto.productsview.AttributeValueDto
import by.lysyakov.shop.dto.productsview.AttributeValuesInfo
import by.lysyakov.shop.mapper.AttributeMapper.toAttribute
import by.lysyakov.shop.mapper.AttributeValueMapper.toAttributePossibleValue
import by.lysyakov.shop.mapper.AttributeValueMapper.toAttributeValueDto
import by.lysyakov.shop.service.productsview.AttributeService
import by.lysyakov.shop.service.productsview.AttributeValueService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class AttributeValueServiceImpl(
    private val attributeValueRepository: AttributeValueRepository,
    private val attributeService: AttributeService,
    private val attributeRepository: AttributeRepository,
    private val productAttributeValRepository: ProductAttributeValRepository
) : AttributeValueService {

    override fun findAllAttrValueFullInfo(): AttributeValuesInfo {
        val attributes = attributeService.findAll()
        return AttributeValuesInfo(attributes.toMutableMap(), findAll(attributes).toMutableMap())
    }

    private fun findAll(attributes: Map<Long, AttributeDto>): Map<Long, AttributeValueDto> =
        attributeValueRepository.findAll()
                                .map { it.toAttributeValueDto(attributes) }
                                .associateBy { it.id!! }

    @Transactional
    override fun save(newValue: AttributeValueDto): AttributeValueDto {
        if (newValue.attribute?.id.isNotNull()) {
            return attributeValueRepository.save(newValue.toAttributePossibleValue { attributeRepository.getReferenceById(it) })
                                           .toAttributeValueDto()
        }
        val savedAttribute = attributeRepository.save(newValue.attribute!!.toAttribute())
        return attributeValueRepository.save(newValue.toAttributePossibleValue { savedAttribute })
                                       .toAttributeValueDto()
    }

    @Transactional
    override fun updateIsVisibleOnFilter(attrValId: Long, newValue: Boolean) {
        attributeValueRepository.updateIsVisibleOnFilter(attrValId, newValue)
    }

    @Transactional
    override fun delete(id: Long) {
        productAttributeValRepository.remove(id)
        attributeValueRepository.deleteById(id)
    }
}