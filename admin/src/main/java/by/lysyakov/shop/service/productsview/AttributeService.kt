package by.lysyakov.shop.service.productsview

import by.lysyakov.shop.dto.productsview.AttributeDto

interface AttributeService {
    fun findAll(): Map<Long, AttributeDto>
    fun findAllWithValues(): List<AttributeDto>
    fun save(newAttribute: AttributeDto): AttributeDto
    fun delete(attributeId: Long)
}