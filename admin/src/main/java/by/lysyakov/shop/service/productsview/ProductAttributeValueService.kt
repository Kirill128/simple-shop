package by.lysyakov.shop.service.productsview

import by.lysyakov.shop.dto.productsview.AttributeValueDto

interface ProductAttributeValueService {

    /**
     * Attribute and value NOT exists
     */
    fun addWithNewAttrAndVal(productId: Long, attributeValue: AttributeValueDto): AttributeValueDto

    /**
     * Attribute exists, but value not
     */
    fun addWithNewAttrVal(productId: Long, attributeValue: AttributeValueDto): AttributeValueDto

    /**
     * Attribute and value exists
     */
    fun addExisting(productId: Long, attributeValueId: Long)
    fun remove(productId: Long, attributeValueId: Long)
}