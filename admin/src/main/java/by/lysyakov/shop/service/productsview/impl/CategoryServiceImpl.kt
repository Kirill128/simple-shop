package by.lysyakov.shop.service.productsview.impl

import by.lysyakov.shop.common.CategoryTreeGenerator
import by.lysyakov.shop.common.dto.TreeCategoryDto
import by.lysyakov.shop.common.dto.TreeCategoryDto.Companion.toCategoriesTree
import by.lysyakov.shop.dao.CategoryRepository
import by.lysyakov.shop.dto.productsview.CategoryDto
import by.lysyakov.shop.dto.productsview.SaveCategoryDto
import by.lysyakov.shop.mapper.CategoryMapper.toProductViewCategoryDto
import by.lysyakov.shop.mapper.CategoryMapper.toProductViewCategoryDtos
import by.lysyakov.shop.service.productsview.CategoryService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class CategoryServiceImpl(
    private val categoryDao: CategoryRepository,
    private val categoryTreeGenerator: CategoryTreeGenerator
): CategoryService {

    override fun findAll(): List<CategoryDto> =
        categoryDao.findAll()
                   .toProductViewCategoryDtos()


    override fun getCategoriesTree(): Set<TreeCategoryDto> =
        categoryTreeGenerator.generateTree(categoryDao.findAll())
                             .toCategoriesTree()

    @Transactional
    override fun save(saveCategoryDto: SaveCategoryDto): CategoryDto =
        categoryDao.save(saveCategoryDto.toCategory())
                   .toProductViewCategoryDto()


    @Transactional
    override fun updateCategoryName(id: Long, newName: String) {
        categoryDao.updateCategoryName(id, newName)
    }

    @Transactional
    override fun remove(id: Long) {
        categoryDao.deleteById(id)
    }

    @Transactional
    override fun updateVisible(id: Long, newValue: Boolean) {
        categoryDao.updateIsVisible(id, newValue)
    }
}
