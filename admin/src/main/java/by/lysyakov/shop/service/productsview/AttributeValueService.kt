package by.lysyakov.shop.service.productsview

import by.lysyakov.shop.dto.productsview.AttributeValueDto
import by.lysyakov.shop.dto.productsview.AttributeValuesInfo

interface AttributeValueService {
    fun findAllAttrValueFullInfo(): AttributeValuesInfo
    fun save(newValue: AttributeValueDto): AttributeValueDto
    fun delete(id: Long)
    fun updateIsVisibleOnFilter(attrValId: Long, newValue: Boolean)
}