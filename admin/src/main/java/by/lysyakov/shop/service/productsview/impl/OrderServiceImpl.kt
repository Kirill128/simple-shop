package by.lysyakov.shop.service.productsview.impl

import by.lysyakov.shop.dao.OrderDao
import by.lysyakov.shop.dao.OrderProductDao
import by.lysyakov.shop.dto.productsview.OrderDto
import by.lysyakov.shop.dto.productsview.OrderStatus
import by.lysyakov.shop.mapper.OrderMapper.toOrderDto
import by.lysyakov.shop.service.productsview.OrderService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
open class OrderServiceImpl(
    private val orderDao: OrderDao,
    private val orderProductDao: OrderProductDao
): OrderService {

    override fun findPage(page: Pageable): Page<OrderDto> {
        val orders = this.orderDao.findAll(page)
        val ordersProducts = this.orderProductDao.findAllByOrderIdIsIn(orders.content.map { it.id })
                                 .groupBy { it.order.id }
        return orders.map { it.toOrderDto { ordersProducts.getOrDefault(it, emptyList()) } }
    }

    override fun allOrdersCount() = orderDao.count()

    @Transactional
    override fun updateOrderStatus(orderId: Long, newStatus: OrderStatus, modificationDate: LocalDateTime) {
        orderDao.updateOrderStatus(orderId, newStatus.id, modificationDate)
    }
}