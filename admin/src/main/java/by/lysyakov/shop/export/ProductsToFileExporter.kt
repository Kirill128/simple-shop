package by.lysyakov.shop.export

import java.io.InputStream

interface ProductsToFileExporter {
    fun export(fileType: String): InputStream
}