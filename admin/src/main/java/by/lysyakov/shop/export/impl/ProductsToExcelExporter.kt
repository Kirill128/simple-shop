package by.lysyakov.shop.export.impl

import by.lysyakov.shop.dao.CategoryRepository
import by.lysyakov.shop.entities.Category
import by.lysyakov.shop.entities.Product
import by.lysyakov.shop.export.ProductsToFileExporter
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream

@Component
class ProductsToExcelExporter(private val categoryDao: CategoryRepository) : ProductsToFileExporter {

    override fun export(fileType: String): InputStream {
        val newWorkbook = WorkbookFactory.create(true)
        val dataSheet = newWorkbook.createSheet("Data")

        val firstRow = 0
        saveHeadRow(dataSheet, firstRow)
        categoryDao.getTreeWithProducts().saveTo(dataSheet, firstRow + 1)

        return newWorkbook.toInputStreamAndClose()
    }

    private fun Iterable<Category>.saveTo(sheet: Sheet, firstRow: Int): Int {
        var nextAvailableRowIndex = firstRow
        this.map { category ->
            saveCategoryDataAsRow(category.name, sheet, nextAvailableRowIndex++)
            val groupFrom = nextAvailableRowIndex
            nextAvailableRowIndex = category.subCategories.saveTo(sheet, nextAvailableRowIndex)
            nextAvailableRowIndex = category.products.saveTo(sheet, nextAvailableRowIndex)
            sheet.groupRow(groupFrom, nextAvailableRowIndex - 1)
        }
        return nextAvailableRowIndex
    }

    private fun saveHeadRow(sheet: Sheet, index: Int) {
        val headRow = sheet.createRow(index)

        headRow.createCell(productIdColumnIdx).setCellValue("Id")
        headRow.createCell(productNameColumnIdx).setCellValue("Name")
        headRow.createCell(productPriceColumnIdx).setCellValue("Price")
    }

    private fun saveCategoryDataAsRow(categoryName: String, sheet: Sheet, nextRowIndex: Int) {
        sheet.createRow(nextRowIndex)
             .createCell(categoryNameColumnIdx)
             .setCellValue(categoryName)
    }

    private fun Collection<Product>.saveTo(sheet: Sheet, nextRowIndex: Int): Int {
        return this.onEachIndexed { index, product -> saveProductDataAsRow(product , sheet, index + nextRowIndex) }
                   .size + nextRowIndex
    }

    private fun saveProductDataAsRow(product: Product, sheet: Sheet, index: Int) {
        val row = sheet.createRow(index)

        row.createCell(productIdColumnIdx).setCellValue(product.id.toString())
        row.createCell(productNameColumnIdx).setCellValue(product.shortName)
        row.createCell(productPriceColumnIdx).setCellValue(product.price)
    }

    private fun Workbook.toInputStreamAndClose(): InputStream {
        val outputStream = ByteArrayOutputStream(10_000_000)
        this.write(outputStream)
        this.close()
        return ByteArrayInputStream(outputStream.toByteArray())
    }

    companion object {
        private val productIdColumnIdx = 0
        private val productNameColumnIdx = 1
        private val productPriceColumnIdx = 2
        private val categoryNameColumnIdx = 1
    }
}