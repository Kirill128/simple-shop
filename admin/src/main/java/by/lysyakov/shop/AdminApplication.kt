package by.lysyakov.shop

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories
open class AdminApplication

fun main(args: Array<String>) {
    SpringApplication.run(AdminApplication::class.java, *args)
}
