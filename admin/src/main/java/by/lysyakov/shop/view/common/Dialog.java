package by.lysyakov.shop.view.common;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import java.util.function.Consumer;

public abstract class Dialog<T> extends com.vaadin.flow.component.dialog.Dialog {
    protected final Consumer<T> afterSuccessAction;

    private Button ok = new Button("Ok");
    private Button cancel = new Button("Cancel", event -> this.close());
    protected T context;

    public Dialog(Consumer<T> afterSuccessAction) {
        this.afterSuccessAction = afterSuccessAction;

        ok.addClickListener(buttonOKClickListener());
        add(header(), content(), new HorizontalLayout(ok, cancel));
    }

    public void open(T context) {
        this.context = context;
        this.open();
    }

    protected abstract ComponentEventListener<ClickEvent<Button>> buttonOKClickListener();

    protected abstract Component content();

    protected abstract Component header();
}