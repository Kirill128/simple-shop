package by.lysyakov.shop.view.products.save;

import by.lysyakov.shop.dto.productsview.ImageFileActionInfo;
import by.lysyakov.shop.view.common.MultiFileMemoryBuffer;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.List;

class ImageList extends VerticalLayout {

    public ImageList() {
    }

    public void addImages(Long productId, MultiFileMemoryBuffer buffer) {
        buffer.allSaveImageFileActionInfos()
              .forEach(fileInfo -> this.add(new RemovableProductImage(productId, fileInfo)));
    }

    public void setImages(Long productId, List<ImageFileActionInfo> files) {
        this.removeAll();
        files.forEach(fileInfo -> this.add(new RemovableProductImage(productId, fileInfo)));
    }

    public List<ImageFileActionInfo> imageFileInfos() {
        return this.getChildren()
                   .filter(child -> child instanceof RemovableProductImage)
                   .map(child -> ((RemovableProductImage) child).imageFileActionInfo())
                   .toList();
    }

}