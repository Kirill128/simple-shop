package by.lysyakov.shop.view.importview;

import by.lysyakov.shop.dto.productsview.ProductDto;
import by.lysyakov.shop.dto.productsview.ProductsImportFileData;
import by.lysyakov.shop.dto.productsview.TreeCategoryDtoWithProducts;
import by.lysyakov.shop.service.productsview.ProductService;
import by.lysyakov.shop.view.common.AdminPage;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static by.lysyakov.shop.view.common.AdminPage.PageType.IMPORT;
import static java.util.Collections.emptyList;

@Route("/import")
public class ImportView extends AdminPage {

    private UploadProductsDocumentDialog uploadProductsDocumentDialog;
    private ProductService productService;

    private TreeGrid<TreeCategoryDtoWithProducts> categoriesAndProductsTree = new TreeGrid<>(){{
        addHierarchyColumn(TreeCategoryDtoWithProducts::getId).setHeader("Id");
        addColumn(TreeCategoryDtoWithProducts::getName).setHeader("Name");
        setSizeFull();
        addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
        setItemDetailsRenderer(new ComponentRenderer<>(ImportView::productDtoGrid));
    }};

    private Button save = new Button("Save", event -> {
        productService.importTree(categoriesAndProductsTree.getTreeData()
                                                           .getRootItems());
        categoriesAndProductsTree.setItems(emptyList(), TreeCategoryDtoWithProducts::getSubcategories);
    });

    public ImportView(ProductService productService) {
        this.productService = productService;
        this.uploadProductsDocumentDialog = new UploadProductsDocumentDialog(this::parseProductsDocument);
        setContent(categoriesAndProductsTree);
        addToNavbar(uploadDocumentBtn(), save);
    }

    private void parseProductsDocument(ProductsImportFileData importFileData) {
        categoriesAndProductsTree.setItems(productService.parserFromDocument(importFileData),
                                           TreeCategoryDtoWithProducts::getSubcategories);
    }

    private Button uploadDocumentBtn() {
        return new Button("Upload", event -> uploadProductsDocumentDialog.open());
    }

    private static Grid<ProductDto> productDtoGrid(TreeCategoryDtoWithProducts source) {
        return productDtoGrid(source.getItems());
    }
    private static Grid<ProductDto> productDtoGrid(List<ProductDto> source) {
        return new Grid<>(){{
            addColumn(ProductDto::getId).setHeader("Id");
            addColumn(ProductDto::getShortName).setHeader("Name");
            addColumn(ProductDto::getPrice).setHeader("Price");
            setItems(source);
            setWidthFull();
            addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
        }};
    }

    @NotNull
    @Override
    protected PageType getPageType() {
        return IMPORT;
    }

}
