package by.lysyakov.shop.view.attributesview;

import by.lysyakov.shop.dto.productsview.AttributeDto;
import by.lysyakov.shop.dto.productsview.AttributeValueDto;
import by.lysyakov.shop.service.productsview.AttributeService;
import by.lysyakov.shop.service.productsview.AttributeValueService;
import by.lysyakov.shop.view.common.Dialog;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;

import java.util.function.Consumer;

class Dialogs {

    static class AddEditAttributeDialog extends Dialog<AttributeDto> {
        private final AttributeService attributeService;
        private TextField title;
        private Binder<AttributeDto> binder = new Binder<>(AttributeDto.class){{
            forField(title).bind(AttributeDto::getName, AttributeDto::setName);
        }};

        public AddEditAttributeDialog(Consumer<AttributeDto> afterSuccessAction, AttributeService attributeService) {
            super(afterSuccessAction);
            this.attributeService = attributeService;
        }

        @Override
        public void open(AttributeDto context) {
            super.open(context);
            if (context != null) binder.readBean(context);
        }

        @Override
        protected ComponentEventListener<ClickEvent<Button>> buttonOKClickListener() {
            return event -> {
                try {
                    binder.writeBean(context);
                    afterSuccessAction.accept(attributeService.save(context));
                    this.close();
                } catch (ValidationException ex) {
                    ex.printStackTrace();
                }
            };
        }

        @Override
        protected Component content() {
            title = new TextField("Input name");
            return title;
        }

        @Override
        protected Component header() {
            return new H5("New attribute name");
        }
    }

    static class AddEditAttributeValueDialog extends Dialog<AttributeValueDto> {
        private final AttributeValueService attributeValueService;
        private TextField title;
        private Binder<AttributeValueDto> binder = new Binder<>(AttributeValueDto.class){{
            forField(title).bind(AttributeValueDto::getValue, AttributeValueDto::setValue);
        }};

        public AddEditAttributeValueDialog(Consumer<AttributeValueDto> afterSuccessAction, AttributeValueService attributeValueService) {
            super(afterSuccessAction);
            this.attributeValueService = attributeValueService;
        }

        @Override
        public void open(AttributeValueDto context) {
            super.open(context);
            if (context != null) binder.readBean(context);
        }

        @Override
        protected ComponentEventListener<ClickEvent<Button>> buttonOKClickListener() {
            return event -> {
                try {
                    binder.writeBean(context);
                    afterSuccessAction.accept(attributeValueService.save(context));
                    this.close();
                } catch (ValidationException ex) {
                    ex.printStackTrace();
                }
            };
        }

        @Override
        protected Component content() {
            title = new TextField("Input Value");
            return title;
        }

        @Override
        protected Component header() {
            return new H5("New attribute value");
        }
    }

    static class DeleteAttributeDialog extends Dialog<AttributeDto> {
        private final AttributeService attributeService;

        public DeleteAttributeDialog(Consumer<AttributeDto> afterSuccessAction, AttributeService attributeService) {
            super(afterSuccessAction);
            this.attributeService = attributeService;
        }

        @Override
        protected ComponentEventListener<ClickEvent<Button>> buttonOKClickListener() {
            return event -> {
                attributeService.delete(context.getId());
                afterSuccessAction.accept(context);
                this.close();
            };
        }

        @Override
        protected Component content() {
            return new H5("Are you sure ?");
        }

        @Override
        protected Component header() {
            return new H5("Delete Attribute");
        }
    }

    static class DeleteAttributeValueDialog extends Dialog<AttributeValueDto> {
        private final AttributeValueService attributeService;

        public DeleteAttributeValueDialog(Consumer<AttributeValueDto> afterSuccessAction, AttributeValueService attributeService) {
            super(afterSuccessAction);
            this.attributeService = attributeService;
        }

        @Override
        protected ComponentEventListener<ClickEvent<Button>> buttonOKClickListener() {
            return event -> {
                attributeService.delete(context.getId());
                afterSuccessAction.accept(context);
                this.close();
            };
        }

        @Override
        protected Component content() {
            return new H5("Are you sure ?");
        }

        @Override
        protected Component header() {
            return new H5("Delete Attribute Value");
        }
    }
}
