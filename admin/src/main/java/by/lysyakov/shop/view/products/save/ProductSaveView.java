package by.lysyakov.shop.view.products.save;

import by.lysyakov.shop.common.dto.TreeCategoryDto;
import by.lysyakov.shop.dto.productsview.ImageFileActionInfo;
import by.lysyakov.shop.dto.productsview.ProductDto;
import by.lysyakov.shop.service.productsview.ProductService;
import by.lysyakov.shop.view.common.MultiFileMemoryBuffer;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

import static java.lang.Math.toIntExact;

public class ProductSaveView extends Dialog {
    private static final Logger log = LoggerFactory.getLogger(ProductSaveView.class);

    public final static ProductDto NEW_PRODUCT_DTO = new ProductDto();

    private final static Notification CANNOT_DELETE_NOT_SAVED = new Notification() {{
        addThemeVariants(NotificationVariant.LUMO_ERROR);
        add(new Label("Cannot delete not saved item"));
        setDuration(5000);
    }};

    private final ProductService productService;
    private final Consumer<ProductDto> productPostChangeAction;

    private UpdatableProduct currentProduct = new UpdatableProduct();

    private IntegerField id = new IntegerField("Id");
    private TextArea shortName = new TextArea("Short Name");
    private NumberField price = new NumberField("Price");
    private Select<TreeCategoryDto> category = new Select<>();

    private MutableProductImage mainImage = new MutableProductImage();
    private ImageList additionalImages = new ImageList();

    private MemoryBuffer mainImageMemoryBuffer = new MemoryBuffer();
    Upload uploadMainImg = new Upload(mainImageMemoryBuffer) {{
        setAcceptedFileTypes();
        addSucceededListener(event -> {
            var inputStream = mainImageMemoryBuffer.getInputStream();
            currentProduct.setMainImageFile(ImageFileActionInfo.save(mainImageMemoryBuffer.getFileName(), () -> inputStream));
            mainImage.setSrc(currentProduct.getId(), mainImageMemoryBuffer);
        });
    }};

    private MultiFileMemoryBuffer additionalImagesMemoryBuffer = new MultiFileMemoryBuffer();
    Upload uploadAdditionalImages = new Upload(additionalImagesMemoryBuffer) {{
        setAcceptedFileTypes();
        addSucceededListener(event -> {
            additionalImages.addImages(currentProduct.getId(), additionalImagesMemoryBuffer);
            currentProduct.setAdditionalImagesFiles(additionalImages.imageFileInfos());
        });
    }};

    Binder<UpdatableProduct> binder;

    public ProductSaveView(Map<Long, TreeCategoryDto> categoryById,
                           ProductService productService,
                           Consumer<ProductDto> productPostChangeAction) {
        this.productPostChangeAction = productPostChangeAction;
        this.productService = productService;

        var productFieldsLayout = new VerticalLayout() {{
            setSpacing(true);
            setSizeUndefined();
            add(id, mainImage, uploadMainImg, shortName, price, category, initButtons(), uploadAdditionalImages, additionalImages);
        }};

        id.setEnabled(false);

        category.setLabel("Category");
        category.setItems(categoryById.values());
        category.setItemLabelGenerator(TreeCategoryDto::fullName);

        this.binder = initBinder();
        this.add(productFieldsLayout);
        this.addDialogCloseActionListener(event -> {
            uploadMainImg.clearFileList();
            uploadAdditionalImages.clearFileList();
            additionalImages.removeAll();
            additionalImagesMemoryBuffer.clearFiles();
            close();
        });
    }

    private Binder<UpdatableProduct> initBinder() {
        return new Binder<>(UpdatableProduct.class) {{
            forField(id).bindReadOnly(p -> p.getId() == null ? null : toIntExact(p.getId()));
            forField(shortName).withValidator(name -> !name.isBlank(), "Shor Name should be not empty")
                               .bind(UpdatableProduct::getShortName, UpdatableProduct::setShortName);
            forField(price).withValidator(Objects::nonNull, "Price should be not empty")
                           .bind(UpdatableProduct::getPrice, UpdatableProduct::setPrice);
            forField(category).withValidator(Objects::nonNull, "You must choose any category")
                              .bind(dto -> {
                                  bindImages(dto);
                                  return dto.getCategory();
                              }, UpdatableProduct::setCategory);
        }};
    }

    private void bindImages(UpdatableProduct productDto) {
        try {
            mainImage.setSrc(productDto.getId(), productDto.getMainImageFile());
            additionalImages.setImages(productDto.getId(), productDto.getAdditionalImagesFiles());
        } catch (Exception ex) {
            log.error("Error binding image", ex);
            mainImage.setEmptySrc();
            additionalImages.removeAll();
        }
    }

    private HorizontalLayout initButtons() {
        ProductSaveView saveView = this;
        return new HorizontalLayout(
                new Button("Save") {{
                    getElement().getThemeList().add("primary");
                    addClickListener(e -> {
                        try {
                            binder.validate().notifyBindingValidationStatusHandlers();
                            if (binder.isValid()) {
                                binder.writeBean(currentProduct);

                                productService.saveWithImage(currentProduct.productUpdateInfo());

                                saveView.close();
                                productPostChangeAction.accept(currentProduct.updatedSourceProduct());
                            }
                        } catch (ValidationException ex) {
                            ex.printStackTrace();
                        }
                    });
                }},
                new Button("Cancel") {{
                    addClickListener(e -> saveView.close());
                }},
                new Button("Delete") {{
                    getElement().getThemeList().add("error");
                    addClickListener(e -> {
                        if (id.isEmpty()) {
                            CANNOT_DELETE_NOT_SAVED.open();
                        } else {
                            productService.delete(currentProduct.getId());
                            productPostChangeAction.accept(null);
                            setCurrentNewProduct();
                        }
                        saveView.close();
                    });
                }});
    }

    public boolean isCurrentNewProduct() {
        return isCurrentProduct(ProductSaveView.NEW_PRODUCT_DTO);
    }

    public boolean isCurrentProduct(ProductDto product) {
        return this.currentProduct.same(product);
    }

    public void setCurrentNewProduct() {
        setCurrentProduct(ProductSaveView.NEW_PRODUCT_DTO);
    }

    public void setCurrentProduct(ProductDto product) {
//        if (isCurrentProduct(product)) return;

        this.currentProduct.setSourceProduct(product);
        this.binder.readBean(this.currentProduct);
    }

    public void openWithProduct(ProductDto product) {
        setCurrentProduct(product);
        open();
    }

    public void openWithNewProduct() {
        openWithProduct(ProductSaveView.NEW_PRODUCT_DTO);
    }
}

