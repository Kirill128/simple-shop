package by.lysyakov.shop.view.filtersview;

import by.lysyakov.shop.dto.productsview.FilterDto;
import by.lysyakov.shop.service.productsview.FilterService;
import by.lysyakov.shop.view.common.AdminPage;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;
import org.jetbrains.annotations.NotNull;

@Route("/filters")
public class FiltersView extends AdminPage {

    private FilterService filterService;
    private FilterAddEditDialog addEditDialog;

    private Grid<FilterDto> filtersGrid = new Grid<>() {{
        addColumn(FilterDto::getId).setHeader("Id");
        addColumn(FilterDto::getTitle).setHeader("Title");
        addColumn(filter -> filter.getAttribute().getId()).setHeader("Attribute Id");
        addColumn(filter -> filter.getAttribute().getName()).setHeader("Attribute Name");
        addColumn(FilterDto::getOperationType).setHeader("Operation Type");
        addComponentColumn(filter -> new Button("Settings", event -> addEditDialog.open(filter)));
        addComponentColumn(filter -> new Button("Delete", event -> {
            filterService.remove(filter.getId());
            getListDataView().removeItem(filter);
        }));
        setSizeFull();
        addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
//        setItemDetailsRenderer(new ComponentRenderer<>());
    }};

    public FiltersView(FilterService filterService) {
        this.filterService = filterService;
        var filterSearchRes = filterService.findAllWithAttrs();
        this.addEditDialog = new FilterAddEditDialog(filterService,
                                                     filterSearchRes.getAttributes(),
                                                     filterSearchRes.getCategories(),
                                                     updatedFilter -> filtersGrid.getListDataView().refreshItem(updatedFilter));
        filtersGrid.setItems(filterSearchRes.getFilters());

        setContent(new HorizontalLayout(filtersGrid, addEditDialog){{
            setHeight("100%");
        }});

        addToNavbar(new Button("Add Filter", event -> addEditDialog.open(new FilterDto())));
    }

    @NotNull
    @Override
    protected PageType getPageType() {
        return PageType.FILTER;
    }
}
