package by.lysyakov.shop.view.products;

import by.lysyakov.shop.common.dto.TreeCategoryDto;
import by.lysyakov.shop.dto.productsview.ProductDto;
import by.lysyakov.shop.service.productsview.AttributeValueService;
import by.lysyakov.shop.service.productsview.CategoryService;
import by.lysyakov.shop.service.productsview.ProductAttributeValueService;
import by.lysyakov.shop.service.productsview.ProductService;
import by.lysyakov.shop.view.common.AdminPage;
import by.lysyakov.shop.view.products.attributes.AttributesSaveView;
import by.lysyakov.shop.view.products.save.ProductSaveView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;
import org.jetbrains.annotations.NotNull;

import static by.lysyakov.shop.mapper.CategoryMapper.flatMapCategoryTree;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Route("/products")
public class ProductsView extends AdminPage {
    private Grid<ProductDto> productsGrid;
    private ProductSaveView productSaveView;
    private AttributesSaveView attributesSaveView;

    public ProductsView(ProductService productService,
                        CategoryService categoryService,
                        AttributeValueService attributeValueService,
                        ProductAttributeValueService productAttributeValueService) {
        var categories = flatMapCategoryTree(categoryService.getCategoriesTree()).stream()
                          .collect(toMap(TreeCategoryDto::getId, identity()));
        this.productsGrid = new ProductsGrid(productService, categories);
        this.productSaveView = new ProductSaveView(categories,
                                                   productService,
                                                   (updatedProduct) -> productsGrid.getGenericDataView().refreshItem(updatedProduct));
//                                                       productsGrid.setItems(productService.findAll());
        this.attributesSaveView = new AttributesSaveView(attributeValueService, productAttributeValueService);

        this.productsGrid.addItemClickListener(e -> productSaveView.openWithProduct(e.getItem()));
        this.productsGrid.addComponentColumn(product -> new Button("Attributes",
                                                                   clickEvent -> attributesSaveView.openWithProduct(product)));

        setContent(new HorizontalLayout(productsGrid, productSaveView, attributesSaveView){{
            setHeight("100%");
        }});

        addToNavbar(new Button("Add Product"){{
            addClickListener(e -> productSaveView.openWithNewProduct());
        }});
    }

    @NotNull
    @Override
    protected PageType getPageType() {
        return PageType.PRODUCT;
    }

}
