package by.lysyakov.shop.view.filtersview

import by.lysyakov.shop.dto.productsview.AttributeDto
import by.lysyakov.shop.dto.productsview.CategoryDto
import by.lysyakov.shop.dto.productsview.FilterDto
import by.lysyakov.shop.entities.FilterOperationType
import by.lysyakov.shop.service.productsview.FilterService
import by.lysyakov.shop.view.common.Dialog
import com.vaadin.flow.component.ClickEvent
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.ComponentEventListener
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.grid.GridVariant
import com.vaadin.flow.component.html.H5
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.select.Select
import com.vaadin.flow.component.textfield.IntegerField
import com.vaadin.flow.component.textfield.TextArea
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.binder.ValidationException
import java.util.function.Consumer

class FilterAddEditDialog(
    private val filterService: FilterService,
    private val allAttributes: Collection<AttributeDto>,
    private val allCategories: Collection<CategoryDto>,
    afterSuccessAction: Consumer<FilterDto>
) : Dialog<FilterDto>(afterSuccessAction) {

    private val categoriesToSelect = ArrayList<CategoryDto>()
    private val filterCategories = ArrayList<CategoryDto>()

    private lateinit var idField: IntegerField
    private lateinit var titleArea: TextArea
    private lateinit var operationTypeSelect: Select<FilterOperationType>
    private lateinit var attributeSelect: Select<AttributeDto>
    private lateinit var categorySelect: Select<CategoryDto>

    private val binder = Binder(FilterDto::class.java).also {
        it.forField(idField).bindReadOnly { it.id?.toInt() }
        it.forField(titleArea)
          .bind(FilterDto::title, { dto, title -> dto.title = title })
        it.forField(operationTypeSelect)
          .bind(FilterDto::operationType, { dto, operType -> dto.operationType = operType })
        it.forField(attributeSelect)
          .bind(FilterDto::attribute, { dto, attr -> dto.attribute = attr })
        it.forField(categorySelect)
          .bind({ null },
                { _, _ -> context.categories = filterCategories })
    }

    private lateinit var categoriesGrid: Grid<CategoryDto>

    init {
        attributeSelect.setItems(*allAttributes.toTypedArray())
    }

    override fun buttonOKClickListener(): ComponentEventListener<ClickEvent<Button>> {
        return ComponentEventListener {
            try {
                binder.writeBean(context)
                afterSuccessAction.accept(filterService.save(context))
                this.close()
            } catch (ex: ValidationException) {
                ex.printStackTrace()
            }
        }
    }

    override fun open(context: FilterDto?) {
        val (categoriesForFilter, categoriesForSelect) = allCategories.partition { it.id in context!!.categoriesIds }
        filterCategories.replaceAll(categoriesForFilter)
        categoriesToSelect.replaceAll(categoriesForSelect)

        categorySelect.setItems(categoriesToSelect)
        categoriesGrid.setItems(filterCategories)

        super.open(context)
        context?.let { binder.readBean(it) }
    }

    override fun content(): Component {
        categoriesGrid = Grid<CategoryDto>().also {
            it.setHeightFull()
            it.addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT)
            it.isAllRowsVisible = true

            it.addColumn(CategoryDto::id).setHeader("Category Id")
              .setAutoWidth(true)
              .setSortable(true)
            it.addColumn(CategoryDto::name).setHeader("Category Name")
              .setAutoWidth(true)
              .setSortable(true)
            it.addComponentColumn { category -> Button("Delete") { removeCategoryFromFilter(category) } }
              .setAutoWidth(true)
        }

        idField = IntegerField("Id")
        titleArea = TextArea("Title")
        operationTypeSelect = Select("Operation type", {}, *FilterOperationType.values())
        attributeSelect = Select<AttributeDto>().also {
            it.label = "Attribute"
            it.setItemLabelGenerator { "${it.id}. ${it.name}" }
        }
        categorySelect = Select<CategoryDto>().also {
            it.label = "Category Add"
            it.addValueChangeListener { it.value?.let { addCategoryToFilter(it) } }
            it.setItemLabelGenerator { "${it.id}. ${it.name}" }
        }
        return VerticalLayout(idField, titleArea, operationTypeSelect, attributeSelect, categorySelect, categoriesGrid)
    }

    private fun addCategoryToFilter(category: CategoryDto) {
        categoriesToSelect.removeIf { it.id == category.id }
        filterCategories.add(category)
        updateCategoriesView()
    }

    private fun removeCategoryFromFilter(category: CategoryDto) {
        filterCategories.removeIf { it.id == category.id }
        categoriesToSelect.add(category)
        updateCategoriesView()
    }

    private fun updateCategoriesView() {
        categorySelect.setItems(categoriesToSelect)
        categoriesGrid.setItems(filterCategories)
    }

    override fun header(): Component {
        return H5("Fill Filter data")
    }

    private fun <E, T : MutableCollection<E>> T.replaceAll(coll: Collection<E>) {
        this.clear()
        this.addAll(coll)
    }
}
