package by.lysyakov.shop.view.products.attributes;

import by.lysyakov.shop.dto.productsview.AttributeDto;
import by.lysyakov.shop.dto.productsview.AttributeValueDto;
import by.lysyakov.shop.dto.productsview.AttributeValuesInfo;
import by.lysyakov.shop.dto.productsview.ProductDto;
import by.lysyakov.shop.service.productsview.AttributeValueService;
import by.lysyakov.shop.service.productsview.ProductAttributeValueService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static by.lysyakov.shop.dto.productsview.AttributeDto.attributeDto;
import static by.lysyakov.shop.dto.productsview.AttributeValueDto.attributeValueDto;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class AttributesSaveView extends Dialog {
    private AttributeAddPanel attributeAddPanel;
    private AttributeValueGrid attributeValueGrid;


    public AttributesSaveView(AttributeValueService attributeValueService,
                              ProductAttributeValueService productAttributeValueService) {
        var attributesValuesFullInfo = attributeValueService.findAllAttrValueFullInfo();
        this.attributeValueGrid = new AttributeValueGrid(attributesValuesFullInfo,
                                                         productAttributeValueService,
                                                         attributeValueService);
        this.attributeAddPanel = new AttributeAddPanel(attributeValueService,
                                                       productAttributeValueService,
                                                       attributesValuesFullInfo,
                                                       () -> attributeValueGrid.repaint());
        this.add(new VerticalLayout(attributeAddPanel, attributeValueGrid));
    }
    public void openWithProduct(ProductDto product) {
        this.attributeValueGrid.setCurrentProduct(product);
        this.attributeAddPanel.setCurrentProduct(product);
        this.open();
    }
}

class AttributeValueGrid extends Grid<AttributeValueDto> {
    private final ProductAttributeValueService productAttributeValueService;
    private ProductDto currentProduct;
    private List<AttributeValueDto> currentProductCachedAttrValues;

    private final AttributeValuesInfo attributeValuesInfo;

    public AttributeValueGrid(AttributeValuesInfo attributeValuesInfo,
                              ProductAttributeValueService productAttributeValueService,
                              AttributeValueService attributeValueService) {
        this.productAttributeValueService = productAttributeValueService;
        this.attributeValuesInfo = attributeValuesInfo;

        addColumn(AttributeValueDto::getId).setHeader("Id")
                                           .setSortable(true)
                                           .setAutoWidth(true);
        addColumn(attrVal -> attrVal.getAttribute().getName()).setHeader("Attribute")
                                                              .setAutoWidth(true)
                                                              .setSortable(true);
        addColumn(AttributeValueDto::getValue).setHeader("Value")
                                              .setAutoWidth(true)
                                              .setSortable(false);
        addComponentColumn(attrVal -> new Checkbox(attrVal.visible()) {{
            addValueChangeListener(event -> {
                attributeValueService.updateIsVisibleOnFilter(attrVal.getId(), !attrVal.visible());
                setValue(!attrVal.visible());
                attrVal.setVisibleOnFilter(!attrVal.visible());
            });
        }}).setHeader("Visible");

        addComponentColumn(attrVal -> new Button("Delete", event -> onAttrValDelete(attrVal)))
                .setHeader("")
                .setAutoWidth(true);

        setAllRowsVisible(true);
        setSizeFull();
    }

    public void setCurrentProduct(ProductDto product) {
        this.currentProductCachedAttrValues = product.getAttributeValuesIds()
                                                     .stream()
                                                     .map(attributeValuesInfo::attrValByValId)
                                                     .collect(toList());
        setItems(currentProductCachedAttrValues);
        this.currentProduct = product;
    }

    private void onAttrValDelete(AttributeValueDto attrVal) {
        productAttributeValueService.remove(currentProduct.getId(), attrVal.getId());
        currentProduct.getAttributeValuesIds().remove(attrVal.getId());

        currentProductCachedAttrValues.removeIf(val -> val.getId() == attrVal.getId());
        setItems(currentProductCachedAttrValues);
    }

    public void repaint() {
        if (currentProduct == null) return;
        setCurrentProduct(currentProduct);
    }

}

class AttributeAddPanel extends VerticalLayout {
    private AttributeValueService attributeValueService;
    private ProductAttributeValueService productAttributeValueService;

    private final AttributeValuesInfo attributeValuesInfo;

    private CacheableComboBox<AttributeDto> attributes;
    private CacheableComboBox<AttributeValueDto> attrValues;
    private Button addBtn;

    private ProductDto currentProduct;

    private Runnable onAddAttributeToProduct;

    public AttributeAddPanel(AttributeValueService attributeValueService,
                             ProductAttributeValueService productAttributeValueService,
                             AttributeValuesInfo attributeValuesInfo,
                             Runnable onAddAttributeToProduct) {
        this.attributeValueService = attributeValueService;
        this.productAttributeValueService = productAttributeValueService;
        this.attributeValuesInfo = attributeValuesInfo;
        this.onAddAttributeToProduct = onAddAttributeToProduct;

        this.attributes = new CacheableComboBox<>("Атрибут"){{
            setItemLabelGenerator(AttributeAddPanel::attributeLabel);
            setItems(attributeValuesInfo.allAttributes());
            addValueChangeListener(event -> {
                if (event.getValue() == null) return;
                attrValues.setItems(attributeValuesInfo.attrValuesByAttrId(event.getValue().getId()));
            });
            addCustomValueSetListener(event -> attrValues.setItems());
        }};
        this.attrValues = new CacheableComboBox<>("Значение"){{
            setItemLabelGenerator(AttributeAddPanel::attributeValueLabel);
        }};

        this.addBtn = new Button("Добавить", event -> addInputtedAttributeToCurrentProduct());

        this.add(new HorizontalLayout(attributes, attrValues), addBtn);
    }

    private static String attributeLabel(AttributeDto attribute) {
        return "%s. %s".formatted(attribute.getId(), attribute.getName());
    }

    private static String attributeValueLabel(AttributeValueDto attributeVal) {
        return "%s. %s".formatted(attributeVal.getId(), attributeVal.getValue());
    }

    /**
     * 1. Атрибут выбран, значение выбрано
     * 2. Атрибут выбран, значение кастомное
     * 3. Атрибут кастомный, значение кастомное
     */
    private void addInputtedAttributeToCurrentProduct() {
        if (currentProduct == null || currentProduct.getId() == null) return;

        var attribute = attributes.getOptionalValue();
        var attributeVal = attrValues.getOptionalValue();

        var customAttribute = attributes.getLastInputtedCustomValueDetail();
        var customAttrValue = attrValues.getLastInputtedCustomValueDetail();

        if (attributeVal.isPresent()) {
            productAttributeValueService.addExisting(currentProduct.getId(), attributeVal.get().getId());
            currentProduct.getAttributeValuesIds().add(attributeVal.get().getId());

            this.attributes.clear();
            this.attrValues.clear();
        } else if (attribute.isPresent()) {
            var newAttrValue = attributeValueDto(attribute.get(), customAttrValue);
            var savedAttrVal = productAttributeValueService.addWithNewAttrVal(currentProduct.getId(), newAttrValue);
            currentProduct.getAttributeValuesIds().add(savedAttrVal.getId());

            this.attributeValuesInfo.addAttrValue(attribute.get().getId(), savedAttrVal);

            this.attrValues.clear();
            this.attributes.clear();
            attrValues.setItems(attributeValuesInfo.allAttrValues());
        } else {
            var newAttrValue = attributeValueDto(attributeDto(customAttribute), customAttrValue);
            var savedAttrVal = productAttributeValueService.addWithNewAttrAndVal(currentProduct.getId(), newAttrValue);
            currentProduct.getAttributeValuesIds().add(savedAttrVal.getId());

            var savedAttr = savedAttrVal.getAttribute();
            this.attributeValuesInfo.addAttr(savedAttr, savedAttrVal);

            this.attributes.clear();
            this.attrValues.clear();
            attributes.setItems(this.attributeValuesInfo.allAttributes());
            attrValues.setItems(this.attributeValuesInfo.allAttrValues());
        }
        onAddAttributeToProduct.run();
    }

    public void setCurrentProduct(ProductDto product) {
        this.currentProduct = product;
    }
}

class CacheableComboBox<T> extends ComboBox<T> {
    private String lastInputtedCustomValueDetail;

    public CacheableComboBox(String label) {
        super(label);

        setAllowCustomValue(true);
        addCustomValueSetListener(event -> {
            lastInputtedCustomValueDetail = event.getDetail();
//            this.setValue(null);
        });
    }

    public String getLastInputtedCustomValueDetail() {
        return lastInputtedCustomValueDetail;
    }
}
