package by.lysyakov.shop.view.exportview;

import by.lysyakov.shop.service.productsview.ProductService;
import by.lysyakov.shop.view.common.AdminPage;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import org.jetbrains.annotations.NotNull;

@Route("/export")
public class ExportView extends AdminPage {
    public ExportView(ProductService productService) {
        var anchor = new Anchor("#", new Button("Export to Excel"));
        anchor.setHref(new StreamResource("products.xlsx",
                                          () -> productService.exportToFile(".xlsx")));
        addToNavbar(new HorizontalLayout(anchor){{
            setJustifyContentMode(JustifyContentMode.CENTER);
        }});
    }

    @NotNull
    @Override
    protected PageType getPageType() {
        return PageType.EXPORT;
    }
}
