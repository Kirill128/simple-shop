package by.lysyakov.shop.view;

import by.lysyakov.shop.dto.productsview.OrderDto;
import by.lysyakov.shop.dto.productsview.OrderProductDto;
import by.lysyakov.shop.dto.productsview.OrderStatus;
import by.lysyakov.shop.service.productsview.OrderService;
import by.lysyakov.shop.view.common.AdminPage;
import by.lysyakov.shop.view.common.Dialog;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;


@Route("/orders")
public class OrdersView extends AdminPage {

    private Grid<OrderDto> ordersGrid;

    public OrdersView(OrderService orderService) {
        this.ordersGrid = new OrdersGrid(orderService);
        setContent(ordersGrid);
    }

    @NotNull
    @Override
    protected PageType getPageType() {
        return PageType.ORDER;
    }

    static class OrdersGrid extends Grid<OrderDto> {
        private ChangeOrderStatusDialog changeOrderStatusDialog;

        public OrdersGrid(OrderService orderService) {
            this.changeOrderStatusDialog = new ChangeOrderStatusDialog(orderService, e -> {
                getGenericDataView().refreshItem(e);
            });
            addColumn(OrderDto::getId).setHeader("Id")
                                      .setSortable(true);
            addColumn(OrderDto::getMessage).setHeader("Message")
                                           .setSortable(true);
            addColumn(OrderDto::getUserName).setHeader("User Name")
                                            .setSortable(true);
            addColumn(OrderDto::getPhone).setHeader("Phone")
                                                .setSortable(true);
            addColumn(OrderDto::getEmail).setHeader("Email")
                                                .setSortable(true);
            addColumn(OrderDto::getStartTime).setHeader("Start Time")
                                             .setSortable(true);
            addColumn(OrderDto::getLastModified).setHeader("Last Modified")
                                                .setSortable(true);
            addColumn(order -> order.getStatus().getDescription()).setHeader("Status")
                                                          .setComparator(order -> order.getStatus().getId());
            addComponentColumn(order -> new Button("Сменить статус") {{
                                    addClickListener(event -> changeOrderStatusDialog.open(order));
                                }})
                    .setHeader("");

            addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);

            setItems(new OrdersProvider(orderService));

            setAllRowsVisible(true);
            setSizeFull();
            setItemDetailsRenderer(new ComponentRenderer<>(OrderProductsGrid::new));
        }

        private static class OrdersProvider extends AbstractBackEndDataProvider<OrderDto, Void> {
            private final OrderService orderService;
            public OrdersProvider(OrderService orderService) {
                this.orderService = orderService;
            }

            @Override
            protected Stream<OrderDto> fetchFromBackEnd(Query<OrderDto, Void> query) {
                // Sorting
//                if (query.getSortOrders().size() > 0) {
//                    stream = stream.sorted(sortComparator(query.getSortOrders()));
//                }
                return orderService.findPage(PageRequest.of(query.getOffset() / query.getLimit(), query.getLimit()))
                                   .stream();
            }

            @Override
            protected int sizeInBackEnd(Query<OrderDto, Void> query) {
                return (int) orderService.allOrdersCount();
            }

//            private static Comparator<OrderProductDto> sortComparator(List<QuerySortOrder> sortOrders) {
//                return sortOrders.stream().map(sortOrder -> {
//                    Comparator<Person> comparator = personFieldComparator(
//                            sortOrder.getSorted());
//
//                    if (sortOrder.getDirection() == SortDirection.DESCENDING) {
//                        comparator = comparator.reversed();
//                    }
//
//                    return comparator;
//                }).reduce(Comparator::thenComparing).orElse((p1, p2) -> 0);
//            }
//
//            private static Comparator<Person> personFieldComparator(String sorted) {
//                if (sorted.equals("name")) {
//                    return Comparator.comparing(person -> person.getFullName());
//                } else if (sorted.equals("profession")) {
//                    return Comparator.comparing(person -> person.getProfession());
//                }
//                return (p1, p2) -> 0;
//            }
        }
    }

    static class OrderProductsGrid extends Grid<OrderProductDto> {
        public OrderProductsGrid(OrderDto order) {
            this(order.getOrderProducts());
        }
        public OrderProductsGrid(List<OrderProductDto> orderProducts) {
            super(OrderProductDto.class, false);
            setHeightFull();
            addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
            setAllRowsVisible(true);

            addColumn(product -> product.getProductDto().getId())
                    .setHeader("Id")
                    .setAutoWidth(false);
            addColumn(product -> product.getProductDto().getShortName())
                    .setHeader("Short Name")
                    .setAutoWidth(false);
            addColumn(product -> product.getProductDto().getPrice()).setHeader("Price");
            addColumn(productDto -> productDto.getProductDto().getCategory().getId())
                    .setHeader("Category Id");
            addColumn(productDto -> productDto.getProductDto().getCategory().getName())
                    .setHeader("Category Name");
            addColumn(productDto -> productDto.getProductDto().isVisible()).setHeader("Visible");

            setItems(orderProducts);
        }
    }

    static class ChangeOrderStatusDialog extends Dialog<OrderDto> {
        private final OrderService orderService;
        private Select<OrderStatus> newOrderStatusSelect;
        public ChangeOrderStatusDialog(OrderService orderService, Consumer<OrderDto> afterSuccessAction) {
            super(afterSuccessAction);
            this.orderService = orderService;
        }

        @Override
        protected ComponentEventListener<ClickEvent<Button>> buttonOKClickListener() {
            return event -> {
                newOrderStatusSelect.getOptionalValue()
                                    .filter(newStatus -> context.getStatus().getId() != newStatus.getId())
                                    .ifPresent(newStatus -> {
                                        var currentDate = LocalDateTime.now();
                                        orderService.updateOrderStatus(context.getId(), newStatus, currentDate);
                                        context.setStatus(newStatus);
                                        context.setLastModified(currentDate);
                                    });
                afterSuccessAction.accept(context);
                this.close();
            };
        }

        @Override
        protected Component header() {
            return new H3("Выберите новый статус заказа");
        }

        @Override
        protected Component content() {
            newOrderStatusSelect = new Select<>("Выберите статус заказа",
                                                       e -> {},
                                                       OrderStatus.values());
            newOrderStatusSelect.setItemLabelGenerator(OrderStatus::getDescription);
            return newOrderStatusSelect;
        }
    }
}
