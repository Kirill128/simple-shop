package by.lysyakov.shop.view.common;

import by.lysyakov.shop.dto.productsview.ImageFileActionInfo;
import com.vaadin.flow.component.upload.MultiFileReceiver;
import com.vaadin.flow.component.upload.receivers.FileData;
import kotlin.Pair;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toMap;

public class MultiFileMemoryBuffer implements MultiFileReceiver {

    //----------------------COPY OF com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer-------------------------------------------
    protected Map<String, FileData> files = new HashMap<>();

    @Override
    public OutputStream receiveUpload(String fileName, String MIMEType) {
        ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
        files.put(fileName, new FileData(fileName, MIMEType, outputBuffer));

        return outputBuffer;
    }

    /**
     * Get the files in memory for this buffer.
     *
     * @return files in memory
     */
    public Set<String> getFiles() {
        return files.keySet();
    }

    /**
     * Get file data for upload with file name.
     *
     * @param fileName
     *            file name to get upload data for
     * @return file data for filename or null if not found
     */
    public FileData getFileData(String fileName) {
        return files.get(fileName);
    }

    /**
     * Get the output stream for file.
     *
     * @param fileName
     *            name of file to get stream for
     * @return file output stream or empty stream if no file found
     */
    public ByteArrayOutputStream getOutputBuffer(String fileName) {
        if (files.containsKey(fileName)) {
            return (ByteArrayOutputStream) files.get(fileName)
                                                .getOutputBuffer();
        }
        return new ByteArrayOutputStream();
    }

    /**
     * Get the input stream for file with filename.
     *
     * @param filename
     *            name of file to get input stream for
     * @return input stream for file or empty stream if file not found
     */
    public InputStream getInputStream(String filename) {
        if (files.containsKey(filename)) {
            return new ByteArrayInputStream(((ByteArrayOutputStream) files
                    .get(filename).getOutputBuffer()).toByteArray());
        }
        return new ByteArrayInputStream(new byte[0]);
    }
    //-----------------------------------------------------------------

    public Map<String, InputStream> allFileInputStreams() {
        return this.getFiles()
                   .stream()
                   .map(fileName -> new Pair<>(fileName, this.getInputStream(fileName)))
                   .collect(toMap(Pair::getFirst, Pair::getSecond));
    }

    public List<ImageFileActionInfo> allSaveImageFileActionInfos() {
        return this.getFiles()
                   .stream()
                   .map(fileName -> ImageFileActionInfo.save(fileName, () -> this.getInputStream(fileName)))
                   .toList();
    }

    public List<ImageFileActionInfo> allFileInfo() {
        return this.getFiles()
                   .stream()
                   .map(fileName -> new ImageFileActionInfo(fileName, () -> this.getInputStream(fileName)))
                   .toList();
    }

    public void clearFiles() {
        files.clear();
    }
}
