package by.lysyakov.shop.view.products.save;

import by.lysyakov.shop.dto.productsview.ImageFileActionInfo;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.server.StreamResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

import static by.lysyakov.shop.dto.productsview.ImageFileActionInfo.ACTION.REMOVE;
import static by.lysyakov.shop.dto.productsview.ImageFileActionInfo.ACTION.VIEW;

class ImmutableProductImage extends Image {
    private static final Logger log = LoggerFactory.getLogger(ImmutableProductImage.class);

    protected Long imgProductId;

    protected ImageFileActionInfo fileInfo;

    public ImmutableProductImage(Long imgProductId, ImageFileActionInfo fileInfo) {
        this(imgProductId);
        this.setSrc(fileInfo);
    }

    private ImmutableProductImage(Long imgProductId) {
        this();
        this.imgProductId = imgProductId;
    }
    ImmutableProductImage() {
        setAlt("Couldn't load image");
    }

    public boolean isSameProductImage(Long imgProductId) {
        return Objects.equals(this.imgProductId, imgProductId);
    }

    protected void setSrc(ImageFileActionInfo fileInfo) {
        this.fileInfo = fileInfo;
        this.setSrc(new StreamResource(fileInfo.getFileName(), () -> fileInfo.getInputStream().get()));
    }

}

class RemovableProductImage extends HorizontalLayout {

    private ImmutableProductImage image;

    public RemovableProductImage(Long imgProductId, ImageFileActionInfo fileInfo) {
        this.image = new ImmutableProductImage(imgProductId, fileInfo);
        this.add(onRemoveCheckBox(fileInfo), image);
    }

    public RemovableProductImage() {
    }

    /*
   || action ||&&|| checkbox new value
      view && remove -> remove
      view && not remove -> view
      save && remove -> view
      save && not remove -> save
      remove && remove -> cannot be
      remove && not remove -> view
     */
    private static Checkbox onRemoveCheckBox(ImageFileActionInfo fileInfo) {
        return new Checkbox("Remove", false) {{
            addValueChangeListener(event -> {
                if (event.getValue()) {
                    var newAction = switch (fileInfo.getAction()) {
                        case VIEW -> REMOVE;
                        case SAVE -> VIEW;
                        default -> throw new RuntimeException("ERROR NOT CORRECT STATUS on remove '%s'".formatted(fileInfo.getAction()));
                    };
                    fileInfo.setAction(newAction);
                } else {
                    var newAction = switch (fileInfo.getAction()) {
                        case REMOVE -> VIEW;
                        default -> fileInfo.getAction();
                    };
                    fileInfo.setAction(newAction);
                }
            });
        }};
    }

    public ImageFileActionInfo imageFileActionInfo() {
        return image.fileInfo;
    }
}
