package by.lysyakov.shop.view;

import by.lysyakov.shop.common.dto.TreeCategoryDto;
import by.lysyakov.shop.dto.productsview.SaveCategoryDto;
import by.lysyakov.shop.service.productsview.CategoryService;
import by.lysyakov.shop.view.common.AdminPage;
import by.lysyakov.shop.view.common.Dialog;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.Route;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

@Route("/categories")
public class CategoriesView extends AdminPage {
    private static final TreeCategoryDto ROOT_PARENT_CATEGORY = new TreeCategoryDto();
    private CategoryRemoveDialog removeDialog;
    private CategoryUpdateDialog updateDialog;
    private CategoryCreateDialog createDialog;

    private TreeGrid<TreeCategoryDto> categoriesTree = new TreeGrid<>(){{
        setHeightFull();
        addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
        setAllRowsVisible(true);
        addColumn(TreeCategoryDto::getId).setHeader("Id");
        addHierarchyColumn(TreeCategoryDto::getName).setHeader("Title");
        setHeightFull();
    }};

    public CategoriesView(CategoryService categoryService) {
        final Consumer<TreeCategoryDto> UPDATE_GRID = (dto) -> categoriesTree.setItems(categoryService.getCategoriesTree(), TreeCategoryDto::getSubcategories);
        this.removeDialog = new CategoryRemoveDialog(categoryService, UPDATE_GRID);
        this.updateDialog = new CategoryUpdateDialog(categoryService, UPDATE_GRID);
        this.createDialog = new CategoryCreateDialog(categoryService, UPDATE_GRID);

        categoriesTree.addComponentColumn(category -> new Checkbox(category.isVisible()) {{
                          addValueChangeListener(event -> {
                              categoryService.updateVisible(category.getId(), !category.isVisible());
                              setValue(!category.isVisible());
                              category.setVisible(!category.isVisible());
                          });
                      }})
                      .setHeader("Visible");
        categoriesTree.addComponentColumn(category -> new Button("+")
                      {{addClickListener(event -> createDialog.open(category));}})
                      .setHeader("Add");
        categoriesTree.addComponentColumn(category -> new Button("Delete")
                      {{addClickListener(event -> removeDialog.open(category));}})
                      .setHeader("Delete");

        categoriesTree.addItemDoubleClickListener(item -> updateDialog.open(item.getItem()));

        UPDATE_GRID.accept(null);

        setContent(categoriesTree);
        addToNavbar(new Button("Add Category", event -> createDialog.open(ROOT_PARENT_CATEGORY)));
    }

    @NotNull
    @Override
    protected PageType getPageType() {
        return PageType.CATEGORY;
    }

    static class CategoryCreateDialog extends Dialog<TreeCategoryDto> {
        private final CategoryService categoryService;
        private TextArea title;
        private Binder<SaveCategoryDto> binder = new Binder<>(SaveCategoryDto.class){{
            forField(title).bind(SaveCategoryDto::getName, SaveCategoryDto::setName);
        }};

        public CategoryCreateDialog(CategoryService categoryService, Consumer<TreeCategoryDto> afterCreateAction) {
            super(afterCreateAction);
            this.categoryService = categoryService;
        }

        @Override
        protected ComponentEventListener<ClickEvent<Button>> buttonOKClickListener() {
            return e -> {
                try {
                    var newCategory = new SaveCategoryDto();
                    binder.writeBean(newCategory);
                    newCategory.setParentCategoryId(context.getId());

                    categoryService.save(newCategory);
                    afterSuccessAction.accept(null);
                    this.close();
                } catch (ValidationException ex) {
                    ex.printStackTrace();
                }
            };
        }

        @Override
        protected Component header() {
            return new H5("Change Category Name");
        }

        @Override
        protected Component content() {
            return title = new TextArea("Title");
        }
    }

    static class CategoryUpdateDialog extends Dialog<TreeCategoryDto> {
        private final CategoryService categoryService;
        private TextArea title;
        private Binder<TreeCategoryDto> binder = new Binder<>(TreeCategoryDto.class){{
            forField(title).bind(TreeCategoryDto::getName, TreeCategoryDto::setName);
        }};

        public CategoryUpdateDialog(CategoryService categoryService, Consumer<TreeCategoryDto> afterUpdateAction) {
            super(afterUpdateAction);
            this.categoryService = categoryService;
        }

        @Override
        protected ComponentEventListener<ClickEvent<Button>> buttonOKClickListener() {
            return e -> {
                try {
                    binder.writeBean(context);
                    categoryService.updateCategoryName(context.getId(), context.getName());
                    afterSuccessAction.accept(null);
                    this.close();
                } catch (ValidationException ex) {
                    ex.printStackTrace();
                }
            };
        }

        @Override
        protected Component header() {
            return new H5("Change Category Name");
        }

        @Override
        protected Component content() {
            return title = new TextArea("Title");
        }
    }

    static class CategoryRemoveDialog extends Dialog<TreeCategoryDto> {
        private final CategoryService categoryService;
        public CategoryRemoveDialog(CategoryService categoryService, Consumer<TreeCategoryDto> afterRemoveAction) {
            super(afterRemoveAction);
            this.categoryService = categoryService;
        }

        @Override
        protected ComponentEventListener<ClickEvent<Button>> buttonOKClickListener() {
            return event -> {
                categoryService.remove(context.getId());
                afterSuccessAction.accept(null);
                this.close();
            };
        }

        @Override
        protected Component header() {
            return new H3("Are you sure to delete all category products and orders depending on this products ?");
        }

        @Override
        protected Component content() {
            return new Div();
        }
    }

}
