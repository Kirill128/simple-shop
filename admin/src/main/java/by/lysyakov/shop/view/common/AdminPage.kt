package by.lysyakov.shop.view.common

import by.lysyakov.shop.common.util.Extensions.mapToArray
import by.lysyakov.shop.view.CategoriesView
import by.lysyakov.shop.view.importview.ImportView
import by.lysyakov.shop.view.OrdersView
import by.lysyakov.shop.view.attributesview.AttributesView
import by.lysyakov.shop.view.exportview.ExportView
import by.lysyakov.shop.view.filtersview.FiltersView
import by.lysyakov.shop.view.products.ProductsView
import com.vaadin.flow.component.applayout.AppLayout
import com.vaadin.flow.component.applayout.DrawerToggle
import com.vaadin.flow.component.html.H1
import com.vaadin.flow.component.tabs.Tab
import com.vaadin.flow.component.tabs.Tabs
import com.vaadin.flow.component.tabs.Tabs.Orientation.VERTICAL
import com.vaadin.flow.router.RouterLink

abstract class AdminPage: AppLayout() {
    protected abstract val pageType: PageType

    init {
        val toggle = DrawerToggle()
        val title = H1(pageType.name)
        title.style.set("font-size", "var(--lumo-font-size-l)")["margin"] = "0"
        this.addToDrawer(tabs)
        this.addToNavbar(toggle, title)
    }

    private val tabs: Tabs
        get() = object: Tabs() {
            init {
                add(*pagesEnumToTabs(PageType.TYPES))
                orientation = VERTICAL
                selectedIndex = pageType.index - 1
            }
        }

    enum class PageType(val title: String,
                        val index: Int,
                        val adminPageClass: Class<out AdminPage>) {
        CATEGORY("Category", 1, CategoriesView::class.java),
        PRODUCT("Products", 2, ProductsView::class.java),
        ATTRIBUTE("Attributes", 3, AttributesView::class.java),
        FILTER("Filters", 4, FiltersView::class.java),
        ORDER("Orders", 5, OrdersView::class.java),
        IMPORT("Import", 6, ImportView::class.java),
        EXPORT("Export", 7, ExportView::class.java);

        companion object {
            val TYPES = values().sortedBy { it.index }
        }
    }

    companion object {
        fun pagesEnumToTabs(source: List<PageType>): Array<Tab> =
            source.mapToArray { Tab(RouterLink(it.title, it.adminPageClass)) }

    }
}