package by.lysyakov.shop.view.products.save

import by.lysyakov.shop.common.dto.TreeCategoryDto
import by.lysyakov.shop.dto.productsview.ImageFileActionInfo
import by.lysyakov.shop.dto.productsview.ProductDto

class UpdatableProduct {

    var sourceProduct: ProductDto? = null
        set(value) {
            field = value
            productUpdateInfo.clear()
            productUpdateInfo.id = value?.id
            productUpdateInfo.additionalImagesFiles = value?.additionalImagesFiles.orEmpty()
        }
    private val productUpdateInfo: ProductDto = ProductDto()

    fun getId() = sourceProduct?.id
    fun getShortName() = sourceProduct?.shortName
    fun getPrice() = sourceProduct?.price
    fun getCategory() = sourceProduct?.category
    fun getMainImageFile() = sourceProduct?.mainImageFile
    fun getAdditionalImagesFiles() = sourceProduct?.additionalImagesFiles
//    fun isVisible() = sourceProduct?.isVisibleOrDefault()

    fun setShortName(shortName: String) {
        if (sourceProduct?.shortName == shortName) return
        productUpdateInfo.shortName = shortName
    }
    fun setPrice(price: Double) {
        if (sourceProduct?.price == price) return
        productUpdateInfo.price = price
    }
    fun setCategory(category: TreeCategoryDto) {
        if (sourceProduct?.category == category) return
        productUpdateInfo.category = category
    }
    fun setMainImageFile(mainImageFile: ImageFileActionInfo) {
        if (sourceProduct?.mainImageFile == mainImageFile) return
        productUpdateInfo.mainImageFile = mainImageFile
    }
    fun setAdditionalImagesFiles(additionalImagesFiles: List<ImageFileActionInfo>) {
//        if (sourceProduct?.additionalImagesFiles == additionalImagesFiles) return
        productUpdateInfo.additionalImagesFiles = additionalImagesFiles
    }

//    fun isVisible(isVisible: Boolean) {
//        if (sourceProduct?.visible == isVisible) return
//        productUpdateInfo.visible = isVisible
//    }

    fun productUpdateInfo() = productUpdateInfo

    fun updatedSourceProduct(): ProductDto? {
        productUpdateInfo.shortName?.let { sourceProduct?.shortName = it }
        productUpdateInfo.category?.let { sourceProduct?.category = it }
        productUpdateInfo.price?.let { sourceProduct?.price = it }
        productUpdateInfo.visible?.let { sourceProduct?.visible = it }
        productUpdateInfo.mainImageFile?.let { sourceProduct?.mainImageFile = it }
        if (productUpdateInfo.additionalImagesFiles.isNotEmpty()) sourceProduct?.additionalImagesFiles = productUpdateInfo.additionalImagesFiles
        return sourceProduct
    }

    private fun ProductDto.clear() {
        this.id = null
        this.shortName = null
        this.category = null
        this.price = null
        this.visible = null
        this.mainImageFile = null
        this.additionalImagesFiles = emptyList()
    }

    fun same(other: ProductDto?): Boolean {
        return this.sourceProduct == other
    }
}