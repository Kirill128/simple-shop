package by.lysyakov.shop.view.products;

import by.lysyakov.shop.common.dto.TreeCategoryDto;
import by.lysyakov.shop.dto.productsview.ProductDto;
import by.lysyakov.shop.service.productsview.ProductService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.function.SerializablePredicate;

import java.util.Map;
import java.util.function.Consumer;

import static java.util.Comparator.comparing;

class ProductsGrid extends Grid<ProductDto> {
    public ProductsGrid(ProductService productService, Map<Long, TreeCategoryDto> categoryById) {
        super(ProductDto.class, false);
        setHeightFull();
        addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
        setAllRowsVisible(true);

        var productFilter = new ProductFilter(() -> getListDataView().refreshAll());

        addColumn(ProductDto::getId).setHeader(createFilterHeader("Id", productFilter::setId))
                                    .setAutoWidth(false)
                                    .setSortable(true);
        addColumn(ProductDto::getShortName).setHeader(createFilterHeader("Short Name", productFilter::setName))
                                           .setAutoWidth(false)
                                           .setSortable(true);
        addColumn(ProductDto::getPrice).setHeader(createFilterHeader("Price", productFilter::setPrice))
                                       .setSortable(true);
        addColumn(productDto -> productDto.getCategory().getId())
                .setHeader(createFilterHeader("Category Id", productFilter::setCategoryId))
                .setSortable(true)
                .setComparator(comparing(productDto -> productDto.getCategory().getId()));
        addColumn(productDto -> productDto.getCategory().fullName())
                .setHeader(createFilterHeader("Category Name", productFilter::setCategoryName))
                .setSortable(true)
                .setComparator(comparing(productDto -> productDto.getCategory().fullName()));

        addComponentColumn(product -> new Checkbox(product.isVisible()) {{
            addValueChangeListener(event -> {
                productService.updateVisible(product.getId(), !product.isVisible());
                setValue(!product.isVisible());
                product.setVisible(!product.isVisible());
            });
        }}).setHeader("Visible");

        setItems(productService.findAll().stream()
                               .peek(p -> p.setCategory(categoryById.get(p.getCategory().getId())))
                               .toList());
        getListDataView().setFilter(productFilter);
    }

    private static Component createFilterHeader(String labelText, Consumer<String> filterChangeConsumer) {
        Label label = new Label(labelText);
        label.getStyle().set("padding-top", "var(--lumo-space-m)")
             .set("font-size", "var(--lumo-font-size-xs)");
        TextField textField = new TextField();
        textField.setValueChangeMode(ValueChangeMode.EAGER);
        textField.setClearButtonVisible(true);
        textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        textField.setWidthFull();
        textField.getStyle().set("max-width", "100%");
        textField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
        VerticalLayout layout = new VerticalLayout(label, textField);
        layout.getThemeList().clear();
        layout.getThemeList().add("spacing-xs");

        return layout;
    }

    static class ProductFilter implements SerializablePredicate<ProductDto> {
        private Runnable onFieldSet;
        private String id;
        private String name;
        private String price;
        private String categoryId;
        private String categoryName;

        public ProductFilter(Runnable onFieldSet) {
            this.onFieldSet = onFieldSet;
        }

        public void setId(String id) {
            this.id = id;
            onFieldSet.run();
        }

        public void setName(String name) {
            this.name = name;
            onFieldSet.run();
        }

        public void setPrice(String price) {
            this.price = price;
            onFieldSet.run();
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
            onFieldSet.run();
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
            onFieldSet.run();
        }

        @Override
        public boolean test(ProductDto productDto) {
            return matches(productDto.getId().toString(), id) &&
                   matches(productDto.getShortName(), name) &&
                   matches(productDto.getPrice().toString(), price) &&
                   matches(String.valueOf(productDto.getCategory().getId()), categoryId) &&
                   matches(productDto.getCategory().getName(), categoryName);
        }

        private boolean matches(String value, String searchTerm) {
            return searchTerm == null || searchTerm.isEmpty()
                    || value.toLowerCase().contains(searchTerm.toLowerCase());
        }

    }
}