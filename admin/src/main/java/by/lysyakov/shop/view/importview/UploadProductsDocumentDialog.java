package by.lysyakov.shop.view.importview;

import by.lysyakov.shop.dto.productsview.ProductsImportFileData;
import by.lysyakov.shop.exceptions.ShopException;
import by.lysyakov.shop.parsers.ProductParsingMetadata;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Consumer;

import static by.lysyakov.shop.parsers.ProductParsableField.ID;
import static by.lysyakov.shop.parsers.ProductParsableField.NAME;
import static by.lysyakov.shop.parsers.ProductParsableField.PRICE;

public class UploadProductsDocumentDialog extends Dialog {
    private TextField sheetNameInput = new TextField("Sheet Name");
    private IntegerField firstRowInput = new IntegerField("First Row");
    private IntegerField lastRowInput = new IntegerField("Last Row");
    private TextField productIdColumnInput = new TextField("Product Id Column (Optional)");
    private TextField productNameColumnInput = new TextField("Product Name Column");
    private TextField productPriceColumnInput = new TextField("Product Price Column");
    private TextField categoryNameColumnInput = new TextField("Category Name Column");

    private boolean isFileUploaded;
    private MemoryBuffer documentMemoryBuffer = new MemoryBuffer();
    private Upload uploadDocument = new Upload(documentMemoryBuffer) {{
        setAcceptedFileTypes();
        setMaxFileSize(20_971_520);
        addSucceededListener(event -> isFileUploaded = true);
    }};

    public UploadProductsDocumentDialog(Consumer<ProductsImportFileData> onImport) {
        var doImportBtn = new Button("Parse Document", event -> onDoImportBtnClick(onImport));
        var cancelBtn = new Button("Cancel", event -> close());
        this.add(new VerticalLayout(sheetNameInput,
                                    firstRowInput, lastRowInput,
                                    productIdColumnInput,
                                    productNameColumnInput,
                                    productPriceColumnInput,
                                    categoryNameColumnInput,
                                    uploadDocument,
                                    new HorizontalLayout(doImportBtn, cancelBtn)));

    }

    private void onDoImportBtnClick(Consumer<ProductsImportFileData> onImport) {
        List<Button> buttons = this.getChildren().filter(el -> el instanceof Button).map(el -> (Button) el).toList();
        buttons.forEach(btn -> btn.setDisableOnClick(true));

        onImport.accept(currentMetadata());

        buttons.forEach(btn -> btn.setDisableOnClick(false));

        isFileUploaded = false;
        uploadDocument.clearFileList();
        this.close();
    }

    private ProductsImportFileData currentMetadata() {
        if (!isFileUploaded) throw new ShopException("No file uploaded");
        var metadata = new ProductParsingMetadata(
                sheetNameInput.getValue(),
                firstRowInput.getValue(), lastRowInput.getValue(),
                new LinkedHashMap<>(){{
                    if (!productIdColumnInput.getValue().isBlank()) put(productIdColumnInput.getValue(), ID);
                    put(productNameColumnInput.getValue(), NAME);
                    put(productPriceColumnInput.getValue(), PRICE);
                }},
                categoryNameColumnInput.getValue()
        );
        return new ProductsImportFileData(documentMemoryBuffer.getInputStream(),
                                          documentMemoryBuffer.getFileData().getMimeType(),
                                          metadata);
    }

}
