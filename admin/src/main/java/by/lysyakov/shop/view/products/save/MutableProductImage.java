package by.lysyakov.shop.view.products.save;

import by.lysyakov.shop.dto.productsview.ImageFileActionInfo;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class MutableProductImage extends ImmutableProductImage {
    private static final Logger log = LoggerFactory.getLogger(MutableProductImage.class);

    public MutableProductImage() {
    }

    public void setSrc(Long imgProductId, MemoryBuffer memoryBuffer) {
        var inputStream = memoryBuffer.getInputStream();
        this.setSrc(imgProductId, ImageFileActionInfo.save(memoryBuffer.getFileName(), () -> inputStream));
    }

    public void setSrc(Long imgProductId, ImageFileActionInfo fileInfo) {
        if (fileInfo == null) return;
        this.imgProductId = imgProductId;
        this.setSrc(fileInfo);
    }

    protected void setEmptySrc() {
        this.setSrc("");
    }
}
