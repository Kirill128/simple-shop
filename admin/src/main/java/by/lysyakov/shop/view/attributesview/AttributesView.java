package by.lysyakov.shop.view.attributesview;

import by.lysyakov.shop.dto.productsview.AttributeDto;
import by.lysyakov.shop.dto.productsview.AttributeValueDto;
import by.lysyakov.shop.service.productsview.AttributeService;
import by.lysyakov.shop.service.productsview.AttributeValueService;
import by.lysyakov.shop.view.attributesview.Dialogs.AddEditAttributeDialog;
import by.lysyakov.shop.view.attributesview.Dialogs.AddEditAttributeValueDialog;
import by.lysyakov.shop.view.attributesview.Dialogs.DeleteAttributeDialog;
import by.lysyakov.shop.view.attributesview.Dialogs.DeleteAttributeValueDialog;
import by.lysyakov.shop.view.common.AdminPage;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static by.lysyakov.shop.dto.productsview.AttributeValueDto.attributeValueDto;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toMap;

@Route("/attributes")
public class AttributesView extends AdminPage {
    private Map<Long, AttributeDto> cachedAttributes;

    private AttributeService attributeService;
    private AttributeValueService attributeValueService;

    private AddEditAttributeDialog addEditAttributeDialog;
    private AddEditAttributeValueDialog addEditAttributeValueDialog;

    private DeleteAttributeDialog deleteAttributeConfirmDialog;
    private DeleteAttributeValueDialog deleteAttributeValueConfirmDialog;

    private Grid<AttributeDto> attributesAndValuesGrid = new Grid<>(){{
        addColumn(AttributeDto::getId).setHeader("Id");
        addColumn(AttributeDto::getName).setHeader("Name");
        addColumn(AttributeDto::getUsedByProductsCount).setHeader("Used by products count")
                                                       .setSortable(true);
        addComponentColumn(attributeDto -> new Button("Edit", event -> addEditAttributeDialog.open(attributeDto)));
        addComponentColumn(attributeDto -> new Button("Add Value", event -> addEditAttributeValueDialog.open(attributeValueDto(attributeDto))));
        addComponentColumn(attributeDto -> new Button("Delete", event -> deleteAttributeConfirmDialog.open(attributeDto)));
        setSizeFull();
        addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
        setItemDetailsRenderer(new ComponentRenderer<>(attrVal -> attributeValuesGrid(attrVal)));
    }};

    public AttributesView(AttributeService attributeService,
                          AttributeValueService attributeValueService) {
        this.attributeService = attributeService;
        this.attributeValueService = attributeValueService;

        this.addEditAttributeDialog = new AddEditAttributeDialog(this::addEditAttribute, attributeService);
        this.addEditAttributeValueDialog = new AddEditAttributeValueDialog(this::addEditAttributeValue, attributeValueService);
        this.deleteAttributeConfirmDialog = new DeleteAttributeDialog(this::removeAttribute, attributeService);
        this.deleteAttributeValueConfirmDialog = new DeleteAttributeValueDialog(this::removeAttributeValue, attributeValueService);

        this.attributesAndValuesGrid.setItems(findAllAttributesWithValues());
        this.setContent(attributesAndValuesGrid);
        addToNavbar(new Button("Add attribute", event -> addEditAttributeDialog.open(AttributeDto.attributeDto(""))));
    }

    private void addEditAttribute(AttributeDto attr) {
        cachedAttributes.put(attr.getId(), attr);
        reloadFromCache();
    }

    private void addEditAttributeValue(AttributeValueDto attrVal) {
        cachedAttributes.get(attrVal.getAttribute().getId())
                        .saveValue(attrVal);
        reloadFromCache();
    }

    private void removeAttribute(AttributeDto attr) {
        cachedAttributes.remove(attr.getId());
        reloadFromCache();
    }

    private void removeAttributeValue(AttributeValueDto attrVal) {
        cachedAttributes.get(attrVal.getAttribute().getId())
                        .removeValue(attrVal.getId());
        reloadFromCache();
    }

    private Grid<AttributeValueDto> attributeValuesGrid(AttributeDto source) {
        return attributeValuesGrid(source.valuesFlatten());
    }
    private Grid<AttributeValueDto> attributeValuesGrid(Collection<AttributeValueDto> source) {
        return new Grid<>(){{
            addColumn(AttributeValueDto::getId).setHeader("Id")
                                               .setSortable(true);
            addColumn(AttributeValueDto::getValue).setHeader("Value")
                                                  .setSortable(true);
            addColumn(attrVal -> attrVal.getStatistics().getUsedByProductsIds().size())
                    .setHeader("Used by products count")
                    .setSortable(true)
                    .setComparator(comparing(attrVal -> attrVal.getStatistics().getUsedByProductsIds().size()));
            addComponentColumn(attrVal -> new Checkbox(attrVal.visible()) {{
                addValueChangeListener(event -> {
                    attributeValueService.updateIsVisibleOnFilter(attrVal.getId(), !attrVal.visible());
                    setValue(!attrVal.visible());
                    attrVal.setVisibleOnFilter(!attrVal.visible());
                });
            }}).setHeader("Visible");
            addComponentColumn(attributeVal -> new Button("Edit", event -> addEditAttributeValueDialog.open(attributeVal)));
            addComponentColumn(attributeVal -> new Button("Delete", event -> deleteAttributeValueConfirmDialog.open(attributeVal)));
            setItems(source);
            setWidthFull();
            addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
        }};
    }

    private List<AttributeDto> findAllAttributesWithValues() {
        var attributeDtos = attributeService.findAllWithValues();
        this.cachedAttributes = attributeDtos.stream().collect(toMap(AttributeDto::getId, attr -> attr));
        return attributeDtos;
    }

    private void reloadFromCache() {
        this.attributesAndValuesGrid.setItems(cachedAttributes.values());
    }

    @NotNull
    @Override
    protected PageType getPageType() {
        return PageType.ATTRIBUTE;
    }
}
