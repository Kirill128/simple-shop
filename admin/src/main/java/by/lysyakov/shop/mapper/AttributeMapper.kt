package by.lysyakov.shop.mapper

import by.lysyakov.shop.dto.productsview.AttributeStatistic
import by.lysyakov.shop.dto.productsview.AttributeDto
import by.lysyakov.shop.dto.productsview.AttributeValueDto
import by.lysyakov.shop.entities.Attribute

object AttributeMapper {
    fun Attribute.toAttributeDto() = AttributeDto(this.id, this.name)

    fun Attribute.toAttributeDtoWithValues(valueIdToStatistic: Map<Long, AttributeStatistic>) =
        AttributeDto(this.id, this.name).also { attr ->
            attr.values = this.values.associateTo(mutableMapOf()) {
                it.id to AttributeValueDto(it.id,
                                           attr,
                                           it.value,
                                           it.isVisibleOnFilter,
                                           valueIdToStatistic[it.id] ?: AttributeStatistic.empty)
            }
        }

    fun AttributeDto.toAttribute() = Attribute(this.id, this.name)
}