package by.lysyakov.shop.mapper

import by.lysyakov.shop.dto.productsview.OrderDto
import by.lysyakov.shop.dto.productsview.OrderProductDto
import by.lysyakov.shop.dto.productsview.ProductDto
import by.lysyakov.shop.entities.Order
import by.lysyakov.shop.entities.OrderProduct
import by.lysyakov.shop.mapper.CategoryMapper.toProductViewTreeCategoryDto
import by.lysyakov.shop.mapper.StatusMapper.toStatusDto

object OrderMapper {

    fun Order.toOrderDto(orderProductById: (Long) -> List<OrderProduct>): OrderDto = OrderDto(
        this.id,
        this.message,
        this.userName,
        this.phone,
        this.email,
        this.startTime,
        this.lastModified,
        this.status.toStatusDto(),
        orderProductById(this.id).map {
            OrderProductDto(
                productDto = ProductDto(
                    id = it.product.id,
                    shortName = it.product.shortName,
                    price = it.priceForOne,
//                    category = it.product.category.toProductViewCategoryDto(),
                    category = it.product.category.toProductViewTreeCategoryDto(),
                    visible = it.product.visible
                ),
                count = it.count
            )

        }
    )

}