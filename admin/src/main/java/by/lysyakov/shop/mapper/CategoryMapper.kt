package by.lysyakov.shop.mapper

import by.lysyakov.shop.common.dto.TreeCategoryDto
import by.lysyakov.shop.dto.productsview.CategoryDto
import by.lysyakov.shop.entities.Category

object CategoryMapper {

    fun List<Category>.toProductViewCategoryDtos(): List<CategoryDto> = this.map { it.toProductViewCategoryDto() }

    fun Category.toProductViewCategoryDto(): CategoryDto = CategoryDto(this.id, this.name, this.parentCategory?.id)

    fun Category.toProductViewTreeCategoryDto() = TreeCategoryDto(this.id, this.name)

    fun CategoryDto?.toProductViewCategory(): Category? = this?.let { Category(it.id, it.name) }

    fun TreeCategoryDto?.toProductViewCategory(): Category? = this?.let { Category(it.id, it.name) }

    @JvmStatic
    fun flatMapCategoryTree(categoryTree: Iterable<TreeCategoryDto>): List<TreeCategoryDto> =
        categoryTree + categoryTree.flatMap { flatMapCategoryTree(it.subcategories) }
}
