package by.lysyakov.shop.mapper

import by.lysyakov.shop.common.util.Extensions.mapToSet
import by.lysyakov.shop.dto.productsview.AttributeDto
import by.lysyakov.shop.dto.productsview.CategoryDto
import by.lysyakov.shop.dto.productsview.FilterDto
import by.lysyakov.shop.entities.Category
import by.lysyakov.shop.entities.CategoryFilter
import by.lysyakov.shop.entities.Filter
import by.lysyakov.shop.mapper.AttributeMapper.toAttribute

object FilterMapper {
    fun List<Filter>.toDtos(
        attributeProvider: (Long) -> AttributeDto,
        categoryProvider: (Long) -> CategoryDto
    ) = this.map { it.toDto(attributeProvider, categoryProvider) }

    fun Filter.toDto(
        attributeProvider: (Long) -> AttributeDto,
        categoryProvider: (Long) -> CategoryDto
    ) = FilterDto(this.id,
                  this.title,
                  this.operationType,
                  attributeProvider(this.attribute.id))
         .also { it.categories = this.categoryFilters.map { categoryProvider(it.category.id) } }


    fun Filter.toDto(sourceOfRel: FilterDto) =
        FilterDto(this.id,
                  this.title,
                  this.operationType,
                  sourceOfRel.attribute)
            .also { it.categories = sourceOfRel.categories }

    fun FilterDto.toFilter(categoryProvider: (Long) -> Category) =
        Filter.builder()
              .id(this.id)
              .title(this.title)
              .operationType(this.operationType)
              .attribute(this.attribute?.toAttribute())
              .build()
              .also { filter -> filter.categoryFilters = this.categories.mapToSet { CategoryFilter(categoryProvider(it.id), filter) } }

}