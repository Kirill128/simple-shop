package by.lysyakov.shop.mapper

import by.lysyakov.shop.dto.productsview.OrderStatus
import by.lysyakov.shop.entities.Status

object StatusMapper {

    fun Status.toStatusDto(): OrderStatus = OrderStatus.statusById[this.id]!!

}