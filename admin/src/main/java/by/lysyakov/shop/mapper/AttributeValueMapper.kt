package by.lysyakov.shop.mapper

import by.lysyakov.shop.dto.productsview.AttributeDto
import by.lysyakov.shop.dto.productsview.AttributeValueDto
import by.lysyakov.shop.entities.Attribute
import by.lysyakov.shop.entities.AttributePossibleValue
import by.lysyakov.shop.mapper.AttributeMapper.toAttributeDto

object AttributeValueMapper {
    fun AttributePossibleValue.toAttributeValueDto(attributes: Map<Long, AttributeDto>) =
        AttributeValueDto(this.id, attributes[this.attribute.id]!!, this.value, this.isVisibleOnFilter)

    fun AttributePossibleValue.toAttributeValueDto() =
        AttributeValueDto(this.id, this.attribute.toAttributeDto(), this.value, this.isVisibleOnFilter)

    fun AttributeValueDto.toAttributePossibleValue(attributeProvider: (Long?) -> Attribute) =
        AttributePossibleValue.builder()
                              .id(this.id)
                              .value(this.value)
                              .attribute(attributeProvider(this.attribute?.id))
                              .build()
}