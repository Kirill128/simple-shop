package by.lysyakov.shop.mapper

import by.lysyakov.shop.common.Switchers
import by.lysyakov.shop.common.dto.TreeCategoryDto
import by.lysyakov.shop.common.img.ProductImagePathGenerator
import by.lysyakov.shop.dto.productsview.ImageFileActionInfo
import by.lysyakov.shop.dto.productsview.ProductDto
import by.lysyakov.shop.entities.Product
import by.lysyakov.shop.mapper.CategoryMapper.toProductViewCategory
import by.lysyakov.shop.mapper.CategoryMapper.toProductViewCategoryDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.nio.file.Path

@Component
class ProductMapper(
    private val switchers: Switchers,
    private val imagePathParser: ProductImagePathGenerator
) {
    private val log = LoggerFactory.getLogger(ProductMapper::class.java)

    fun toProductViewProductDtos(source: List<Product>): List<ProductDto> {
        val imgFolder: Lazy<String> = switchers.imagesFolderBasePath()
        return source.map { this.toProductViewProductDto(it, imgFolder) }
    }

    fun toProductViewProductDto(
        source: Product,
        imgFolder: Lazy<String>
    ): ProductDto {
        return ProductDto(source.id,
                          source.shortName,
                          source.price,
                          TreeCategoryDto(source.category.id),
                          imagePathParser.mainImgAbsolutePath(source.id, imgFolder)
                                         .toFileInfo(),
                          imagePathParser.allAdditionalImagesAbsolutePaths(source.id, imgFolder)
                                         .toFileInfos(),
                          //TODO optimize to not get attribute_possible_val, but use only product_attribute_val table
                          source.productAttributes.mapTo(mutableSetOf()) { it.id },
                          source.visible)
    }

//    fun toProductViewProductDtos(source: Page<Product>): Page<ProductDto> {
//        val imgFolder: Lazy<String> = switchers.imagesFolderBasePath()
//        return source.map { this.toProductViewProductDto(it, imgFolder) }
//    }
//
//    fun toProductViewProductDto(source: Product): ProductDto {
//        val imgFolder: Lazy<String> = switchers.imagesFolderBasePath()
//        return toProductViewProductDto(source, imgFolder)
//    }

    fun toProduct(source: ProductDto): Product {
        return Product.builder()
                      .id(source.id)
                      .price(source.price)
                      .shortName(source.shortName)
                      .category(source.category.toProductViewCategory())
                      .visible(source.visible)
                      .build()
    }

    private fun Path.toFileInfo() = this.toFile().let { ImageFileActionInfo(it.name) { fileInputStream(it) } }

    private fun Iterable<Path>.toFileInfos() = this.map { it.toFileInfo() }

    private fun fileInputStream(imgFile: File): InputStream {
        if (!imgFile.exists() || !imgFile.canRead()) {
            log.warn("File '{}' not exists, or couldn't read", imgFile.absolutePath)
            return InputStream.nullInputStream()
        }
        return try {
            FileInputStream(imgFile)
        } catch (e: IOException) {
            throw RuntimeException("Couldn't read resource: '${imgFile.absolutePath}'", e)
        }
    }
}