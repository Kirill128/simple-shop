package by.lysyakov.shop.util.impl

import by.lysyakov.shop.common.Switchers
import by.lysyakov.shop.common.img.ProductImagePathGenerator
import by.lysyakov.shop.dto.productsview.ImageFileActionInfo
import by.lysyakov.shop.dto.productsview.ImageFileActionInfo.ACTION.REMOVE
import by.lysyakov.shop.dto.productsview.ImageFileActionInfo.ACTION.SAVE
import by.lysyakov.shop.util.ProductImageSaver
import org.apache.commons.io.FileUtils.copyToFile
import org.springframework.stereotype.Component
import java.io.IOException

@Component
class ProductImageSaverImpl(
    private val productImgPathParser: ProductImagePathGenerator,
    private val switchers: Switchers
) : ProductImageSaver {

    override fun save(productId: Long, mainImage: ImageFileActionInfo?, additionalImages: List<ImageFileActionInfo>) {
        try {
            val imgFolderBasePath = switchers.imagesFolderBasePath()

            mainImage?.let { copyToFile(mainImage.inputStream.get(), mainImg(productId, imgFolderBasePath)) }

            additionalImages.filter { it.action == SAVE }.forEach {
                copyToFile(it.inputStream.get(), additionalImg(productId, it, imgFolderBasePath))
            }

            additionalImages.filter { it.action == REMOVE }.forEach {
                additionalImg(productId, it, imgFolderBasePath).delete()
            }

        } catch (e: IOException) {
            throw RuntimeException("Cannot create img file, method ProductServiceImpl.saveWithImage.", e)
        }
    }

    private fun mainImg(productId: Long, imagesFolderPath: Lazy<String>) =
        productImgPathParser.mainImgAbsolutePath(productId, imagesFolderPath).toFile()

    private fun additionalImg(productId: Long, imgData: ImageFileActionInfo, imagesFolderPath: Lazy<String>) =
        productImgPathParser.imageAbsPath(productId, imgData.fileName, imagesFolderPath).toFile()

}