package by.lysyakov.shop.util

import by.lysyakov.shop.dto.productsview.ImageFileActionInfo

interface ProductImageSaver {
    fun save(productId: Long, mainImage: ImageFileActionInfo?, additionalImages: List<ImageFileActionInfo>)
}