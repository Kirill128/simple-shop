package by.lysyakov.shop.dto.productsview

class StatusDto(
    val id: Long,
    val text: String
)