package by.lysyakov.shop.dto.productsview

import by.lysyakov.shop.common.dto.TreeCategoryDto
import java.util.Objects

class ProductDto(
    var id: Long? = null,
    var shortName: String? = null,
    var price: Double? = null,
    var category: TreeCategoryDto? = null,
    var mainImageFile: ImageFileActionInfo? = null,
    var additionalImagesFiles: List<ImageFileActionInfo> = emptyList(),
    val attributeValuesIds: Set<Long> = mutableSetOf(),
    var visible: Boolean? = null
) {

    fun isVisible(): Boolean = visible ?: true

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val that = o as ProductDto
        return id == that.id
    }

    override fun hashCode() =  Objects.hash(id)
}
