package by.lysyakov.shop.dto.productsview

import by.lysyakov.shop.parsers.ProductParsingMetadata
import java.io.InputStream

class ProductsImportFileData(val file: InputStream,
                             val fileType: String,
                             val productsParsingMetadata: ProductParsingMetadata) {

}