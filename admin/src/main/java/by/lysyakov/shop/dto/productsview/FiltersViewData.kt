package by.lysyakov.shop.dto.productsview

class FiltersViewData(val filters: Collection<FilterDto>,
                      val attributes: Collection<AttributeDto>,
                      val categories: Collection<CategoryDto>)
