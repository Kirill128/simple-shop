package by.lysyakov.shop.dto.productsview

data class CategoryDto(
    val id: Long,
    val name: String,
    val parentCategoryId: Long?
) 