package by.lysyakov.shop.dto.productsview

class UpdateCategoryDto(
    var id: Long? = null,
    var name: String? = null
)