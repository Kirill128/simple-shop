package by.lysyakov.shop.dto.productsview

import by.lysyakov.shop.entities.Category

class SaveCategoryDto(
    val id: Long? = null,
    var name: String? = null,
    var parentCategoryId: Long? = null
) {

    fun toCategory(): Category =
         Category.builder()
            .id(id)
            .name(name)
            .parentCategory(parentCategoryId?.let {  Category(it) })
            .build()

}