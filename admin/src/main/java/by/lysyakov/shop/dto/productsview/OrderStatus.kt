package by.lysyakov.shop.dto.productsview

enum class OrderStatus(val id: Long, val description: String) {
    ORDERED(1, "Заказано, обрабатывается"),
    ON_THE_WAY(2, "Заказ в пути"),
    ARRIVED(3, "Заказ прибыл в пункт выдачи"),
    COMPLETED(5, "Заказ был отдан"),
    REJECTED(6, "Заказ отменён");

    companion object {
        val statusById = OrderStatus.values().associateBy { it.id }
    }
}