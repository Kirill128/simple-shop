package by.lysyakov.shop.dto.productsview

data class AttributeValuesInfo(
    private val allAttributes: MutableMap<Long, AttributeDto> = mutableMapOf(),
    private val allAttrValues: MutableMap<Long, AttributeValueDto> = mutableMapOf()
) {
    private val attrIdToAttrValDtos = allAttrValues.values.groupByTo(mutableMapOf()) { attrVal -> attrVal.attribute?.id }
    fun addAttr(newAttr: AttributeDto, vararg newValues: AttributeValueDto) {
        allAttributes[newAttr.id!!] = newAttr
        allAttrValues.putAll(newValues.map { it.id!! to it })
        attrIdToAttrValDtos[newAttr.id] = newValues.toMutableList()
    }

    fun addAttrValue(attrId: Long, newAttrValue: AttributeValueDto) {
        allAttrValues[newAttrValue.id!!] = newAttrValue
        attrIdToAttrValDtos.computeIfAbsent(attrId) { mutableListOf() }.add(newAttrValue)
    }

    fun attrValByValId(attrValId: Long): AttributeValueDto? = allAttrValues[attrValId]
    fun attrByAttrId(attrId: Long): AttributeDto? = allAttributes[attrId]
    fun attrValuesByAttrId(attrId: Long): List<AttributeValueDto> = attrIdToAttrValDtos[attrId].orEmpty()
    fun allAttributes() = allAttributes.values
    fun allAttrValues() = allAttrValues.values
}
