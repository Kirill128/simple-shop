package by.lysyakov.shop.dto.productsview

import by.lysyakov.shop.dto.productsview.ImageFileActionInfo.ACTION.REMOVE
import by.lysyakov.shop.dto.productsview.ImageFileActionInfo.ACTION.SAVE
import by.lysyakov.shop.dto.productsview.ImageFileActionInfo.ACTION.VIEW
import java.io.InputStream
import java.util.function.Supplier

data class ImageFileActionInfo(val fileName: String, val inputStream: Supplier<InputStream>, var action: ACTION) {
    constructor(fileName: String): this(fileName, { InputStream.nullInputStream() })
    private constructor(fileName: String, action: ACTION): this(fileName, { InputStream.nullInputStream() }, action)
    constructor(fileName: String, inputStream: Supplier<InputStream>): this(fileName, inputStream, VIEW)

    enum class ACTION {
        VIEW,
        REMOVE,
        SAVE
    }

    companion object {
        @JvmStatic
        fun save(fileName: String, inputStream: Supplier<InputStream>) = ImageFileActionInfo(fileName, inputStream, SAVE)

        @JvmStatic
        fun view(fileName: String, inputStream: Supplier<InputStream>) = ImageFileActionInfo(fileName, inputStream, VIEW)

        @JvmStatic
        fun remove(fileName: String) = ImageFileActionInfo(fileName, REMOVE)
    }

}
