package by.lysyakov.shop.dto.productsview

class OrderProductDto(
    val productDto: ProductDto,
    val count: Int
)