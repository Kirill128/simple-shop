package by.lysyakov.shop.dto.productsview

class ProductsAndAttributes(val products: List<ProductDto>,
                            val attributes: Map<Long, AttributeDto>,
                            val attributeValues: Map<Long, AttributeValueDto>)