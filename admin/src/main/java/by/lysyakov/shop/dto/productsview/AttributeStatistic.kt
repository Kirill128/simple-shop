package by.lysyakov.shop.dto.productsview

class AttributeStatistic(
    val usedByProductsIds: Set<Long> = emptySet()
) {
    companion object {
        @JvmStatic
        val empty = AttributeStatistic()
    }
}