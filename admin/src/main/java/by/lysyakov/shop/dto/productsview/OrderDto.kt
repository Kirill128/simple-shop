package by.lysyakov.shop.dto.productsview

import java.time.LocalDateTime

class OrderDto(
    val id: Long,
    val message: String,
    val userName: String,
    val phone: String,
    val email: String,
    val startTime: LocalDateTime,
    var lastModified: LocalDateTime,
    var status: OrderStatus,
    val orderProducts: List<OrderProductDto>
)

