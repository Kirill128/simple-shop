package by.lysyakov.shop.dto.productsview

class AttributeValueDto(
    var id: Long? = null,
    var attribute: AttributeDto?,
    var value: String,
    var visibleOnFilter: Boolean?,
    var statistics: AttributeStatistic = AttributeStatistic.empty
) {
    fun visible() = visibleOnFilter ?: false
    companion object {
        @JvmStatic
        fun attributeValueDto(attribute: AttributeDto, value: String) = AttributeValueDto(null, attribute, value, false)

        @JvmStatic
        fun attributeValueDto(attribute: AttributeDto) = AttributeValueDto(null, attribute, "", false)
    }
}