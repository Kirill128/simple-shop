package by.lysyakov.shop.dto.productsview

class TreeCategoryDtoWithProducts(var id: Long?,
                                  val name: String,
                                  val subcategories: List<TreeCategoryDtoWithProducts>,
                                  val items: List<ProductDto>)
