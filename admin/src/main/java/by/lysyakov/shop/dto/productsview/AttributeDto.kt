package by.lysyakov.shop.dto.productsview

class AttributeDto(
    val id: Long? = null,
    var name: String,
) {
    constructor(id: Long?, name: String, values: MutableMap<Long, AttributeValueDto>): this(id, name) {
        this.values = values
    }

    var values: MutableMap<Long, AttributeValueDto> = mutableMapOf()
        set(source) {
            field = source
            this.usedByProductsIds = usedByProductsIds()
        }

    fun valuesFlatten() = values.values

    var usedByProductsIds = usedByProductsIds()
        private set

    val usedByProductsCount: Int
        get() = usedByProductsIds.size

    fun removeValue(valueId: Long) {
        values.remove(valueId)
    }

    fun saveValue(value: AttributeValueDto) {
        values[value.id!!] = value
    }

    private fun usedByProductsIds() = values.values.fold(setOf<Long>()) { acc, next -> acc + next.statistics.usedByProductsIds }

    companion object {
        @JvmStatic
        fun attributeDto(name: String) = AttributeDto(null, name)

        @JvmStatic
        fun empty() = AttributeDto(null, "")
    }
}