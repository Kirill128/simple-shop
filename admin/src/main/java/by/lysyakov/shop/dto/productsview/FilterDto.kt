package by.lysyakov.shop.dto.productsview

import by.lysyakov.shop.common.util.Extensions.mapToSet
import by.lysyakov.shop.entities.FilterOperationType

class FilterDto(
    var id: Long? = null,
    var title: String? = null,
    var operationType: FilterOperationType? = null,
    var attribute: AttributeDto? = null
) {
    var categories: List<CategoryDto> = listOf()
        set(value) {
            field = value
            categoriesIds = value.mapToSet { it.id }
        }
    var categoriesIds: Set<Long> = emptySet()


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FilterDto

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
